DnD\_5e.weapons package
=======================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.weapons
.. figure:: DnD_5e.weapons.Weapon.png
.. automodule:: DnD_5e.weapons
    :members:
    :undoc-members:
    :show-inheritance:
