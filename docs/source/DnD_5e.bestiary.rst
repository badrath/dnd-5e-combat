DnD\_5e.bestiary package
========================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.bestiary
.. automodule:: DnD_5e.bestiary
    :members:
    :undoc-members:
    :show-inheritance:
