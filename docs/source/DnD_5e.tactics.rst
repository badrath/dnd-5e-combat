DnD\_5e.tactics package
=======================

Subpackages
-----------

.. toctree::

    DnD_5e.tactics.combatant_tactics

Module contents
---------------

.. inheritance-diagram:: DnD_5e.tactics
.. figure:: DnD_5e.tactics.Tactic.png
.. automodule:: DnD_5e.tactics
    :members:
    :undoc-members:
    :show-inheritance:
