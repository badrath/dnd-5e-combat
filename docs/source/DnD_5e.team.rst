DnD\_5e.team package
====================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.team
.. figure:: DnD_5e.team.Team.png
.. automodule:: DnD_5e.team
    :members:
    :undoc-members:
    :show-inheritance:
