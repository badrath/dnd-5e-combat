DnD\_5e.character\_classes package
==================================
Module contents
---------------
.. inheritance-diagram:: DnD_5e.character_classes
.. figure:: DnD_5e.combatant.Character.png
.. figure:: DnD_5e.combatant.SpellCaster.png
.. automodule:: DnD_5e.character_classes
    :members:
    :undoc-members:
    :show-inheritance:
