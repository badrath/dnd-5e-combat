DnD\_5e.combatant package
=========================
Module contents
---------------
.. inheritance-diagram:: DnD_5e.combatant
.. figure:: DnD_5e.combatant.Creature.png
.. figure:: DnD_5e.combatant.Character.png
.. figure:: DnD_5e.combatant.SpellCaster.png
.. automodule:: DnD_5e.combatant
    :members:
    :undoc-members:
    :show-inheritance:
