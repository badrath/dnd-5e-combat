DnD\_5e.armor package
======================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.armor
.. figure:: DnD_5e.armor.Armor.png
    :scale: 50 %
.. figure:: DnD_5e.armor.Shield.png
    :scale: 50 %
.. automodule:: DnD_5e.armor
    :members:
    :undoc-members:
    :show-inheritance:
