DnD\_5e package
===============

Subpackages
-----------

.. toctree::

    DnD_5e.armory
    DnD_5e.attack_class
    DnD_5e.bestiary
    DnD_5e.character_classes
    DnD_5e.combatant
    DnD_5e.dice
    DnD_5e.read_from_web
    DnD_5e.tactics
    DnD_5e.team
    DnD_5e.tests
    DnD_5e.utility_methods_dnd
    DnD_5e.weapons

Module contents
---------------

.. inheritance-diagram:: DnD_5e
.. automodule:: DnD_5e
    :members:
    :undoc-members:
    :show-inheritance:
