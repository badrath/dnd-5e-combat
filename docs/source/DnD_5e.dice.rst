DnD\_5e.dice package
====================
Module contents
---------------
.. inheritance-diagram:: DnD_5e.dice
.. figure:: DnD_5e.dice.DamageDiceBag.png
.. automodule:: DnD_5e.dice
    :members:
    :undoc-members:
    :show-inheritance:
