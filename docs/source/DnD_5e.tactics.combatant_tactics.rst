DnD\_5e.tactics.combatant\_tactics package
==========================================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.tactics.combatant_tactics
.. figure:: DnD_5e.tactics.combatant_tactics.CombatantTactic.png
.. automodule:: DnD_5e.tactics.combatant_tactics
    :members:
    :undoc-members:
    :show-inheritance:
