DnD\_5e.armory package
======================

Module contents
---------------

.. inheritance-diagram:: DnD_5e.armory
.. figure:: DnD_5e.weapons.Weapon.png
.. automodule:: DnD_5e.armory
    :members:
    :undoc-members:
    :show-inheritance:
