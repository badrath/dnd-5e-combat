import warnings
from typing import Optional, List
from DnD_5e.utility_methods_dnd import time_to_rounds, TYPE_DICE_TUPLE, TYPE_ROLL_RESULT
from DnD_5e import weapons, dice

class Attack:
    """
    This class represents attacks
    """
    __hash__ = None  # Tell the interpreter that instances of this class cannot be hashed

    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param range_val: the range of the ranged attack
        :type range_val: usually an integer, but other types are allowed currently (a warning is issued)
        :param melee_range: the range of the melee attack
        :param adv: indicates whether the attack rolls should be made normally, with advantage, or with disadvanatage
        :type adv: one of these integers: -1, 0, 1
        :param weapon: the Weapon this attack comes from, if any
        :type weapon: Weapon or None
        :param attack_mod: the number to be added to attack rolls (ignored if attack_dice is passed in as a Dice)
        :type attack_mod: int
        :param damage_mod: the number to be added to damage rolls (ignored if damage_dice is passed in as a DamageDice)
        :type damage_mod: int
        :param attack_dice: the dice for making attack rolls
        :type attack_dice: dice.Dice
        :param damage_dice: the dice to determine damage points. If provided as a tuple or string,
            keyword arguments will be passed on to the DamageDice constructor, including damage_type
        :type damage_dice: dice.DamageDice or tuple or string
        :raise: ValueError if input is invalid
        """
        name = kwargs.get("name")
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return

        self._name = name
        if not isinstance(name, str):
            raise ValueError("Name should be a string")

        range_val = kwargs.get("range", 0)
        if isinstance(range_val, int):
            self._range = range_val
        else:
            warnings.warn("Non-integer value for range provided")
            self._range = range_val

        melee_range = kwargs.get("melee_range", 0)
        if isinstance(melee_range, int):
            self._melee_range = melee_range
        else:
            self._melee_range = 0

        if not self._range and not self._melee_range:  # assume this is a melee attack if not otherwise specified
            self._melee_range = 5

        adv = kwargs.get("adv", 0)
        if not isinstance(adv, int):
            raise ValueError("Advantage must be an int")

        weapon = kwargs.get("weapon")
        if isinstance(weapon, weapons.Weapon):
            self._weapon = weapon
        elif weapon:
            raise ValueError("Weapon should be a Weapon object")
        else:
            self._weapon = None

        attack_dice = kwargs.get("attack_dice")
        if isinstance(attack_dice, dice.Dice):
            self._attack_dice = attack_dice
        else:
            if isinstance(attack_dice, tuple):
                kwargs.update(dice_tuple=attack_dice)
            elif isinstance(attack_dice, str):
                kwargs.update(str_val=attack_dice)
            else:
                kwargs.update(dice_tuple=(1, 20))
            kwargs.update(modifier=kwargs.get("attack_mod", 0))
            kwargs.update(critable=True)
            kwargs.update(adv=0)  # don't add advantage twice
            self._attack_dice = dice.Dice(**kwargs)
        self._attack_dice.shift_adv(adv)

        damage_dice = kwargs.get("damage_dice")
        if not damage_dice:
            if not self._weapon:
                raise ValueError("Must provide damage dice or a weapon to get damage dice from")
            self._damage_dice = self._weapon.get_damage_dice().get_copy()
            # adjusting damage and attack mods based on keyword arguments
            if kwargs.get("damage_mod"):
                self.shift_damage_mod(kwargs.get("damage_mod"))
        elif isinstance(damage_dice, dice.DamageDice):
            self._damage_dice = damage_dice.get_copy()
            # adjusting damage and attack mods based on keyword arguments
            if kwargs.get("damage_mod"):
                self.shift_damage_mod(kwargs.get("damage_mod"))
        else:
            if isinstance(damage_dice, tuple):
                kwargs.update(dice_tuple=damage_dice)
            elif isinstance(damage_dice, str):
                kwargs.update(str_val=damage_dice)
            kwargs.update(modifier=kwargs.get("damage_mod", 0))
            kwargs.update(critable=False)
            kwargs.update(adv=0)  # can't have advantage or disadvantage on damage
            self._damage_dice = dice.DamageDice(**kwargs)

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Attack to be copied
        :type other: Attack
        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param name: what *self* is called. If not provided, name will be set to the value of *other*. :py:meth:`get_name()`
        :type name: str
        :return: None
        :raise: ValueError if *other* is not an Attack
        """
        if not isinstance(other, Attack):
            raise ValueError("Cannot make self a copy of something that is not an Attack")

        if other.get_weapon():
            warnings.warn("Not recommended to copy an attack tied to a weapon. Copy the weapon itself and assign it to a person.")

        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = other.get_name()

        Attack.__init__(self=self, attack_dice=other.get_attack_dice().get_copy(),
                        damage_dice=other.get_damage_dice().get_copy(), range=other.get_range(),
                        melee_range=other.get_melee_range(),
                        name=name, weapon=other.get_weapon())

    def get_copy(self, **kwargs):
        """
        Create and return a deep copy of *self*

        :param kwargs: keyword arguments (see copy_constructor for keyword arguments)
        :return: a deep copy of *self*
        :rtype: Attack
        """
        copy_obj = type(self)(copy=self, **kwargs)
        return copy_obj

    def __eq__(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the following characteristics:
        damage dice, range, melee range, adv, weapon

        Override the default __eq__ because it's useful to be able to tell if one Combatant's Attacks are the same as another's

        :param other: the Attack to be compared
        :type other: Attack
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        if other is self:
            return True
        if type(other) != type(self):  # pylint: disable=unidiomatic-typecheck
            return False
        return self.get_attack_dice() == other.get_attack_dice() \
            and self.get_damage_dice() == other.get_damage_dice() \
            and self.get_range() == other.get_range() \
            and self.get_melee_range() == other.get_melee_range() \
            and self.get_weapon() is other.get_weapon()

    def get_attack_dice(self) -> dice.Dice:
        """
        :return: attack dice
        :rtype: dice.Dice
        """
        return self._attack_dice

    def get_damage_dice(self) -> dice.DamageDice:
        """
        :return: damage dice
        :rtype: dice.DamageDice
        """
        return self._damage_dice

    def get_attack_mod(self) -> int:
        """
        :return: attack mod
        :rtype: int
        """
        return self.get_attack_dice().get_modifier()

    def get_damage_mod(self) -> int:
        """
        :return: damage mod
        :rtype: int
        """
        try:
            return self.get_damage_dice().get_modifier()
        except NotImplementedError:  # DamageDiceBag
            warnings.warn("Tried to get damage mod on a DamageDiceBag. Returning 0 instead")
            return 0

    def get_damage_type(self) -> Optional[str]:
        """
        :return: damage type
        :rtype: str
        """
        return self.get_damage_dice().get_damage_type()

    def has_damage_type(self, damage_type: str) -> bool:
        """
        :param damage_type: the type of damage to look for
        :type damage_type: str
        :return: True if *self* has the specified damage type, False otherwise
        :rtype: bool
        """
        return self.get_damage_dice().has_damage_type(damage_type)

    def get_range(self) -> int:
        """
        :return: range for ranged attacks
        :rtype: usually a positive integer, but currently other types are also supported
        """
        return self._range

    def get_melee_range(self) -> int:
        """
        :return: range for melee attacks
        :rtype: positive integer
        """
        return self._melee_range

    def get_adv(self) -> int:
        """
        :return: adv
        :rtype: int
        """
        try:
            result = self.get_attack_dice().get_adv()
        except NotImplementedError:
            warnings.warn("Tried to get adv of a DiceBag, which is not supported. Returning 0.")
            result = 0
        return result

    def get_name(self) -> str:
        """
        :return: name
        :rtype: str
        """
        return self._name

    def get_weapon(self) -> Optional[weapons.Weapon]:
        """
        :return: weapon, if any
        :rtype: Weapon or None
        """
        return self._weapon

    def get_max_hit(self) -> int:
        """
        :return: the maximum number that could result from an attack roll
        :rtype: int
        """
        return self.get_attack_dice().get_max_value()

    def get_min_hit(self) -> int:
        """
        :return: the minimum number that could result from an attack roll
        :rtype: int
        """
        return self.get_attack_dice().get_min_value()

    def get_average_hit(self) -> float:
        """
        :return: the average number that would result from an attack roll
        :rtype: float
        """
        return self.get_attack_dice().get_average_value()

    def get_max_damage(self) -> int:
        """
        :return: the maximum number that could result from a damage roll (not counting extra damage from a critical hit)
        :rtype: int
        """
        return self.get_damage_dice().get_max_value()

    def get_min_damage(self) -> int:
        """
        :return: the minimum number that could result from a damage roll (not counting extra damage from a critical hit)
        :rtype: int
        """
        return self.get_damage_dice().get_min_value()

    def get_average_damage(self) -> float:
        """
        :return: the average number that would result from a damage roll
        :rtype: float
        """
        return self.get_damage_dice().get_average_value()

    def get_prob_hit(self, **kwargs) -> float:
        """
        :param ac: the ac you're trying to hit
        :return: the probability of rolling greater than or equal to ac
        """
        ac = kwargs.get("ac")
        return self.get_attack_dice().get_prob(ac, kind="ge")

    def get_dpr(self, **kwargs) -> float:
        """
        :param ac: the armor class you need to hit
        :type ac: int
        :return: the average damage per round (dpr)
        :rtype: int
        """
        ac = kwargs.get("ac")
        if ac is None:
            ac = kwargs.get('target').get_ac()
        return self.get_prob_hit(ac=ac) * self.get_average_damage()

    def set_attack_mod(self, attack_mod: int):
        """
        Set *self._attack_mod*

        :param attack_mod: the new attack mod
        :type attack_mod: int
        :return: None
        :raise: ValueError if *attack_mod* is not an integer
        """
        self.get_attack_dice().set_modifier(attack_mod)

    def shift_attack_mod(self, attack_mod: int):
        """
        Modify *self._attack_mod* by the given value

        :param attack_mod: the damage mod to shift by
        :type attack_mod: int
        :return: None
        :raise: ValueError if *attack_mod* is not an integer
        """
        self.get_attack_dice().shift_modifier(attack_mod)

    def set_damage_mod(self, damage_mod: int):
        """
        Set *self._damage_mod* to the given value

        :param damage_mod: the new damage mod
        :type damage_mod: int
        :return: None
        :raise: ValueError if *damage_mod* is not an integer
        """
        self.get_damage_dice().set_modifier(damage_mod)

    def shift_damage_mod(self, damage_mod: int):
        """
        Modify *self._damage_mod* by the given value

        :param damage_mod: the damage mod to shift by
        :type damage_mod: int
        :return: None
        :raise: ValueError if *damage_mod* is not an integer
        """
        self.get_damage_dice().shift_modifier(damage_mod)

    def roll_attack(self, adv: int = 0) -> TYPE_ROLL_RESULT:  # adv is the additional advantage afforded by circumstance
        """
        Roll a d20 (with advantage/disadvantage as computed with *adv* and :py:attr:`_adv`), adding :py:attr:`_attack_mod`

        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: int
        :return: the roll and an indication of whether it was a critical hit, critical miss, or neither
        :rtype: TYPE_ROLL_RESULT
        """
        result = self.get_attack_dice().roll_dice(adv=adv)
        return result

    def roll_damage(self, crit: int = 0, crit_multiplier: int = 2):
        """
        Roll :py:attr:`_damage_dice` (if crit == 1, multiply the number of dice to roll by *crit_multiplier*),
        then add :py:attr:`_damage_mod`

        :param crit: indicates whether the hit was a crit
        :type crit: one of these integers: -1, 0, 1
        :param crit_multiplier: the multiplier for dice num if the hit is a crit
        :type crit_multiplier: positive integer
        :return: the damage rolled
        :rtype: namedtuple
        :raise: ValueError if *crit_multiplier* is invalid
        """
        return self.get_damage_dice().roll_dice(crit=crit, crit_multiplier=crit_multiplier)

    def make_attack(self, source, target, adv: int = 0) -> Optional[int]:
        """
        Roll attack using :py:meth:`roll_attack` and store the result in variable *result*.
        Call *target*. :py:meth:`take_attack` to see if the attack hits.
        If the attack hits, call :py:meth:`send_damage`.

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :return: the damage taken (if the attack misses, damage taken is None)
        :rtype: non-negative integer or None
        :raise: ValueError if *source* can't send attacks or *target* can't take attacks
        """
        num, crit = self.roll_attack(adv=adv)
        if target.has_condition("unconscious") and crit != -1:  # TODO: and source is within 5 feet of target
            crit = 1  # any attack that hits the target is a critical hit if the attacker is within 5 feet of the target
        try:
            source.get_logger().info("%s attacks %s with %s and rolls a %d." % (source.get_name(), target.get_name(), self.get_name(),
                                    num))
            if target.take_attack((num, crit), source=source, attack=self):  # take_attack returns True if attack hits
                if crit == 1:
                    source.get_logger().info("Critical hit!")
                else:
                    source.get_logger().info("Hit!")
                return self.on_hit(source, target, adv, crit)
            # if the attack failed to hit
            if crit == -1:
                source.get_logger().info("Critical miss.")
            else:
                source.get_logger().info("Miss.")
            return self.on_miss(source, target, adv, crit)  # by default on_miss returns None so it's clear from the return value whether the attack hit
        except NameError:
            raise ValueError("Tried to attack with a source that can't send attacks or a target that can't take attacks")

    def on_hit(self, source, target, adv=0, crit=0) -> int:  # pylint: disable=unused-argument
        """
        You hit the target, now do whatever happens on a hit

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :type crit: whether or not the hit was a crit
        :return: the damage taken
        """
        if source.has_feature("brutal critical"):
            crit_multiplier = 3
        else:
            crit_multiplier = 2
        damage_taken = self.send_damage(target, crit=crit, crit_multiplier=crit_multiplier)
        return damage_taken

    def on_miss(self, source, target, adv=0, crit=0):
        """
        You missed the target, now do whatever happens on a miss

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :return: None
        """
        pass  # pylint: disable=unnecessary-pass

    def send_damage(self, target, crit: int = 0, crit_multiplier: int = 2) -> int:
        """
        Roll damage using :py:meth:`roll_damage` and store the result in variable *damage*.
        Call *target*. :py:meth:`take_damage` , passing in *damage* and :py:attr:`damage_type`.

        :param target: the Combatant that is taking the damage
        :type target: Combatant
        :param crit: indicates whether the hit was a crit
        :type crit: one of these integers: -1, 0, 1
        :param crit_multiplier: the number to multiply dice num by if the hit is a crit
        :type crit_multiplier: positive integer
        :return: the damage taken, as returned by *target.take_damage*
        :rtype: non-negative integer or None
        """
        roll_result = self.roll_damage(crit=crit, crit_multiplier=crit_multiplier)
        damage_taken = 0
        try:
            damage_taken = target.take_damage(roll_result.roll_number, roll_result.damage_type)
        except AttributeError:  # DamageDiceBag was rolled, so roll_list is list of namedtuples
            roll_list = roll_result[1]
            for result in roll_list:
                damage_taken += target.take_damage(result.roll_number, result.damage_type)
        return damage_taken

class MultiAttack(Attack):
    """
    Container for multiple Attacks
    """
    def __init__(self, **kwargs):  # pylint: disable=super-init-not-called
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param attack_list: list of Attacks, in the order in which they will be executed
        :type attack_list: list of Attacks
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return

        self._name = kwargs.get("name", "multiattack")
        if not self._name or not isinstance(self._name, str):
            self._name = "multiattack"

        attack_list = kwargs.get("attacks")
        if not isinstance(attack_list, (list, tuple)):
            raise ValueError("Attacks must be a list or tuple")
        self._attack_list = []
        for attack in attack_list:
            self.add_attack(attack)
        if len(self._attack_list) < 2:
            raise ValueError("MultiAttack must contain 2 or more Attacks")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the MultiAttack to copy
        :type other: MultiAttack
        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param name: what *self* is called. If not provided, name will be set to the value of *other.get_name()*
        :type name: str
        :param deep_copy: if this evaluates to True, make copies of all the Attacks in attack list (otherwise, just use the same reference)
        :return: None
        :raise: ValueError if *other* is not a MultiAttack
        """
        if not isinstance(other, MultiAttack):
            raise ValueError("Cannot copy MultiAttack from other class")
        name = kwargs.get("name")
        if not name or not isinstance(name, str):
            name = other.get_name()
        self._name = name
        self._attack_list = []
        deep_copy = kwargs.get("deep_copy")
        for attack in other.get_attacks():
            if deep_copy:
                attack = attack.get_copy()
            self.add_attack(attack)

    def __eq__(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the following characteristics:
        attack list

        :param other: the MultiAttack to be compared
        :type other: MultiAttack
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        if other is self:
            return True
        if type(other) != type(self):  # pylint: disable=unidiomatic-typecheck
            return False
        return self.get_attacks() == other.get_attacks()

    def current_eq(self, other) -> bool:
        """
        For this class, it is the same as equals.

        :param other: the MultiAttack to be compared
        :type other: MultiAttack
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return self == other

    def get_attacks(self) -> List[Attack]:
        """
        :return: attacks
        :rtype: list of Attacks
        """
        return self._attack_list

    def get_attack_by_name(self, name) -> Optional[Attack]:
        """
        Get an Attack from :py:attr:`_attack_list` with the given name

        :param name: the name of the Attack
        :type name: str
        :return: the first Attack with the given name (or None, if no match is found)
        :raise: ValueError if *name* is invalid
        """
        if not isinstance(name, str):
            raise ValueError("Name must be a string")
        for attack in self.get_attacks():
            if attack.get_name() == name:
                return attack
        return None

    def get_max_damage(self) -> int:
        """
        :return: The maximum damage combined across all attacks
        :rtype: non-negative integer
        """
        max_damage = 0
        for attack in self.get_attacks():
            max_damage += attack.get_max_damage()
        return max_damage

    def get_min_damage(self) -> int:
        """
        :return: The minimum damage combined across all attacks
        :rtype: non-negative integer
        """
        min_damage = 0
        for attack in self.get_attacks():
            min_damage += attack.get_min_damage()
        return min_damage

    def get_average_damage(self) -> int:
        """
        :return: The average damage combined across all attacks
        :rtype: non-negative integer
        """
        average_damage = 0
        for attack in self.get_attacks():
            average_damage += attack.get_average_damage()
        return average_damage

    def add_attack(self, attack: Attack):
        """
        Add *attack* to the end of *self._attack_list*

        :param attack: the Attack to add
        :type attack: Attack
        :return: None
        :raise: ValueError if *attack* is not an Attack
        """
        if not isinstance(attack, Attack):
            raise ValueError("Each attack must be an Attack object")
        self._attack_list.append(attack)

    def remove_attack(self, attack):
        """
        Remove *attack* from :py:attr:`_attack_list`

        :param attack: the Attack to remove (or the name of the Attack to remove)
        :type attack: Attack or str
        :return: None
        """
        if isinstance(attack, str):
            attack = self.get_attack_by_name(attack)
        try:
            self._attack_list.remove(attack)
        except ValueError as error:
            warnings.warn(str(error))

    def make_attack(self, source, target, adv: int = 0) -> Optional[int]:
        """
        Convert *target* and *adv* to lists so that each attack has one target and one adv.
        If *target* or *adv* is too short of a sequence, the last value in the provided list will be copied as needed.
        For each attack in :py:meth:`get_attacks`, call the attack the appropriate target with the appropriate adv.

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant(s) that *source* is attacking
        :type target: Combatant or list of Combatants
        :param adv: one or more integers that indicates advantage on the attack roll
        :return: the total damage taken
        :rtype: non-negative integer
        :raise: ValueError if *source* can't send attacks
        """
        source.get_logger().info("%s attacks using %s" % (source.get_name(), self.get_name()))
        if not isinstance(target, (list, tuple)):
            targets = [target for attack in self.get_attacks()]
        elif len(target) < len(self.get_attacks()):
            targets = target
            for i in range(len(target), len(self.get_attacks())):
                targets.append(target[-1])
        else:
            targets = target
        if not isinstance(adv, (list, tuple)):
            advs = [adv for _ in range(len(self.get_attacks()))]
        elif len(adv) < len(self.get_attacks()):
            advs = adv
            for i in range(len(adv), len(self.get_attacks())):
                advs.append(adv[-1])
        else:
            advs = adv
        total_damage = 0
        for i in range(len(self.get_attacks())):
            current_damage = self.get_attacks()[i].make_attack(source, target=targets[i], adv=advs[i])
            if current_damage:  # done to avoid errors from adding None
                total_damage += current_damage
        return total_damage

    def get_damage_dice(self) -> TYPE_DICE_TUPLE:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_damage_dice on one of the attacks in this multiattack")

    def get_attack_mod(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_attack_mod on one of the attacks in this multiattack")

    def get_damage_mod(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_damage_mod on one of the attacks in this multiattack")

    def get_damage_type(self) -> Optional[str]:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_damage_type on one of the attacks in this multiattack")

    def get_range(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_range on one of the attacks in this multiattack")

    def get_melee_range(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_melee_range on one of the attacks in this multiattack")

    def get_adv(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_adv on one of the attacks in this multiattack")

    def get_weapon(self) -> Optional[weapons.Weapon]:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_weapon on one of the attacks in this multiattack")

    def get_max_hit(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_max_hit on one of the attacks in this multiattack")

    def get_average_hit(self) -> int:
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call get_average_hit on one of the attacks in this multiattack")

    def set_attack_mod(self, attack_mod: int):
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call set_attack_mod on one of the attacks in this multiattack")

    def set_damage_mod(self, damage_mod: int):
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call set_damage_mod on one of the attacks in this multiattack")

    def shift_damage_mod(self, damage_mod: int):
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call shift_damage_mod on one of the attacks in this multiattack")

    def roll_attack(self, adv=0):
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call roll_attack on one of the attacks in this multiattack")

    def roll_damage(self, crit=0, crit_multiplier=2):
        """
        :raise: NotImplementedError
        """
        raise NotImplementedError("Call roll_damage on one of the attacks in this multiattack")

class SavingThrowMixin:
    def __init__(self, **kwargs):
        dc = kwargs.get("dc")
        if not dc or not isinstance(dc, int) or dc < 0:
            raise ValueError("DC must be a non-negative integer")
        self._dc = dc
        save_type = kwargs.get("save_type")
        if save_type in ["strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]:
            self._save_type = save_type
        else:
            raise ValueError("Save type must be strength, dexterity, constitution, intelligence, wisdom, or charisma")
        super(SavingThrowMixin, self).__init__(**kwargs)

    def copy_constructor(self, other, **kwargs):
        self._dc = other.get_dc()
        self._save_type = other.get_save_type()
        super(SavingThrowMixin, self).copy_constructor(other, **kwargs)

    def __eq__(self, other):
        if other is self:
            return True
        if type(other) != type(self):  # pylint: disable=unidiomatic-typecheck
            return False
        return super(SavingThrowMixin, self).__eq__(other) and self.get_dc() == other.get_dc() and self.get_save_type() == other.get_save_type()

    def get_dc(self) -> int:
        """
        :return: DC
        :rtype: non-negative integer
        """
        return self._dc

    def get_save_type(self) -> str:
        """
        :return: ability score for saving throws
        :rtype: one of these strings: "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"
        """
        return self._save_type

    def do_saving_thow(self, source, target) -> bool:  # pylint: disable=unused-argument
        return target.take_saving_throw(self.get_save_type(), self.get_dc(), self)

class SavingThrowAttack(SavingThrowMixin, Attack):
    """
    This class represents Attacks that require a saving throw from the target instead of a roll to hit
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param dc: the difficulty class for the saving throw
        :type dc: non-negative integer
        :param save_type: the ability score to be used for the saving throw
        :type save_type: one of these strings: "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"
        :param damage_on_success: if this evaluates to True, the target takes half damage on a successful save

        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param damage_dice: the dice to determine damage points
        :type damage_dice: TYPE_DICE_TUPLE or dice.DamageDice
        :param attack_mod: the number to be added to attack rolls
        :type attack_mod: int
        :param damage_mod: the number to be added to damage rolls
        :type damage_mod: int
        :param range_val: the range of the ranged attack
        :type range_val: usually an integer, but other types are allowed currently (a warning is issued)
        :param melee_range: the range of the melee attack
        :param adv: indicates whether the attack rolls should be made normally, with advantage, or with disadvanatage
        :type adv: one of these integers: -1, 0, 1
        :param weapon: the Weapon this attack comes from, if any
        :type weapon: Weapon or None
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return

        attack_dice = kwargs.get("attack_dice")
        if not attack_dice:
            kwargs.update(attack_dice=dice.NullDice)

        super().__init__(**kwargs)  # irrelevant keyword arguments will be ignored

        damage_on_success = kwargs.get("damage_on_success")
        self._damage_on_success = bool(damage_on_success)  # half damage on successful save

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the SavingThrowAttack to copy from
        :type other: SavingThrowAttack
        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param name: what *self* is called. If not provided, name will be set to the value of *other.get_name()*
        :type name: str
        :return: None
        :raise: ValueError if *other* is not a SavingThrowAttack
        """
        if not isinstance(other, SavingThrowAttack):
            raise ValueError("Cannot copy SavingThrowAttack from other class")
        super().copy_constructor(other, **kwargs)

        self._damage_on_success = other.get_damage_on_success()  # half damage on successful save

    def __eq__(self, other):
        """
        Compare *self* and *other* to determine if they are equal based on
        what is checked in the superclass method as well as dc, save_type, and damage_on_success

        :param other: the SavingThrowAttack to be compared
        :type other: SavingThrowAttack
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) and self.get_damage_on_success() == other.get_damage_on_success()

    def get_damage_on_success(self) -> bool:
        """
        :return: damage on success
        :rtype: bool
        """
        return self._damage_on_success

    def get_prob_hit(self, **kwargs) -> float:
        """
        :param mod: the target's modifier to the saving throw
        :return: the probability of target failing their saving throw
        """
        mod = kwargs.get("mod")
        return dice.Dice(dice_num=1, dice_type=20, modifier=mod).get_prob(self.get_dc(), kind="lt")

    def get_dpr(self, **kwargs) -> float:
        """
        :param ac: the armor class you need to hit
        :type ac: int
        :return: the average damage per round (dpr)
        :rtype: int
        """
        target = kwargs.get("target")
        mod = target.get_saving_throw(self.get_save_type())
        prob_hit = self.get_prob_hit(mod=mod)
        result = prob_hit * self.get_average_damage()
        if self.get_damage_on_success():
            result += (1 - prob_hit) * (self.get_average_damage() // 2)
        return result

    def make_attack(self, source, target, adv=0) -> Optional[int]:
        """
        Call *target*. :py:meth:`take_saving_throw` to see if the target makes the save.
        Call :py:meth:`send_damage` (that method will handle whether to damage on a successful save).

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the saving throw
        :type adv: one of these integers: -1, 0, 1
        :return: the damage taken (if the attack misses, damage taken is None)
        :rtype: non-negative integer or None
        :raise: ValueError if *target* can't take attacks
        """
        try:
            source.get_logger().info("%s attacks %s with %s." % (source.get_name(), target.get_name(), self.get_name()))
            if self.do_saving_thow(source, target):  # pylint: disable=no-else-return
                # take_saving_throw returns True if target made the save
                source.get_logger().info("%s saves." % target.get_name())
                return self.on_miss(source, target)
            else:
                source.get_logger().info("%s fails the saving throw!" % target.get_name())
                return self.on_hit(source, target)
        except NameError:
            source.get_logger().error("Tried to attack something that can't take attacks", stack_info=True)
            raise ValueError("%s tried to attack something that can't take attacks" % source.get_name())

    def on_hit(self, source, target, adv=0, crit=0):
        """
        The target failed the save, now do whatever happens on a failed save

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :type crit: whether or not the hit was a crit
        :return: the damage taken
        """
        return self.send_damage(target)

    def on_miss(self, source, target, adv=0, crit=0):
        """
        The target made the save, now do whatever happens on a successful save

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :type crit: whether or not the hit was a crit
        :return: the damage taken
        """
        return self.send_damage(target, saved=True)

    def send_damage(self, target, saved: bool = False) -> Optional[int]:  # pylint: disable=arguments-differ
        """
        Roll damage using *self.roll_damage* and store the result in variable *damage*.
        Call *target*. :py:meth:`take_damage`, passing in *damage* and :py:attr:`_damage_type`.

        :param target: the Combatant that is taking the damage
        :type target: Combatant
        :param saved: if this is True, damage is none if not :py:attr:`_damage_on_success` or half damage if :py:attr:`_damage_on_success`
        :type saved: bool
        :return: the damage taken (as returned by *target.take_damage*)
        :rtype: non-negative integer or None
        """
        roll_result = self.roll_damage()

        try:
            roll_result.roll_number  # pylint: disable=pointless-statement
            # stick namedtuple in list so that it can be processed the same way as output from DamageDiceBag
            roll_result = [roll_result]
        except AttributeError:  # DamageDiceBag
            pass

        for result in roll_result:
            if not saved:
                if target.has_feature("evasion") and self._save_type == "dexterity":
                    return target.take_damage(result.roll_number // 2, damage_type=result.damage_type)
                return target.take_damage(result.roll_number, damage_type=result.damage_type)
            if self._damage_on_success:
                if target.has_feature("evasion") and self._save_type == "dexterity":
                    return 0
                return target.take_damage(result.roll_number // 2, damage_type=result.damage_type)
            return None

class Spell(Attack):
    """
    This class represents spells
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param level: the level of the spell
        :type level: integer between 1 and 9 (inclusive)
        :param school: the school of magic that *self* belongs to (currently not typechecked)
        :param casting_time: the time it takes to cast *self*
        :type casting_time: a positive integer for the number of rounds, or one of these strings: "1 action", "1 bonus action", "1 reaction"
        :param components: the components ("v" for verbal, "s" for somatic, "m" for material) needed to cast *self*
        :type components: list or tuple containing one or more of the aforementioned components
        :param duration: how long the spell lasts (in rounds)
        :type duration: non-negative integer
        :param ritual: whether or not the spell can be cast as a ritual
        :type ritual: bool

        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param damage_dice: the dice to determine damage points
        :type damage_dice: TYPE_DICE_TUPLE
        :param attack_mod: the number to be added to attack rolls
        :type attack_mod: int
        :param damage_mod: the number to be added to damage rolls
        :type damage_mod: int
        :param range_val: the range of the ranged attack
        :type range_val: usually an integer, but other types are allowed currently (a warning is issued)
        :param melee_range: the range of the melee attack
        :param adv: indicates whether the attack rolls should be made normally, with advantage, or with disadvanatage
        :type adv: one of these integers: -1, 0, 1
        :param weapon: the Weapon this attack comes from, if any
        :type weapon: Weapon or None
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, **kwargs)
            return
        super().__init__(**kwargs)
        level = kwargs.get("level")
        if isinstance(level, int) and 0 <= level < 10:
            self._level = level
        else:
            raise ValueError("Level must be an integer between 0 and 9")
        school = kwargs.get("school")
        self._school = school
        casting_time = kwargs.get("casting_time")
        if casting_time in ["1 action", "1 bonus action", "1 reaction"]:
            self._casting_time = casting_time
        else:
            self._casting_time = time_to_rounds(casting_time)
        components = kwargs.get("components")
        if not isinstance(components, (list, tuple)):
            raise ValueError("Components must be a list or tuple")
        self._components = []
        if "v" in components:
            self._components.append("v")
        if "s" in components:
            self._components.append("s")
        if "m" in components:
            self._components.append("m")
        duration = kwargs.get("duration")
        self._duration = time_to_rounds(duration)

        if kwargs.get("ritual"):
            self._ritual = True
        else:
            self._ritual = False

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Spell to be copied
        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param name: what *self* is called. If not provided, name will be set to the value of *other.get_name()*. (see :py:meth:`get_name`)
        :type name: str
        :return: None
        :raise: ValueError if *other* is not a Spell
        """
        if not isinstance(other, Spell):
            raise ValueError("Cannot copy Spell from other class")
        super().copy_constructor(other, **kwargs)
        self._level = other.get_level()
        self._school = other.get_school()
        self._casting_time = other.get_casting_time()
        self._components = other.get_components().copy()
        self._duration = other.get_duration()
        self._ritual = other.get_ritual()

    def __eq__(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on
        what is checked in the superclass method as well as level, school, casting time, components, and duration

        :param other: the Spell to be compared
        :type other: Spell
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().__eq__(other) \
            and self.get_level() == other.get_level() \
            and self.get_school() == other.get_school() \
            and self.get_casting_time() == other.get_casting_time() \
            and self.get_components() == other.get_components() \
            and self.get_duration() == other.get_duration() \
            and self.get_ritual() == other.get_ritual()

    def get_level(self) -> int:
        """
        :return: level
        :rtype: integer between 1 and 9 (inclusive)
        """
        return self._level

    def get_school(self):
        """
        :return: school of magic
        """
        return self._school

    def get_casting_time(self) -> int:
        """
        :return: amount of time it takes cast *self*, in rounds
        :rtype: a positive integer for the number of rounds, or one of these strings: "1 action", "1 bonus action", "1 reaction"
        """
        return self._casting_time

    def get_components(self) -> list:
        """
        :return: the components needed to cast *self*
        :rtype: list of strings
        """
        return self._components

    def has_component(self, component: str) -> bool:
        """
        :param component: the component in question
        :type component: str
        :return: True if *self* has that component, False otherwise
        :rtype: bool
        """
        return component in self.get_components()

    def get_duration(self) -> int:
        """
        :return: duration in rounds
        :rtype: non-negative integer
        """
        return self._duration

    def get_ritual(self) -> bool:
        """
        :return: whether spell can be cast as a ritual
        """
        return self._ritual

    def make_attack(self, source, target, adv: int = 0, level: int = None) -> int:  # pylint: disable=arguments-differ
        """
        First, try to spend the appropriate spell slot. Then, call the superclass method.

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :param level: the level the spell is being cast at
        :type level: integer between 1 and 9 (inclusive)
        :return: the damage taken (if the attack misses, damage taken is None)
        :rtype: non-negative integer or None
        :raise: ValueError if *source* can't send attacks or *target* can't take attacks
        """
        if level is None:
            level = self.get_level()
        elif level < self.get_level():
            raise ValueError("Cannot cast a spell at a lower level than the spell's original level")
        if not source.can_cast(self):
            raise ValueError("Source tried to cast a spell that they cannot cast right now")
        try:
            source.spend_spell_slot(level, self)
        except ValueError:
            source.get_logger().error("You tried to cast a level %d spell even though you have no slots left for it" % level, stack_info=True)
            raise ValueError("Source tried to cast a level %d spell even though they have no slots left for it" % level)
        except NameError:
            raise ValueError("Tried to cast a spell using a source that can't cast spells")
        return super().make_attack(source=source, target=target, adv=adv)  # To be overridden in subclasses

class SavingThrowSpell(Spell, SavingThrowAttack):
    """
    Spells that require the target to make a saving throw
    """
    def __init__(self, **kwargs):
        """
        See Spell and SavingThrowAttack methods
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, **kwargs)
            return
        super().__init__(**kwargs)

    def copy_constructor(self, other, **kwargs):
        """
        See Spell and SavingThrowAttack methods
        """
        super().copy_constructor(other, **kwargs)

class HealingSpell(Spell):
    """
    Spells that heal (i.e., add hit points) instead of dealing damage; damage is used as healing
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Valid arguments are as follows:
        :param level: the level of the spell
        :type level: integer between 1 and 9 (inclusive)
        :param school: the school of magic that *self* belongs to (currently not typechecked)
        :param casting_time: the time it takes to cast *self*
        :type casting_time: a positive integer for the number of rounds, or one of these strings: "1 action", "1 bonus action", "1 reaction"
        :param components: the components ("v" for verbal, "s" for somatic, "m" for material) needed to cast *self*
        :type components: list or tuple containing one or more of the aforementioned components
        :param duration: how long the spell lasts (in rounds)
        :type duration: non-negative integer

        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param damage_dice: the dice to determine damage points
        :type damage_dice: TYPE_DICE_TUPLE
        :param attack_mod: the number to be added to attack rolls
        :type attack_mod: int
        :param damage_mod: the number to be added to damage rolls
        :type damage_mod: int
        :param range_val: the range of the ranged attack
        :type range_val: usually an integer, but other types are allowed currently (a warning is issued)
        :param melee_range: the range of the melee attack
        :param adv: indicates whether the attack rolls should be made normally, with advantage, or with disadvanatage
        :type adv: one of these integers: -1, 0, 1
        :param weapon: the Weapon this attack comes from, if any
        :type weapon: Weapon or None
        :raise: ValueError if input is invalid
        """
        super().__init__(**kwargs)

    def make_attack(self, source, target, adv: int = 0, level=None) -> int:
        """
        Spend the appropriate spell slot, then roll damage and heal the target for that amount

        :param source: the Combatant that is sending the healing
        :type source: Combatant
        :param target: the Combatant that *source* is healing
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll (NOT USED FOR THIS METHOD)
        :type adv: one of these integers: -1, 0, 1
        :param level: the level the spell is being cast at
        :type level: integer between 1 and 9 (inclusive)
        :return: the points the target is healed for
        :rtype: non-negative integer
        :raise: ValueError if input is invalid
        """
        if level is None:
            level = self._level
        elif level < self.get_level():
            source.get_logger.error("You cannot cast a spell at a lower level than the spell's original level", stack_info=True)
            raise ValueError("%s cannot cast a spell at a lower level than the spell's original level" % source.get_name())
        try:
            source.spend_spell_slot(level, self)
        except ValueError:
            source.get_logger.error("You tried to cast a level %d spell even though they have no slots left for it" % level)
            raise ValueError("%s tried to cast a level %d spell even though they have no slots left for it" % (source.get_name(), level))
        source.get_logger().info("%s heals %s" % (source.get_name(), target.get_name()))
        healing = self.roll_damage().roll_number
        return target.take_healing(healing)

class SaveOrDieAttack(SavingThrowMixin, Attack):
    """
    Attacks where the target must make a saving throw and if it fails it dies.
    Does not work for attacks that have a saving throw and then a save or die. For that, you need a different approach (maybe a MultiAttack?)
    """
    def __init__(self, **kwargs):
        """
        Create the attack

        :param kwargs: see superclasses
        :param threshold: the hp below which the target must save or die
        :type threshold: int
        :param save_on_miss: whether the target still has to make the saving throw if the main attack missed
        :type save_on_miss: bool
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return
        if not kwargs.get("damage_dice"):
            kwargs.update({"damage_dice": dice.NullDamageDice()})
        self._threshold = kwargs.get("threshold")
        if not isinstance(self._threshold, int):
            raise ValueError("Must provide threshold (the hp below which the target must save or die)")
        self._save_on_miss = bool(kwargs.get("save_on_miss"))
        super().__init__(**kwargs)

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        self._threshold = other.get_threshold()
        self._save_on_miss = other.get_save_on_miss()

    def get_threshold(self) -> int:
        """
        :return: the hp below which the target must save or die
        """
        return self._threshold

    def get_save_on_miss(self) -> bool:
        """
        :return: True if the target still has to make the saving throw if the main attack missed, False otherwise
        """
        return self._save_on_miss

    def make_attack(self, source, target, adv: int = 0):
        """
        Roll attack using :py:meth:`roll_attack` and store the result in variable *result*.
        Call *target*. :py:meth:`take_attack` to see if the attack hits.
        If the attack hits, call :py:meth:`send_damage`.

        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :return: the damage taken (if the attack misses, damage taken is None)
        :rtype: non-negative integer or None
        :raise: ValueError if *source* can't send attacks or *target* can't take attacks
        """
        damage_taken = super().make_attack(source, target, adv)
        if damage_taken is not None or self.get_save_on_miss():
            if target.get_current_hp() < self.get_threshold():
                if self.do_saving_thow(source, target):
                    target.get_logger().info("%s saves and avoids death.", target.get_name())
                else:
                    target.get_logger().info("%s failed the save and will now die.", target.get_name())
                    target.die()

class HitAndSaveAttack(SavingThrowMixin, Attack):
    """
    Attacks where the attacker makes a regular (roll to hit) attack and then, if that hits, makes a saving throw attack
    """
    def __init__(self, **kwargs):
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(other=copy, **kwargs)
            return
        super().__init__(**kwargs)

        save_damage_dice = kwargs.get("save_damage_dice")
        if not save_damage_dice:
            if not self._weapon:
                raise ValueError("Must provide damage dice or a weapon to get damage dice from")
            self._save_damage_dice = self._weapon.get_damage_dice().get_copy()
        elif isinstance(save_damage_dice, dice.DamageDice):
            self._save_damage_dice = save_damage_dice.get_copy()
        else:
            if isinstance(save_damage_dice, tuple):
                kwargs.update(dice_tuple=save_damage_dice)
            elif isinstance(save_damage_dice, str):
                kwargs.update(str_val=save_damage_dice)
            kwargs.update(modifier=kwargs.get("save_damage_mod", 0))
            kwargs.update(critable=False)
            kwargs.update(adv=0)  # can't have advantage or disadvantage on damage
            kwargs.update(damage_type=kwargs.get("save_damage_type"))
            self._save_damage_dice = dice.DamageDice(**kwargs)

        self._damage_on_success = bool(kwargs.get("damage_on_success"))

    def copy_constructor(self, other, **kwargs):
        super().copy_constructor(other, **kwargs)
        self._save_damage_dice = other.get_save_damage_dice()
        self._damage_on_success = other.get_damage_on_success()

    def __eq__(self, other):
        return super().__eq__(other) and self.get_save_damage_dice() == other.get_save_damage_dice() \
               and self.get_damage_on_success() == other.get_damage_on_success()

    def get_save_damage_dice(self):
        """
        :return: saving throw damage dice
        """
        return self._save_damage_dice

    def get_save_damage_type(self) -> str:
        """
        :return: damage type for saving throw damage
        """
        return self.get_save_damage_dice().get_damage_type()

    def get_damage_on_success(self) -> bool:
        """
        :return: whether the target takes damage on a successful save
        """
        return self._damage_on_success

    def on_hit(self, source, target, adv=0, crit=0):
        """
        The attack hit! Do all the normal Attack stuff (i.e., send damage), then have the target make a saving throw.
        :param source: the Combatant that is making the attack
        :type source: Combatant
        :param target: the Combatant that *source* is attacking
        :type target: Combatant
        :param adv: advantage, disadvantage, or neither on the attack roll
        :type adv: one of these integers: -1, 0, 1
        :type crit: whether or not the hit was a crit
        :return: the total damage taken
        """
        damage = super().on_hit(source, target, crit)
        save_result = self.do_saving_thow(source, target)
        save_damage = self.send_save_damage(target, saved=save_result)
        if save_damage:
            damage += save_damage
        return damage

    def roll_save_damage(self):
        """
        Roll :py:attr:`_damage_dice` (if crit == 1, multiply the number of dice to roll by *crit_multiplier*),
        then add :py:attr:`_damage_mod`

        :return: the damage rolled
        :rtype: namedtuple
        :raise: ValueError if *crit_multiplier* is invalid
        """
        return self.get_damage_dice().roll_dice()

    def send_save_damage(self, target, saved: bool = False) -> Optional[int]:  # pylint: disable=arguments-differ
        """
        Roll damage using *self.roll_save_damage* and store the result in variable *damage*.
        Call *target*. :py:meth:`take_damage`, passing in *damage* and :py:attr:`_damage_type`.

        :param target: the Combatant that is taking the damage
        :type target: Combatant
        :param saved: if this is True, damage is none if not :py:attr:`_damage_on_success` or half damage if :py:attr:`_damage_on_success`
        :type saved: bool
        :return: the damage taken (as returned by *target.take_damage*)
        :rtype: non-negative integer or None
        """
        roll_result = self.roll_save_damage()

        try:
            roll_result.roll_number  # pylint: disable=pointless-statement
            # stick namedtuple in list so that it can be processed the same way as output from DamageDiceBag
            roll_result = [roll_result]
        except AttributeError:  # DamageDiceBag
            pass

        for result in roll_result:
            if not saved:
                if target.has_feature("evasion") and self._save_type == "dexterity":
                    return target.take_damage(result.roll_number // 2, damage_type=result.damage_type)
                return target.take_damage(result.roll_number, damage_type=result.damage_type)
            if self._damage_on_success:
                if target.has_feature("evasion") and self._save_type == "dexterity":
                    return 0
                return target.take_damage(result.roll_number // 2, damage_type=result.damage_type)
            return None
