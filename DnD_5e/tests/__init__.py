import warnings
import traceback
import random
import inspect
import logging
import time
from math import isclose
from DnD_5e.utility_methods_dnd import ability_to_mod, cr_to_xp, calc_advantage, proficiency_bonus_per_level, \
    time_to_rounds, proficency_bonus_by_cr, NullLogger
from DnD_5e import armor, armory, attack_class, bestiary, character_classes, combatant, dice, encounter, features, \
    tactics, team, simulation, weapons
from DnD_5e.tactics import combatant_tactics, attack_tactics

# pylint: disable=invalid-name, no-member

# def test_all():
#     test_dice()
#     test_utility()
#     test_combatant()
#     test_creature()
#     test_character()
#     test_weapon()
#     test_armory()
#     test_armor()
#     test_hands()
#     test_attack()
#     test_saving_throw_attack()
#     test_spell()
#     test_saving_throw_spell()
#     test_healing_spell()
#     test_barbarian()
#     test_fighter()
#     test_monk()
#     test_rogue()
#     test_spellcaster()
#     test_bard()
#     test_cleric()
#     test_druid()
#     test_paladin()
#     test_ranger()
#     test_sorcerer()
#     test_warlock()
#     test_wizard()
#     test_features()
#     test_tactics()
#     test_combatant_tactics()
#     test_attack_tactics()
#     test_team()
#     test_encounter()
#     test_simulation()

logging.basicConfig()
test_logger = logging.getLogger("DnD_5e.tests")
test_logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('test.txt')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
test_logger.addHandler(fh)
test_logger.addHandler(ch)

def test_dice():
    """
    :Test module: :py:mod:`dice`
    :Test class: :py:class:`Dice` and subclasses
    :Test functions: N/A
    :Untested classes/functions:
    :Untested parts: rolling the dice
    :Test dependencies: None
    :return: None
    """
    try:
        dice.Dice(dice_tuple=5.2)
        raise Exception("Allowed non-tuple dice_tuple")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_num=3.7)
        raise Exception("Allowed non-int dice_num")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_type=4.1)
        raise Exception("Allowed non-int dice_type")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_tuple=(1, 2, 3))
        raise Exception("Allowed dice_tuple of len != 2")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(dice_tuple=("a", "b"))
        raise Exception("Allowed dice_tuple of non-integers")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="xdz")
        raise Exception("Allowed dice with non-integers in str_val")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="1d4d20")
        raise Exception("Allowed dice with multiple d in str_val")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.Dice(str_val="1d4", modifier="nope")
        raise Exception("Allowed dice with invalid modifier")  # pragma: no cover
    except ValueError:
        pass

    two_d3 = dice.Dice(dice_tuple=(2, 3))
    assert two_d3.get_dice_tuple() == (2, 3)
    assert two_d3.get_dice_num() == 2
    assert two_d3.get_dice_type() == 3
    assert two_d3.get_modifier() == 0
    assert two_d3.get_adv() == 0
    assert two_d3.get_critable() == False  # pylint: disable=singleton-comparison
    assert two_d3.get_max_value() == 6
    assert two_d3.get_min_value() == 2
    assert two_d3.get_average_value() == 4
    assert two_d3.get_max_value() == 6
    assert two_d3.get_prob(2) == 1/9
    assert two_d3.get_prob(3) == 2/9
    assert two_d3.get_prob(4) == 3/9
    assert two_d3.get_prob(5) == 2/9
    assert two_d3.get_prob(6) == 1/9

    three_d4 = dice.Dice(str_val="3d4")
    assert three_d4.get_dice_tuple() == (3, 4)
    assert three_d4.get_dice_num() == 3
    assert three_d4.get_dice_type() == 4
    assert three_d4.get_modifier() == 0
    assert three_d4.get_adv() == 0
    assert three_d4.get_critable() == False  # pylint: disable=singleton-comparison
    assert three_d4.get_max_value() == 12
    assert three_d4.get_min_value() == 3
    assert three_d4.get_average_value() == 7.5
    assert three_d4.get_max_value() == 12
    assert three_d4.get_prob(3) == 1/(4**3)
    assert three_d4.get_prob(4) == 3/(4**3)
    assert three_d4.get_prob(5) == 6/(4**3)

    one_d8_plus_3 = dice.Dice(dice_type=8, modifier=3)
    assert one_d8_plus_3.get_dice_tuple() == (1, 8)
    assert one_d8_plus_3.get_modifier() == 3
    assert one_d8_plus_3.get_adv() == 0
    assert one_d8_plus_3.get_critable() == False  # pylint: disable=singleton-comparison
    assert one_d8_plus_3.get_max_value() == 11
    assert one_d8_plus_3.get_min_value() == 4
    assert one_d8_plus_3.get_average_value() == 7.5
    assert one_d8_plus_3.get_prob(1) == 0
    assert one_d8_plus_3.get_prob(11) == 1/8
    assert one_d8_plus_3.get_prob(10) == 1/8
    assert one_d8_plus_3.get_prob(4) == 1/8

    one_d8_plus_3_copy = dice.Dice(copy=one_d8_plus_3)
    assert one_d8_plus_3_copy.get_dice_tuple() == (1, 8)
    assert one_d8_plus_3_copy.get_modifier() == 3
    assert one_d8_plus_3_copy.get_adv() == 0
    assert one_d8_plus_3_copy.get_critable() == False  # pylint: disable=singleton-comparison
    assert one_d8_plus_3_copy == one_d8_plus_3 == one_d8_plus_3.get_copy()

    try:
        one_d8_plus_3.set_modifier("why")
        raise Exception("Allowed set_modifier with non-int modifier")
    except ValueError:
        pass

    one_d8_plus_3.set_modifier(-1)
    assert one_d8_plus_3.get_modifier() == -1
    assert one_d8_plus_3.get_max_value() == 7
    assert one_d8_plus_3.get_min_value() == 0
    assert one_d8_plus_3.get_average_value() == 3.5
    assert one_d8_plus_3.get_prob(0) == 1/8
    assert one_d8_plus_3.get_prob(8) == 0
    assert one_d8_plus_3 != one_d8_plus_3_copy
    one_d8_plus_3.set_modifier(3)
    assert one_d8_plus_3 == one_d8_plus_3_copy

    one_d8_plus_3.set_adv(2)
    assert one_d8_plus_3.get_adv() == 2
    assert one_d8_plus_3 != one_d8_plus_3_copy
    one_d8_plus_3.shift_adv(-3)
    assert one_d8_plus_3.get_adv() == -1

    try:
        one_d8_plus_3.set_adv("no")
        raise Exception("Allowed set_adv with non-int adv")
    except ValueError:
        pass

    try:
        one_d8_plus_3.shift_adv([])
        raise Exception("Allowed shift_adv with non-int adv")
    except ValueError:
        pass

    four_d10 = dice.Dice(dice_num=4, dice_type=10)
    assert four_d10.get_dice_tuple() == (4, 10)
    assert four_d10.get_adv() == 0
    assert four_d10.get_max_value() == 40
    assert four_d10.get_min_value() == 4
    assert four_d10.get_average_value() == 22

    d20_sample = dice.Dice(str_val="1d20", critable=True)
    assert d20_sample.get_dice_tuple() == (1, 20)
    assert d20_sample.get_adv() == 0
    assert d20_sample.get_critable() == True  # pylint: disable=singleton-comparison
    assert d20_sample.get_max_value() == 20
    assert d20_sample.get_min_value() == 1
    assert d20_sample.get_average_value() == 10.5
    assert isclose(d20_sample.get_prob(10, kind="ge"), 11/20), d20_sample.get_prob(10, kind="ge")
    assert isclose(d20_sample.get_prob(5, kind="le"), 5/20)
    assert isclose(d20_sample.get_prob(6, kind="gt"), 14/20)
    assert isclose(d20_sample.get_prob(4, kind="lt"), 3/20)

    d20_adv = dice.Dice(str_val="1d20", adv=1, critable=True)
    assert d20_adv.get_adv() == 1
    assert isclose(d20_adv.get_prob(5), 0.0225)  # from https://anydice.com/program/1203
    assert isclose(d20_adv.get_prob(15), 0.0725)  # from https://anydice.com/program/1203
    assert d20_adv.get_average_value() == 13.825, d20_adv.get_average_value()

    d20_disadv = dice.Dice(str_val="1d20", adv=-1)
    assert isclose(d20_disadv.get_prob(3), 0.0875)  # from https://anydice.com/program/1203
    assert isclose(d20_disadv.get_prob(17), 0.0175)  # from https://anydice.com/program/1203
    assert isclose(d20_disadv.get_average_value(), 7.175)
    assert isclose(d20_disadv.get_prob(5), 0.0775), d20_disadv.get_prob(5)
    assert isclose(d20_disadv.get_prob(10), 0.0525)
    assert isclose(d20_disadv.get_prob(15), 0.0275)
    d20_disadv_copy = d20_disadv.get_copy()
    assert d20_disadv_copy.get_adv() == -1
    d20_disadv.set_modifier(2)
    assert isclose(d20_disadv.get_prob(5), 0.0875), d20_disadv.get_prob(5)
    assert isclose(d20_disadv.get_prob(10), 0.0625)
    assert isclose(d20_disadv.get_prob(15), 0.0375)

    try:
        d20_sample.shift_modifier(9.0012)
        raise Exception("Allowed shift modifier with non-int modifier")
    except ValueError:
        pass

    d20_sample.shift_modifier(-3)
    assert d20_sample.get_modifier() == -3
    assert d20_sample.get_prob(20) == 0
    assert d20_sample.get_prob(-2) == 1/20
    d20_sample.shift_modifier(5)
    assert d20_sample.get_modifier() == 2

    try:
        dice.DamageDice(dice_obj="nah")
        raise Exception("Allowed DamageDice with invalid dice_obj")
    except ValueError:
        pass

    four_d10_lightning = dice.DamageDice(dice_obj=four_d10, damage_type="lightning")
    assert four_d10_lightning.get_dice_tuple() == (4, 10)
    assert four_d10 is not four_d10_lightning
    assert four_d10_lightning.get_damage_type() == "lightning"
    assert four_d10_lightning.has_damage_type("lightning")

    one_d4_plus_2_fire = dice.DamageDice(dice_tuple=(1, 4), modifier=2, damage_type="fire")
    assert one_d4_plus_2_fire.get_dice_tuple() == (1, 4)
    assert one_d4_plus_2_fire.get_modifier() == 2
    assert one_d4_plus_2_fire.get_damage_type() == "fire"
    assert one_d4_plus_2_fire.has_damage_type("fire")

    four_d10_bludgeoning = dice.DamageDice(copy=four_d10, damage_type="bludgeoning")
    assert four_d10_bludgeoning.get_damage_type() == "bludgeoning"
    assert four_d10_lightning != four_d10_bludgeoning
    assert four_d10_bludgeoning.has_damage_type("bludgeoning")

    four_d10_lightning_copy = dice.DamageDice(copy=four_d10_lightning)
    assert four_d10_lightning_copy.get_dice_tuple() == (4, 10)
    assert four_d10_lightning == four_d10_lightning_copy
    assert four_d10_lightning is not four_d10_lightning_copy

    try:
        dice.DiceBag(dice_list="nah")
        raise Exception("Allowed DiceBag with invalid dice list")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.DiceBag(dice_list=[2])
        raise Exception("Allowed DiceBag with dice list that doesn't contain dice")  # pragma: no cover
    except ValueError:
        pass

    try:
        dice.DiceBag(dice_list=[d20_sample])
        raise Exception("Allowed DiceBag with short dice list")  # pragma: no cover
    except ValueError:
        pass

    one_d6_plus_2 = dice.Dice(dice_tuple=(1, 6), modifier=2)
    dice_list = [two_d3, three_d4, one_d6_plus_2]
    dice_bag_1 = dice.DiceBag(dice_list=dice_list)
    assert dice_bag_1.get_dice_list() == dice_list  # pylint: disable=no-member
    assert dice_bag_1.get_dice_list() is not dice_list  # pylint: disable=no-member
    assert dice_bag_1.get_dice_list()[0] is two_d3 and dice_bag_1.get_dice_list()[1] is three_d4 \
           and dice_bag_1.get_dice_list()[2] is one_d6_plus_2  # pylint: disable=no-member
    assert dice_bag_1.get_max_value() == 26  # 6 + 12 + 8
    assert dice_bag_1.get_min_value() == 8  # 2 + 3 + 3
    assert dice_bag_1.get_average_value() == 17  # 4 + 7.5 + 5.5
    assert isclose(dice_bag_1.get_prob(2+3+3), 1/((3**2)*(4**3)*6)), dice_bag_1.get_prob(2+3+3)  # rolling a 1 on every die

    dice_bag_1_copy = dice_bag_1.get_copy()
    assert dice_bag_1_copy.get_dice_list() == dice_list  # pylint: disable=no-member
    assert dice_bag_1_copy.get_dice_list() is not dice_list  # pylint: disable=no-member
    assert (dice_bag_1_copy.get_dice_list()[0] is two_d3 and dice_bag_1_copy.get_dice_list()[1] is three_d4  # pylint: disable=no-member
        and dice_bag_1_copy.get_dice_list()[2] is one_d6_plus_2)  # pylint: disable=no-member
    assert dice_bag_1_copy.get_max_value() == 26  # 6 + 12 + 8
    assert dice_bag_1_copy.get_min_value() == 8  # 2 + 3 + 3
    assert dice_bag_1_copy.get_average_value() == 17  # 4 + 7.5 + 5.5
    assert dice_bag_1_copy == dice_bag_1
    assert dice_bag_1_copy is not dice_bag_1
    assert dice_bag_1_copy.get_dice_list() is not dice_bag_1.get_dice_list()  # pylint: disable=no-member

    dice_bag_1_deepcopy = dice_bag_1.get_copy(deep_copy=True)
    assert dice_bag_1 == dice_bag_1_deepcopy
    assert dice_bag_1 is not dice_bag_1_deepcopy
    assert dice_bag_1_deepcopy.get_dice_list()[0] is not two_d3  # pylint: disable=no-member
    assert dice_bag_1_deepcopy.get_dice_list()[1] is not three_d4  # pylint: disable=no-member
    assert dice_bag_1_deepcopy.get_dice_list()[2] is not one_d6_plus_2  # pylint: disable=no-member

    dice_bag_1.set_modifier(1)
    assert two_d3.get_modifier() == 1
    assert three_d4.get_modifier() == 1
    assert one_d6_plus_2.get_modifier() == 1

    one_d6_plus_2.set_modifier(2)
    dice_bag_1.shift_modifier(-2)
    assert two_d3.get_modifier() == -1
    assert three_d4.get_modifier() == -1
    assert one_d6_plus_2.get_modifier() == 0

    dice_bag_1.set_adv(1)
    for die in dice_bag_1.get_dice_list():
        assert die.get_adv() == 1
    dice_bag_1.shift_adv(2)
    for die in dice_bag_1.get_dice_list():
        assert die.get_adv() == 3

    try:
        dice_bag_1.get_dice_tuple()
        raise Exception("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_dice_num()
        raise Exception("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_dice_type()
        raise Exception("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_modifier()
        raise Exception("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    try:
        dice_bag_1.get_critable()
        raise Exception("Allowed non-implemented method access")
    except NotImplementedError:
        pass

    dice_bag_2 = dice.DiceBag(dice_list=[dice.Dice(str_val="1d4"), dice.Dice(str_val="1d6")])
    assert dice_bag_2.get_prob(3) == 2/(4*6)

    try:
        dice.DamageDiceBag(dice_list=[four_d10_lightning, one_d8_plus_3])
        raise Exception("Allowed DamageDiceBag with non-damage dice")
    except ValueError:
        pass

    dice_list = [four_d10_lightning, one_d4_plus_2_fire]
    damage_dice_bag = dice.DamageDiceBag(dice_list=dice_list)
    assert damage_dice_bag.get_dice_list() == dice_list
    assert damage_dice_bag.get_dice_list() is not dice_list
    assert damage_dice_bag.get_damage_type_list() == ["lightning", "fire"]
    assert damage_dice_bag.has_damage_type("lightning")
    assert damage_dice_bag.has_damage_type("fire")

    try:
        damage_dice_bag.add_dice(one_d6_plus_2)
        raise Exception("Allowed adding non-damage dice to DamageDiceBag")
    except ValueError:
        pass

    damage_dice_bag.add_dice(four_d10_lightning_copy)  # in normal use cases this is redundant, but it works for this test
    assert damage_dice_bag.get_damage_type_list() == ["lightning", "fire", "lightning"]
    assert damage_dice_bag.get_damage_type_set() == set(["lightning", "fire"])
    assert damage_dice_bag.has_damage_type("lightning")
    assert damage_dice_bag.has_damage_type("fire")

    try:
        damage_dice_bag.get_damage_type()
        raise Exception("Allowed get_damage_type for DamageDiceBag")
    except NotImplementedError:
        pass

    for i in range(10):  # pylint: disable=unused-variable
        print(damage_dice_bag.roll_dice())

    null_dice = dice.NullDice(modifier=1)
    assert null_dice.get_dice_tuple() == (0, 0)
    assert null_dice.get_prob(0) == 1
    assert null_dice.get_modifier() == 0
    null_damage_dice = dice.NullDamageDice()
    assert null_damage_dice.get_damage_type() == ""
    assert null_damage_dice.get_dice_tuple() == (0, 0)
    null_lightning_dice = dice.NullDamageDice(damage_type="lightning")
    assert null_lightning_dice.get_dice_tuple() == (0, 0)
    assert null_lightning_dice.get_damage_type() == "lightning"

def test_utility():
    """
    :Test module: :py:mod:`utility_methods_dnd`
    :Test class: N/A
    :Test functions: :py:func:`ability_to_mod`
    :Untested classes/functions:
        - :py:func:`roll_dice`
        - :py:func:`ability_from_abbreviation` - simple
    :Test dependencies: None
    :return: None
    """
    assert ability_to_mod(10) == 0
    assert ability_to_mod(11) == 0
    assert ability_to_mod(14) == 2
    assert ability_to_mod(8) == -1

    try:
        ability_to_mod(-5)
        raise Exception("Allowed low ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        ability_to_mod(70)
        raise Exception("Allowed high ability score")   # pragma: no cover
    except ValueError:
        pass

    assert cr_to_xp(0) == 10
    assert cr_to_xp(1/8) == 25
    assert cr_to_xp(1/4) == 50
    assert cr_to_xp(2) == 450

    try:
        cr_to_xp(34)
        raise Exception("Allowed invalid challenge rating")
    except ValueError:
        pass

    assert time_to_rounds("1 round") == 1
    assert time_to_rounds("10 rounds") == 10
    assert time_to_rounds("1 minute") == 10
    assert time_to_rounds("2 minutes") == 20
    assert time_to_rounds("1 hour") == 600
    assert time_to_rounds("2 hours") == 1200
    assert time_to_rounds((2, "minutes")) == 20
    assert time_to_rounds([3, "round"]) == 3
    assert time_to_rounds("instantaneous") == 0

    try:
        time_to_rounds("-1 hour")
        raise Exception("Allowed non-positive time")
    except ValueError:
        pass

    try:
        time_to_rounds("3 days")
        raise Exception("Allowed other unit of time")
    except ValueError:
        pass

    try:
        time_to_rounds("2")
        raise Exception("Allowed incomplete input")
    except ValueError:
        pass

    try:
        time_to_rounds(82)
        raise Exception("Allowed invalid input")
    except ValueError:
        pass

    assert calc_advantage([0]) == 0
    assert calc_advantage([1, -1]) == 0
    assert calc_advantage([3, -2, 1]) == 1
    assert calc_advantage([-1, -1, 1]) == -1

    assert proficiency_bonus_per_level(4) == 2
    assert proficiency_bonus_per_level(6) == 3
    assert proficiency_bonus_per_level(13) == 5

    try:
        proficiency_bonus_per_level(0)
        raise Exception("Allowed level too low")
    except ValueError:
        pass

    try:
        proficiency_bonus_per_level(25)
        raise Exception("Allowed level too high")
    except ValueError:
        pass

    try:
        proficiency_bonus_per_level("nope")
        raise Exception("Allowed non-numerical level")
    except ValueError:
        pass

    assert proficency_bonus_by_cr(4) == 2
    assert proficency_bonus_by_cr(7) == 3
    assert proficency_bonus_by_cr(12) == 4
    assert proficency_bonus_by_cr(30) == 9

    try:
        proficency_bonus_by_cr(-2)
        raise Exception("Allowed negative cr")
    except ValueError:
        pass

    try:
        proficency_bonus_by_cr("hi")
        raise Exception("Allowed non-numerical cr")
    except ValueError:
        pass

def test_combatant():
    """
    :Test module: :py:mod:`combatant`
    :Test class: :py:class:`Combatant`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input
        - a few other things that can be found by running nostests with coverage
    :Test dependencies: None
    :return: None
    """
    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], expertise=["stealth"], proficiency_mod=2,
                             name='t0', vulnerabilities=["bludgeoning"],
                             resistances=["slashing", "slashing", "piercing"], immunities={"fire"}, logger=test_logger)
    assert t0.get_name() == 't0'
    assert t0.get_ac() == 12
    assert t0.get_max_hp() == 20
    assert t0.get_current_hp() == 20
    assert t0.get_hp_to_max() == 0
    assert t0.is_hp_max()
    assert t0.get_temp_hp() == 2
    assert t0.get_speed() == 20
    assert t0.get_climb_speed() == 0
    assert t0.get_fly_speed() == 0
    assert t0.get_swim_speed() == 0
    assert t0.get_vision() == 'darkvision'
    assert t0.get_strength() == 2
    assert t0.get_dexterity() == 3
    assert t0.get_constitution() == -1
    assert t0.get_intelligence() == 1
    assert t0.get_wisdom() == 0
    assert t0.get_charisma() == -1
    assert t0.has_proficiency("dexterity")
    assert t0.has_proficiency("charisma")
    assert t0.get_proficiency_mod() == 2
    assert t0.get_vulnerabilities() == {"bludgeoning"}
    assert t0.is_vulnerable("bludgeoning")
    assert t0.is_resistant("slashing")
    assert t0.is_resistant("piercing")
    assert t0.is_immune("fire")
    assert t0.get_saving_throw("strength") == 2
    assert t0.get_saving_throw("dexterity") == 5
    assert t0.get_saving_throw("constitution") == -1
    assert t0.get_saving_throw("intelligence") == 1
    assert t0.get_saving_throw("wisdom") == 0
    assert t0.get_saving_throw("charisma") == 1
    assert not t0.get_features()
    assert t0.get_death_saves() == {0: 0, 1: 0}, t0.get_death_saves()
    assert not t0.get_conditions()
    assert not t0.get_weapons()
    assert t0.get_size() == "medium"
    assert not t0.get_items()
    assert t0.has_expertise("stealth")

    t10 = combatant.Combatant(copy=t0, name="t10")
    assert t10.get_name() == 't10'
    assert t10.get_ac() == 12
    assert t10.get_max_hp() == 20
    assert t10.get_current_hp() == 20
    assert t10.get_hp_to_max() == 0
    assert t10.is_hp_max()
    assert t10.get_temp_hp() == 2
    assert t10.get_speed() == 20
    assert t10.get_climb_speed() == 0
    assert t10.get_fly_speed() == 0
    assert t10.get_swim_speed() == 0
    assert t10.get_vision() == 'darkvision'
    assert t10.get_strength() == 2
    assert t10.get_dexterity() == 3
    assert t10.get_constitution() == -1
    assert t10.get_intelligence() == 1
    assert t10.get_wisdom() == 0
    assert t10.get_charisma() == -1
    assert t10.has_proficiency("dexterity")
    assert t10.has_proficiency("charisma")
    assert t10.get_proficiency_mod() == 2
    assert t10.get_vulnerabilities() == {"bludgeoning"}
    assert t10.is_vulnerable("bludgeoning")
    assert t10.is_resistant("slashing")
    assert t10.is_resistant("piercing")
    assert t10.is_immune("fire")
    assert t10.get_saving_throw("strength") == 2
    assert t10.get_saving_throw("dexterity") == 5
    assert t10.get_saving_throw("constitution") == -1
    assert t10.get_saving_throw("intelligence") == 1
    assert t10.get_saving_throw("wisdom") == 0
    assert t10.get_saving_throw("charisma") == 1
    assert not t10.get_features()
    assert t10.get_death_saves() == {0: 0, 1: 0}
    assert not t10.get_conditions()
    assert not t10.get_weapons()
    assert t10.get_size() == "medium"
    assert not t10.get_items()
    assert t10.has_expertise("stealth")
    assert t10 is not t0
    assert t10.get_conditions() is not t0.get_conditions()
    assert t10.get_proficiencies() is not t0.get_proficiencies()
    assert t10.get_vulnerabilities() is not t0.get_vulnerabilities()
    assert t10.get_resistances() is not t0.get_resistances()
    assert t10.get_immunities() is not t0.get_immunities()
    assert t10.get_features() is not t0.get_features()

    t11 = t10.get_copy(name="t11")
    assert t11.get_name() == 't11'
    assert t11.get_ac() == 12
    assert t11.get_max_hp() == 20
    assert t11.get_current_hp() == 20
    assert t11.get_hp_to_max() == 0
    assert t11.is_hp_max()
    assert t11.get_temp_hp() == 2
    assert t11.get_speed() == 20
    assert t11.get_vision() == 'darkvision'
    assert t11.get_strength() == 2
    assert t11.get_dexterity() == 3
    assert t11.get_constitution() == -1
    assert t11.get_intelligence() == 1
    assert t11.get_wisdom() == 0
    assert t11.get_charisma() == -1
    assert t11.has_proficiency("dexterity")
    assert t11.has_proficiency("charisma")
    assert t11.get_proficiency_mod() == 2
    assert t11.get_vulnerabilities() == {"bludgeoning"}
    assert t11.is_vulnerable("bludgeoning")
    assert t11.is_resistant("slashing")
    assert t11.is_resistant("piercing")
    assert t11.is_immune("fire")
    assert t11.get_saving_throw("strength") == 2
    assert t11.get_saving_throw("dexterity") == 5
    assert t11.get_saving_throw("constitution") == -1
    assert t11.get_saving_throw("intelligence") == 1
    assert t11.get_saving_throw("wisdom") == 0
    assert t11.get_saving_throw("charisma") == 1
    assert not t11.get_features()
    assert t11.get_death_saves() == {0: 0, 1: 0}
    assert not t11.get_conditions()
    assert not t11.get_weapons()
    assert t11.get_size() == "medium"
    assert not t11.get_items()
    assert t11 is not t0
    assert t11.get_conditions() is not t0.get_conditions()
    assert t11.get_proficiencies() is not t0.get_proficiencies()
    assert t11.get_vulnerabilities() is not t0.get_vulnerabilities()
    assert t11.get_resistances() is not t0.get_resistances()
    assert t11.get_immunities() is not t0.get_immunities()
    assert t11.get_features() is not t0.get_features()

    assert t0.equals(t10)  # test equality operator
    assert t10.equals(t11)
    assert t0.current_eq(t10)  # test current equality operator
    assert t10.current_eq(t11)

    t1 = combatant.Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="t1")
    assert t1.get_unarmored_ac() == 13
    assert t1.get_ac() == 13

    # pylint: disable=unused-variable
    try:
        t1 = combatant.Combatant(name="bob", logger=test_logger)
        raise Exception("Create combatant succeeded without ability scores")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac="10", max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                                 logger=test_logger)
        raise Exception("Create combatant succeeded with non-integer AC")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, conditions="dog",
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                                 logger=test_logger)
        raise Exception("Create combatant succeeded with non-list conditions")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                                 logger=test_logger)
        raise Exception("Create combatant succeeded without max hp")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=35, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="t1", logger=test_logger)
        raise Exception("Create combatant succeeded with current hp greater than max hp")   # pragma: no cover
    except ValueError:
        pass

    t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=-5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                             strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="t1", logger=test_logger)  # should give warning that combatant was created unconscious

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 logger=test_logger)
        raise Exception("Create combatant succeeded without name")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=-1, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                                 logger=test_logger)
        raise Exception("Create combatant succeeded with low ability score")   # pragma: no cover
    except ValueError:
        pass

    try:
        t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                                 strength=7, dexterity=50, constitution=9, intelligence=12, wisdom=11, charisma=8, name="t1",
                                 logger=test_logger)
        raise Exception("Create combatant succeeded with high ability score")   # pragma: no cover
    except ValueError:
        pass
    # pylint: enable=unused-variable

    # testing Combatant vision and speed
    t2 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20,
                             climb_speed=5, fly_speed=10, swim_speed=15, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             size="gargantuan", name="t2", logger=test_logger)
    assert t2.can_see("normal")
    assert not t2.can_see("dark")
    assert not t2.can_see("magic")
    assert t2.get_size() == "gargantuan"
    t2.add_condition("blinded")
    assert not t2.can_see("normal")
    assert not t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.remove_condition("blinded")
    assert not t2.has_condition("blinded")
    assert t2.get_climb_speed() == 5
    assert t2.get_fly_speed() == 10
    assert t2.get_swim_speed() == 15

    t3 = t2.get_copy(name="t3")
    assert t3.equals(t2)
    assert t3.current_eq(t2)
    assert t3.get_climb_speed() == 5
    assert t3.get_fly_speed() == 10
    assert t3.get_swim_speed() == 15

    t2.set_size("tiny")
    assert t2.get_size() == "tiny"
    assert t3.equals(t2)
    assert not t3.current_eq(t2)

    t2.set_vision("blindsight")
    assert t2.can_see("normal")
    assert t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.add_condition("blinded")
    assert t2.can_see("normal")
    assert t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.remove_condition("blinded")

    t2.set_vision("darkvision")
    assert t2.can_see("normal")
    assert t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.add_condition("blinded")
    assert not t2.can_see("normal")
    assert not t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.remove_condition("blinded")

    t2.set_vision("truesight")
    assert t2.can_see("normal")
    assert t2.can_see("dark")
    assert t2.can_see("magic")
    t2.add_condition("blinded")
    assert not t2.can_see("normal")
    assert not t2.can_see("dark")
    assert not t2.can_see("magic")
    t2.remove_condition("blinded")

    # take less than temp hp
    assert t2.take_damage(1) == 1
    assert t2.get_temp_hp() == 1
    assert t2.get_current_hp() == 5
    assert t2.get_damage_taken() == 1
    # take more than temp hp
    assert t2.take_damage(2) == 2
    assert t2.get_temp_hp() == 0
    assert t2.get_current_hp() == 4, t2.get_current_hp()
    assert t2.get_hp_to_max() == 26
    # take damage to current hp
    assert t2.take_damage(2) == 2
    assert t2.get_temp_hp() == 0
    assert t2.get_current_hp() == 2
    assert t2.get_hp_to_max() == 28
    # go unconscious but don't die
    assert t2.take_damage(10) == 10
    assert t2.get_temp_hp() == 0
    assert t2.get_current_hp() == 0
    assert t2.has_condition("unconscious")
    assert t2.has_condition("unstable")
    assert t2.get_death_saves() == {0: 0, 1: 0}  # 0 failures, 0 successes
    assert t2.get_times_unconscious() == 1
    t2.fail_death_save()
    t2.fail_death_save()
    t2.succeed_death_save()
    assert t2.get_death_saves() == {0: 2, 1: 1}  # 2 failures, 1 success
    # heal
    assert t2.take_healing(20) == 20
    assert t2.get_current_hp() == 20
    assert not t2.has_condition("unconscious")
    assert not t2.has_condition("unstable")
    assert t2.get_death_saves() == {0: 0, 1: 0}  # ensure death saves have been reset
    # heal above max
    assert t2.take_healing(40) == 10
    assert t2.get_current_hp() == t2.get_max_hp() == 30
    # die
    assert t2.take_damage(100) == 100  # suuuper dead.
    assert t2.get_conditions() == ["dead"]
    assert t2.has_condition("dead")
    assert t2.get_current_hp() == 0
    assert t2.get_temp_hp() == 0

    t2.remove_all_conditions()
    assert t2.get_conditions() == []
    t2.heal_to_max()
    assert t2.get_current_hp() == 30

    t2.add_condition("bored")
    assert t2.has_condition("bored")
    t2.remove_condition("bored")
    assert not t2.has_condition("bored")

    t2.become_unconscious()
    for _ in range(3):
        t2.succeed_death_save()
    assert t2.has_condition("unconscious")
    assert t2.has_condition("stable")
    assert not t2.has_condition("unstable")
    assert t2.get_times_unconscious() == 2
    t2.take_healing(10)  # become conscious again!
    t2.become_unconscious()
    assert t2.has_condition("unconscious")
    assert t2.has_condition("unstable")
    assert not t2.has_condition("stable")
    assert t2.get_death_saves() == {0: 0, 1: 0}
    t2.take_attack((20, 1))  # automatic crit, which means 1 failed death saves just from the hit
    t2.take_damage(5)  # 1 failed death save from taking damage
    assert t2.get_death_saves() == {0: 2, 1: 0}, t2.get_death_saves()
    t2.fail_death_save()
    assert t2.has_condition("dead")
    t2.reset()
    assert t2.get_damage_taken() == 0
    assert t2.get_times_unconscious() == 0

    t1 = combatant.Combatant(ac=18, max_hp=30, current_hp=10, temp_hp=2, hit_dice='1d4', speed=20, vision='blindsight',
                             strength=9, dexterity=10, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="t1", logger=test_logger, conditions=['paralyzed'], death_saves={0: 1, 1: 0})
    t1.modify_adv_to_be_hit(1)
    t1.reset()
    assert t1.get_current_hp() == 30
    assert t1.get_death_saves() == {0: 0, 1: 0}
    assert t1.get_conditions() == []
    assert t1.get_adv_to_be_hit() == 0

def test_creature():
    """
    :Test module: :py:mod:`combatant`
    :Test class: :py:class:`Creature`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - raise ValueError on :py:meth:`copy_constructor` with :py:class:`Combatant` for *other* and invalid keyword arguments
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/2, xp=100, creature_type="elemental", logger=test_logger)
    assert c0.get_cr() == 1/2
    assert c0.get_xp() == 100
    assert c0.get_creature_type() == "elemental"

    c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/4, creature_type="monstrosity", logger=test_logger)
    assert c0.get_cr() == 1/4
    assert c0.get_xp() == 50

    c1 = combatant.Creature(copy=c0, name="c1")
    assert c1.get_name() == "c1"
    assert c1.get_max_hp() == 70  # just check a stat lol
    assert c1.get_cr() == 1/4
    assert c1.get_xp() == 50
    assert c1.get_creature_type() == "monstrosity"
    assert c0 is not c1
    assert c0.equals(c1)

    t2 = combatant.Combatant(ac=18, max_hp=30, current_hp=5, temp_hp=2, hit_dice='1d4', speed=20, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                            name="t2", logger=test_logger)
    c2 = combatant.Creature(copy=t2, name="c2", cr=1, xp=200, creature_type="aberration")
    assert c2.get_name() == "c2"
    assert c2.get_ac() == 18
    assert c2.get_cr() == 1
    assert c2.get_xp() == 200
    assert c2.get_creature_type() == "aberration"
    assert c2 is not t2

    c3 = combatant.Creature(copy=t2, name="c3", cr=1, creature_type="goblin (humanoid)")
    assert c3.get_name() == "c3"
    assert c3.get_ac() == 18
    assert c3.get_cr() == 1
    assert c3.get_xp() == 200
    assert c3.get_creature_type() == "goblin (humanoid)"
    c3.become_unconscious()  # Creatures don't become unconscious, they just die.
    assert c3.has_condition("dead")

    try:
        c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                                cr="dog", logger=test_logger)
        raise Exception("Create Creature succeeded for non-number cr")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                                cr=-2, logger=test_logger)
        raise Exception("Create Creature succeeded for negative cr")   # pragma: no cover
    except ValueError:
        pass

    try:
        combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/4, creature_type="tacocat", logger=test_logger)
        raise Exception("Create Creature succeeded for invalid creature type (string)")
    except ValueError:
        pass

    try:
        combatant.Creature(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                            strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                            cr=1/4, creature_type=2, logger=test_logger)
        raise Exception("Create Creature succeeded for non-string creature type")
    except ValueError:
        pass

def test_character():
    """
    :Test module: :py:mod:`combatant`
    :Test class: :py:class:`Character`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        raise ValueError on :py:meth:`copy_constructor` with :py:class:`Combatant` for *other* and invalid level
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c0",
                             level=5, logger=test_logger)
    assert c0.get_level() == 5

    c1 = combatant.Character(copy=c0, name="c1")
    assert c1.get_name() == "c1"
    assert c1.get_temp_hp() == 2
    assert c1.get_level() == 5
    assert c1 is not c0
    assert c1.equals(c0) and c0.equals(c0.get_copy())

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0', logger=test_logger)

    c2 = combatant.Character(copy=t0, level=2, name="c2", hit_dice='3d6')
    assert c2.get_name() == "c2"
    assert c2.get_vision() == "darkvision"
    assert c2.get_level() == 2
    assert c2 is not t0

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0", level=30, logger=test_logger)
        raise Exception("Create Character succeeded with level too high")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0", level=-2, logger=test_logger)
        raise Exception("Create Character succeeded with level too low")   # pragma: no cover
    except ValueError:
        pass

    try:
        c0 = combatant.Character(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='normal',
                                 strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                                 name="c0", level="stuff", logger=test_logger)
        raise Exception("Create Character succeeded with non-int level")   # pragma: no cover
    except ValueError:
        pass

def test_weapon():
    """
    :Test module: :py:mod:`weapons`
    :Test class: :py:class:`Weapon`
    :Test functions: :py:func:`weapon_list_equals`
    :Untested classes/functions: None
    :Untested parts: invalid input
    :Test dependencies: :py:meth:`test_combatant`
    :return: None
    """
    # pylint: disable=unused-variable
    try:
        weapon = weapons.Weapon(name='weapon')
        raise Exception("Create weapon succeeded without damage dice")   # pragma: no cover
    except ValueError:
        pass

    try:
        weapon = weapons.Weapon(damage_dice=(1, 4))
        raise Exception("Create weapon succeeded without name")   # pragma: no cover
    except ValueError:
        pass
    # pylint: enable=unused-variable

    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_mod=2, damage_mod=1, damage_type="piercing")
    assert dagger.get_name() == "dagger"
    props = dagger.get_properties()
    assert "finesse" in props
    assert dagger.has_prop("light")
    assert dagger.has_prop("range")
    assert dagger.get_range() == (20, 60)
    assert dagger.has_prop("melee")
    assert dagger.get_melee_range() == 5
    assert dagger.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger.has_prop("heavy")
    assert not dagger.has_prop("load")
    assert not dagger.has_prop("reach")
    assert not dagger.has_prop("two_handed")
    assert not dagger.has_prop("versatile")
    assert dagger.get_attack_mod() == 2
    assert dagger.get_damage_mod() == 1
    assert dagger.get_damage_type() == "piercing"

    dagger_2 = weapons.Weapon(copy=dagger, name="dagger_2")
    assert dagger_2.get_name() == "dagger_2"
    props = dagger_2.get_properties()
    assert "finesse" in props
    assert dagger_2.has_prop("light")
    assert dagger_2.has_prop("range")
    assert dagger_2.get_range() == (20, 60)
    assert dagger_2.has_prop("melee")
    assert dagger_2.get_melee_range() == 5
    assert dagger_2.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger_2.has_prop("heavy")
    assert not dagger_2.has_prop("load")
    assert not dagger_2.has_prop("reach")
    assert not dagger_2.has_prop("two_handed")
    assert not dagger_2.has_prop("versatile")
    assert dagger_2.get_attack_mod() == 2
    assert dagger_2.get_damage_mod() == 1
    assert dagger_2.get_damage_type() == "piercing"
    assert dagger is not dagger_2  # make sure they aren't the same object
    assert dagger.get_damage_dice() is not dagger_2.get_damage_dice()

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0', logger=test_logger)

    assert t0.get_weapon_attack_mod(dagger) == 3
    assert t0.get_weapon_damage_mod(dagger) == 3

    t0.add_weapon(dagger)
    assert t0.get_weapons() == [dagger]
    assert dagger.get_owner() is t0

    t1 = combatant.Combatant(copy=t0, name="t1")
    assert len(t1.get_weapons()) == 1
    dagger_3 = t1.get_weapons()[0]
    assert dagger_3.get_name() == "dagger"
    props = dagger_3.get_properties()
    assert "finesse" in props
    assert dagger_3.has_prop("light")
    assert dagger_3.has_prop("range")
    assert dagger_3.get_range() == (20, 60)
    assert dagger_3.has_prop("melee")
    assert dagger_3.get_melee_range() == 5
    assert dagger_3.get_damage_dice().get_dice_tuple() == (1, 4)
    assert not dagger_3.has_prop("heavy")
    assert not dagger_3.has_prop("load")
    assert not dagger_3.has_prop("reach")
    assert not dagger_3.has_prop("two_handed")
    assert not dagger_3.has_prop("versatile")
    assert dagger_3.get_attack_mod() == 2
    assert dagger_3.get_damage_mod() == 1
    assert dagger_3.get_damage_type() == "piercing"
    assert dagger is not dagger_3

    assert dagger.equals(dagger_2) and dagger.equals(dagger_3) and dagger.equals(dagger_2.get_copy())
    assert dagger.current_eq(dagger_2)
    dagger.set_attack_mod(3)
    assert dagger.equals(dagger_2)
    assert not dagger.current_eq(dagger_2)

    try:
        t0.add_weapon('stuff')
        raise Exception("Add weapon succeeded for non-weapon")   # pragma: no cover
    except ValueError:
        pass

def test_armory():
    """
    :Test module: :py:mod:`armory`
    :Test class: :py:class:`Weapon` and subclasses
    :Test functions: :py:func:`is_monk_weapon`
    :Untested classes/functions: None
    :Untested parts: not all weapons/classes are tested, just a sample
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    dagger = armory.Dagger()
    assert isinstance(dagger, weapons.Weapon)
    assert isinstance(dagger, armory.SimpleWeapon)
    assert isinstance(dagger, armory.MeleeWeapon)
    assert dagger.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger.get_damage_type() == "piercing"
    assert dagger.has_prop("finesse")
    assert dagger.has_prop("light")
    assert dagger.get_melee_range() == 5
    assert dagger.get_range() == (20, 60)
    assert armory.is_monk_weapon(dagger)

    t0 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma", "simple weapons", "warpick"], proficiency_mod=2, name='t0',
                             logger=test_logger)
    assert t0.has_weapon_proficiency(dagger)

    war_pick = armory.WarPick()  # choose this one to make sure multi_word names are handled correctly for proficiency
    assert isinstance(war_pick, armory.MartialWeapon)
    assert isinstance(war_pick, armory.MeleeWeapon)
    assert t0.has_weapon_proficiency(war_pick)
    assert not armory.is_monk_weapon(war_pick)  # not monk because it is Martial

    shortbow = armory.Shortbow()  # ranged simple
    assert isinstance(shortbow, armory.RangedWeapon)
    assert not armory.is_monk_weapon(shortbow)  # not monk because it is Ranged

    shortsword = armory.Shortsword()
    assert armory.is_monk_weapon(shortsword)

def test_armor():
    """
    :Test module: :py:mod:`armor`
    :Test class: :py:class:`Armor` and subclasses
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    c0 = combatant.Combatant(armor=armor.PaddedArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["light armor"], proficiency_mod=2, name='c0', logger=test_logger)
    assert c0.get_armor() is armor.PaddedArmor, print(c0.get_armor(), armor.PaddedArmor)
    assert c0.get_ac() == 11 + 3
    assert armor.PaddedArmor.get_stealth() == -1  # TODO: implement penalty on stealth checks
    assert c0.has_armor_proficiency(armor.PaddedArmor)
    assert c0.has_armor_proficiency(armor.LightArmor)

    c1 = combatant.Combatant(armor=armor.ChainShirtArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["medium armor", "PlateArmor"], proficiency_mod=2, name='c1', logger=test_logger)
    assert c1.get_armor() is armor.ChainShirtArmor
    assert c1.get_ac() == 13 + 2
    assert armor.ChainShirtArmor.get_stealth() == 0
    assert c1.has_armor_proficiency(armor.ChainShirtArmor)
    assert c1.has_armor_proficiency(armor.MediumArmor)
    assert c1.has_armor_proficiency(armor.PlateArmor)
    assert not c1.has_armor_proficiency(armor.SplintArmor)

    c2 = combatant.Combatant(armor=armor.HalfPlateArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                             strength=14, dexterity=9, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["medium armor"], proficiency_mod=2, name='c2', logger=test_logger)
    assert c2.get_armor() is armor.HalfPlateArmor
    assert c2.get_ac() == 15 - 1
    assert armor.HalfPlateArmor.get_stealth() == -1
    assert c2.has_armor_proficiency(armor.HalfPlateArmor)
    assert c2.has_armor_proficiency(armor.MediumArmor)
    assert not c2.has_armor_proficiency(armor.StuddedLeatherArmor)

    c3 = combatant.Combatant(armor=armor.SplintArmor, max_hp=20, current_hp=20, hit_dice='3d6', speed=20,
                             strength=15, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["heavy armor"], proficiency_mod=2, name='c3', logger=test_logger)
    assert c3.get_armor() is armor.SplintArmor
    assert c3.get_ac() == 17
    assert armor.SplintArmor.get_strength() == 2
    assert armor.SplintArmor.get_stealth() == -1
    assert c3.has_armor_proficiency(armor.HeavyArmor)
    assert c3.has_armor_proficiency(armor.SplintArmor)
    assert not c3.has_armor_proficiency(armor.ChainShirtArmor)

    c0_copy = c0.get_copy(name="c0_copy")
    assert c0_copy.get_armor() == c0.get_armor()

    c1.set_armor(armor.BreastplateArmor)
    assert c1.get_armor() is armor.BreastplateArmor
    assert c1.get_ac() == 14 + 2
    c1_copy = c1.get_copy()
    assert c1_copy.current_eq(c1)
    c1.set_armor(armor.ScaleMailArmor)  # same AC, different armor
    assert c1.get_ac() == c1_copy.get_ac(), c1_copy.get_ac()
    assert not c1.current_eq(c1_copy)
    c1_copy.set_armor(None)
    assert c1_copy.get_ac() == c1_copy.get_unarmored_ac()

    try:
        c1.set_armor(2)
        raise Exception("Allowed invalid armor for set_armor")
    except ValueError:
        pass

def test_hands():
    """
    :Test module: :py:mod:`combatant` and :py:mod:`armor`
    :Test class: :py:class:`Combatant` and :py:class:`Shield`
    :Test dependencies: :py:func:`test_combatant`, :py:func:`test_armory`
    :return: None
    """
    sword = armory.Shortsword()
    dagger = armory.Dagger()
    c0 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                             intelligence=12, wisdom=11, charisma=8, main_hand=sword, off_hand=dagger, ac=12, name='c0',
                             logger=test_logger)
    assert c0.get_main_hand() == sword
    assert c0.get_off_hand() == dagger

    mace = armory.Mace()
    c1 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                             intelligence=12, wisdom=11, charisma=8, main_hand=mace, off_hand=armor.Shield, ac=12, name='c1',
                             logger=test_logger)
    assert c1.get_main_hand() == mace
    assert c1.get_off_hand() is armor.Shield
    assert c1.get_ac() == 12 + 2

    c2 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                             intelligence=12, wisdom=11, charisma=8, main_hand=mace, ac=12, name='c2', logger=test_logger)
    assert c2.get_main_hand() == mace
    assert c2.get_off_hand() is None

    c3 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                             intelligence=12, wisdom=11, charisma=8, off_hand=armor.Shield, ac=12, name='c3', logger=test_logger)
    assert c3.get_main_hand() is None
    assert c3.get_off_hand() is armor.Shield

    # pylint: disable=unused-variable
    try:
        c4 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                                intelligence=12, wisdom=11, charisma=8, off_hand=armory.Pike(), ac=12, name='c4',
                                 logger=test_logger)
        raise Exception("allowed a two-handed off hand")
    except ValueError:
        pass
    try:
        c4 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                                intelligence=12, wisdom=11, charisma=8, main_hand="hi", off_hand=armor.Shield, ac=12, name='c4',
                                logger=test_logger)
        raise Exception("allowed an invalid main hand")
    except ValueError:
        pass
    try:
        c4 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                             intelligence=12, wisdom=11, charisma=8, off_hand=2, ac=12, name='c4', logger=test_logger)
        raise Exception("allowed an invalid off hand")
    except ValueError:
        pass
    try:
        c4 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                                intelligence=12, wisdom=11, charisma=8, main_hand=mace, off_hand=dagger, ac=12, name='c4',
                                logger=test_logger)
        raise Exception("allowed non-light dual wielding")
    except ValueError:
        pass
    try:
        c4 = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, strength=14, dexterity=16, constitution=9,
                                 intelligence=12, wisdom=11, charisma=8, main_hand=sword, off_hand=armory.HandCrossbow(),
                                 ac=12, name='c4', logger=test_logger)
        raise Exception("allowed non-melee dual wielding")
    except ValueError:
        pass

def test_attack():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`Attack`
    :Test functions: N/A
    :Untested classes/functions: run SaveOrDieAttack or HitAndSaveAttack
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`__eq__` and :py:meth:`current_eq`
        - :py:meth:`shift_attack_mod` (just calls the Dice method, so should be ok)
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_weapon`
    :return: None
    """
    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5.2)
        raise Exception("Create Attack succeeded with non-int attack mod")   # pragma: no cover
    except ValueError:
        pass

    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), damage_mod=["fail"])
        raise Exception("Create Attack succeeded with non-int damage mod")   # pragma: no cover
    except ValueError:
        pass

    try:
        a0 = attack_class.Attack(damage_dice=(1, 8), name=1)
        raise Exception("Create Attack succeeded with non-string name")   # pragma: no cover
    except ValueError:
        pass

    try:
        a0 = attack_class.Attack(attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                            melee_range=10, name="a0")
        raise Exception("Create Attack succeeded with no weapon or damage dice")
    except ValueError:
        pass

    a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="a0")
    assert a0.get_damage_dice().get_dice_tuple() == (1, 8)
    assert a0.get_attack_mod() == 5
    assert a0.get_damage_mod() == 2
    assert a0.get_damage_type() == "piercing"
    assert a0.get_range() == 120
    assert a0.get_melee_range() == 10
    assert a0.get_adv() == -1
    assert a0.get_name() == "a0"
    assert a0.get_max_hit() == 25
    assert isclose(a0.get_average_hit(), 7.175 + 5), a0.get_average_hit()
    assert a0.get_max_damage() == 10
    assert a0.get_average_damage() == 6.5, a0.get_average_damage()
    assert a0.get_min_hit() == 6
    assert isclose(a0.get_prob_hit(ac=6), 1)
    assert isclose(a0.get_dpr(ac=6), a0.get_average_damage())
    assert a0.get_dpr(ac=10) == 0.64 * a0.get_average_damage()

    dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="dagger", damage_dice=(1, 4),
                            attack_mod=2, damage_mod=1, damage_type="piercing")  # cool damage thing
    assert dagger.get_damage_mod() == 1

    t0 = combatant.Combatant(ac=12, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t0', logger=test_logger)

    t0.add_weapon(dagger)
    assert dagger in t0.get_weapons()
    assert dagger.get_owner() is t0
    assert len(t0.get_attacks()) == 3  # melee and two ranged

    dagger_attacks = t0.get_attacks()

    dagger_attack = dagger_attacks[0]
    assert dagger_attack.get_name() == "dagger_range"
    assert dagger_attack == t0.get_attack_by_name("dagger_range")
    assert dagger_attack.get_weapon() is dagger
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5  # 3 + 2
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 20
    assert dagger_attack.get_melee_range() == 0
    assert dagger_attack.get_adv() == 0

    dagger_attack1 = attack_class.Attack(copy=dagger_attack)
    assert dagger_attack1.get_name() == "dagger_range"
    assert dagger_attack1.get_weapon() is dagger
    assert dagger_attack1.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack1.get_damage_type() == "piercing"
    assert dagger_attack1.get_attack_mod() == 5  # 3 + 2
    assert dagger_attack1.get_damage_mod() == 4  # 3 + 1
    assert dagger_attack.get_damage_mod() == dagger_attack1.get_damage_mod()
    assert dagger_attack1.get_range() == 20
    assert dagger_attack1.get_melee_range() == 0
    assert dagger_attack1.get_adv() == 0

    dagger_attack2 = dagger_attack1.get_copy()
    assert dagger_attack2.get_name() == "dagger_range"
    assert dagger_attack2.get_weapon() is dagger
    assert dagger_attack2.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack2.get_damage_type() == "piercing"
    assert dagger_attack2.get_attack_mod() == 5  # 3 + 2
    assert dagger_attack2.get_damage_mod() == 4  # 3 + 1
    assert dagger_attack2.get_range() == 20
    assert dagger_attack2.get_melee_range() == 0
    assert dagger_attack2.get_adv() == 0
    assert dagger_attack2 is not dagger_attack1
    dagger_attack2.set_damage_mod(4)
    dagger_attack2.shift_damage_mod(-4)
    assert dagger_attack2.get_damage_mod() == 0
    assert dagger_attack2.get_min_damage() == 1
    dagger_attack2.set_damage_mod(4)
    assert dagger_attack2.get_damage_mod() == 4

    dagger_attack = dagger_attacks[1]
    assert dagger_attack.get_name() == "dagger_range_disadvantage"
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 60
    assert dagger_attack.get_melee_range() == 0
    assert dagger_attack.get_adv() == -1

    dagger_attack = dagger_attacks[2]
    assert dagger_attack.get_name() == "dagger_melee"
    assert dagger_attack.get_damage_dice().get_dice_tuple() == (1, 4)
    assert dagger_attack.get_damage_type() == "piercing"
    assert dagger_attack.get_attack_mod() == 5
    assert dagger_attack.get_damage_mod() == 4
    assert dagger_attack.get_range() == 0
    assert dagger_attack.get_melee_range() == 5
    assert dagger_attack.get_adv() == 0

    t0.remove_weapon_attacks(dagger)
    assert not t0.get_attacks()

    t0.remove_weapon(dagger)
    assert not t0.get_weapons()
    assert not t0.get_attacks()

    dagger.set_name("dagger")

    mace = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")
    assert mace.get_melee_range() == 5

    t0.add_weapon(dagger)
    t0.add_weapon(mace)

    assert len(t0.get_attacks()) == 4
    t0.remove_weapon(mace)
    assert len(t0.get_attacks()) == 3

    attack_names = [attack.get_name() for attack in t0.get_attacks()]
    assert attack_names[0] == "dagger_range"
    assert attack_names[1] == "dagger_range_disadvantage"
    assert attack_names[2] == "dagger_melee"

    t0.add_weapon(mace)
    t0.remove_weapon(dagger)
    assert len(t0.get_attacks()) == 1
    assert t0.get_attacks()[0].get_name() == "mace_melee"
    t0.add_weapon(dagger)

    t0.remove_all_weapons()
    assert not t0.get_weapons()
    assert not t0.get_attacks()

    dagger.set_attack_mod(0)
    assert dagger.get_attack_mod() == 0
    dagger.set_damage_mod(0)
    assert dagger.get_damage_mod() == 0

    t0.add_weapon(dagger)
    t1 = combatant.Combatant(ac=10, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t1', attacks=[a0], logger=test_logger)  # almost identical to t0, but easier to hit
    assert t1.get_attacks() == [a0]
    t1.add_weapon(mace)
    t1_copy = t1.get_copy()
    assert t1_copy.get_attacks()[0] == a0
    assert t1_copy.get_attacks()[0] is not a0
    t1.get_attacks().remove(a0)

    dagger_melee = t0.get_attacks()[2]
    mace_melee = t1.get_attacks()[0]

    damage_t0_dealt = 0
    for i in range(15):  # pylint: disable=unused-variable
        damage = t0.send_attack(t1, dagger_melee)
        if damage is not None:
            damage_t0_dealt += damage
        if t1.has_condition("unconscious") or t1.has_condition("dead"):  # pragma: no cover
            break
        t1.send_attack(t0, mace_melee)
        if t0.has_condition("unconscious") or t0.has_condition("dead"):  # pragma: no cover
            break
    assert t0.get_damage_dealt() == damage_t0_dealt
    t0.reset()
    assert t0.get_damage_dealt() == 0

    t3 = combatant.Combatant(ac=10, max_hp=40, current_hp=40, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t3', logger=test_logger)
    t4 = t3.get_copy(name="t4")
    t4.add_resistance("fire")
    t4.add_vulnerability("cold")
    fire_dice = dice.DamageDice(dice_tuple=(1, 4), damage_type="fire", modifier=2)
    cold_dice = dice.DamageDice(dice_tuple=(1, 4), damage_type="cold", modifier=2)
    icy_hot_dicebag = dice.DamageDiceBag(dice_list=[fire_dice, cold_dice])
    icy_hot_dagger = weapons.Weapon(finesse=1, light=1, range=(20, 60), melee_range=5, name="Icy Hot Dagger",
                                    damage_dice=icy_hot_dicebag)  # cool damage thing
    t3.add_weapon(icy_hot_dagger)
    icy_hot_dagger_attacks = t3.get_weapon_attacks(icy_hot_dagger)
    icy_hot_dagger_melee = icy_hot_dagger_attacks[2]

    for i in range(15):  # pylint: disable=unused-variable
        t3.send_attack(t4, icy_hot_dagger_melee)
        if t3.has_condition("unconscious") or t3.has_condition("dead"):  # pragma: no cover
            break

    save_or_die_attack = attack_class.SaveOrDieAttack(damage_dice=(1, 8), damage_type="piercing", range=50,
                             melee_range=10, name="doom", save_type='constitution', dc=11, threshold=51)
    assert save_or_die_attack.get_threshold() == 51
    assert save_or_die_attack.get_dc() == 11
    assert save_or_die_attack.get_save_type() == 'constitution'
    assert not save_or_die_attack.get_save_on_miss()
    save_or_die_attack2 = save_or_die_attack.get_copy()
    assert save_or_die_attack2.get_threshold() == 51
    assert save_or_die_attack2.get_dc() == 11
    assert save_or_die_attack2.get_save_type() == 'constitution'
    assert not save_or_die_attack2.get_save_on_miss()
    assert save_or_die_attack2 == save_or_die_attack
    assert save_or_die_attack2 is not save_or_die_attack
    t3_attacks = t3.get_attacks().copy()
    t3.add_attack(save_or_die_attack)
    assert len(t3.get_attacks()) == len(t3_attacks) + 1, [x.get_name() for x in t3.get_attacks()]
    assert save_or_die_attack in t3.get_attacks()
    t3.remove_attack(save_or_die_attack)
    assert t3.get_attacks() == t3_attacks

    hit_and_save_attack = attack_class.HitAndSaveAttack(damage_dice=(1, 8), damage_type="piercing", range=50,
                             name="ouch", save_type='dexterity', dc=15, save_damage_dice="3d6", save_damage_mod=2,
                             save_damage_type="bludgeoning")
    assert hit_and_save_attack.get_save_damage_dice().get_dice_tuple() == (3, 6)
    assert hit_and_save_attack.get_save_damage_dice().get_modifier() == 2
    assert hit_and_save_attack.get_save_type() == "dexterity"
    assert hit_and_save_attack.get_save_damage_type() == "bludgeoning"
    hit_and_save_attack2 = hit_and_save_attack.get_copy()
    assert hit_and_save_attack2.get_save_damage_dice().get_dice_tuple() == (3, 6)
    assert hit_and_save_attack2.get_save_damage_dice().get_modifier() == 2
    assert hit_and_save_attack2.get_save_type() == "dexterity"
    assert hit_and_save_attack2.get_save_damage_type() == "bludgeoning"
    assert hit_and_save_attack2 == hit_and_save_attack
    assert hit_and_save_attack2 is not hit_and_save_attack

def test_saving_throw_attack():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`SavingThrowAttack`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`__eq__`
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_attack`
    :return: None
    """
    mace_2 = weapons.Weapon(name="mace", damage_dice=(1, 6), damage_type="bludgeoning")

    # Attack of the clones!
    t2 = combatant.Combatant(ac=12, max_hp=60, current_hp=60, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name='t2', logger=test_logger)
    t2.add_weapon(mace_2)

    t3 = combatant.Combatant(copy=t2, name="t3")
    mace_3 = weapons.Weapon(copy=mace_2, name="mace_3")
    t3.add_weapon(mace_3)

    sunburn_2 = attack_class.SavingThrowAttack(damage_dice=(1, 8), dc=12, save_type="dexterity", damage_on_success=False,
                                               damage_mod=0, damage_type="radiant", name="Sunburn")
    assert sunburn_2.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn_2.get_dc() == 12
    assert sunburn_2.get_save_type() == "dexterity"
    assert not sunburn_2.get_damage_on_success()
    assert sunburn_2.get_attack_mod() == 0
    assert sunburn_2.get_damage_mod() == 0
    assert sunburn_2.get_damage_type() == "radiant"
    assert sunburn_2.get_name() == "Sunburn"
    assert sunburn_2.get_prob_hit(mod=1) == 0.5
    assert sunburn_2.get_dpr(target=t2) == 8/20 * sunburn_2.get_average_damage()

    sunburn_success = attack_class.SavingThrowAttack(damage_dice=(1, 8), dc=12, save_type="dexterity", damage_on_success=True,
                                               damage_mod=0, damage_type="radiant", name="Sunburn")
    assert sunburn_success.get_damage_on_success()
    assert sunburn_success.get_dpr(target=t2) == 8/20 * sunburn_success.get_average_damage() + \
           12/20 * (sunburn_success.get_average_damage() // 2)

    sunburn_3 = attack_class.SavingThrowAttack(copy=sunburn_2)
    assert sunburn_3.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn_3.get_dc() == 12
    assert sunburn_3.get_save_type() == "dexterity"
    assert not sunburn_3.get_damage_on_success()
    assert sunburn_3.get_attack_mod() == 0
    assert sunburn_3.get_damage_mod() == 0
    assert sunburn_3.get_damage_type() == "radiant"
    assert sunburn_3.get_name() == "Sunburn"

    assert sunburn_2 == sunburn_3 == sunburn_2.get_copy()

    t2.add_attack(sunburn_2)
    t2_attacks = t2.get_attacks()
    t3.add_attack(sunburn_3)
    t3_attacks = t3.get_attacks()

    for i in range(30):  # pylint: disable=unused-variable
        t2.send_attack(t3, random.choice(t2_attacks))
        if t3.has_condition("unconscious") or t3.has_condition("dead"):  # pragma: no cover
            break
        t3.send_attack(t2, random.choice(t3_attacks))
        if t2.has_condition("unconscious") or t2.has_condition("dead"):  # pragma: no cover
            break

def test_spell():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`Spell`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - casting spell (including at different levels) - spell is cast in other tests
        - :py:meth:`current_eq`
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_attack`
    :return: None
    """
    spell0 = attack_class.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, ritual=True, name="spell0")
    assert spell0.get_level() == 1
    assert spell0.get_casting_time() == 10
    assert spell0.get_components() == ["v", "s"]
    assert spell0.get_duration() == 0
    assert spell0.get_school() == "magic"
    assert spell0.get_ritual()

    spell1 = attack_class.Spell(copy=spell0)
    assert spell1.get_level() == 1
    assert spell1.get_casting_time() == 10
    assert spell1.get_components() == ["v", "s"]
    assert spell1.get_duration() == 0
    assert spell1.get_school() == "magic"
    assert spell1.get_ritual()

    assert spell0 == spell1 == spell0.get_copy()

def test_saving_throw_spell():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`SavingThrowSpell`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_saving_throw_attack`
        - :py:func:`test_spell`
    :return: None
    """
    sunburn0 = attack_class.SavingThrowSpell(damage_dice=(1, 8), level=0, school="evocation",
                                             casting_time="1 action", components=["v", "s"], duration="instantaneous",
                                             dc=12, save_type="dexterity", damage_on_success=False,
                                             damage_type="radiant", name="Sunburn")
    assert sunburn0.get_damage_dice().get_dice_tuple() == (1, 8)
    assert sunburn0.get_level() == 0
    assert sunburn0.get_school() == "evocation"
    assert sunburn0.get_casting_time() == "1 action"
    assert sunburn0.get_components() == ["v", "s"]
    assert sunburn0.get_duration() == 0
    assert sunburn0.get_dc() == 12
    assert sunburn0.get_save_type() == "dexterity"
    assert not sunburn0.get_damage_on_success()
    assert sunburn0.get_attack_mod() == 0
    assert sunburn0.get_damage_mod() == 0
    assert sunburn0.get_damage_type() == "radiant"
    assert sunburn0.get_name() == "Sunburn"

    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                               name="s0", spell_ability="wisdom", spell_slots={1: 3, 2: 2, 3: 1}, proficiency_mod=2,
                               spells=[sunburn0], logger=test_logger)
    s1 = combatant.SpellCaster(copy=s0, name="s1")
    sunburn1 = s1.get_spells()[0]
    for i in range(30):  # pylint: disable=unused-variable
        s0.send_attack(s1, sunburn0)
        if s1.has_condition("unconscious") or s1.has_condition("dead"):  # pragma: no cover
            break
        s1.send_attack(s0, sunburn1)
        if s0.has_condition("unconscious") or s0.has_condition("dead"):  # pragma: no cover
            break

def test_healing_spell():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`HealingSpell`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_spell`
    :return: None
    """
    t0 = combatant.Combatant(ac=12, max_hp=70, current_hp=5, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='t0', logger=test_logger)
    t1 = combatant.Combatant(ac=12, max_hp=70, current_hp=0, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "charisma"], proficiency_mod=2, name='t1', logger=test_logger)
    cure_wounds = attack_class.HealingSpell(damage_dice=(1, 8), level=1, casting_time="1 action", components=["v", "s"],
                                            duration="instantaneous", name="Cure Wounds")
    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8,
                               name="s0", spell_ability="wisdom", spell_slots={1: 30}, proficiency_mod=2,
                               spells=[cure_wounds], logger=test_logger)
    targets = [t0, t1]
    for i in range(30):  # pylint: disable=unused-variable
        target = random.choice(targets)
        s0.send_attack(target=target, attack=cure_wounds)
        if t0.is_hp_max() or t1.is_hp_max():
            break

def test_multiattack():
    """
    :Test module: :py:mod:`attack_class`
    :Test class: :py:class:`MultiAttack`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - NotImplementedError
        - :py:meth:`__eq__` and :py:meth:`current_eq`
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_attack`
        - :py:func:`test_healing_spell`
    :return: None
    """
    a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="a0")
    assert a0.get_adv() == -1
    assert a0.get_attack_dice().get_adv() == -1
    a1 = attack_class.Attack(damage_dice=(1, 12), attack_mod=2, damage_mod=3, damage_type="bludgeoning", melee_range=5, name="a1")
    attacks = [a0, a1]
    multiattack = attack_class.MultiAttack(attacks=attacks)
    assert multiattack.get_attacks() == attacks
    assert multiattack.get_attacks() is not attacks
    assert multiattack.get_attack_by_name("a0") is a0
    assert multiattack.get_attack_by_name("a1") is a1
    assert multiattack.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack.get_average_damage() == 6.5 + 9.5
    # low ac so that I know the attacks will hit
    c0 = combatant.Combatant(ac=2, max_hp=100, current_hp=50, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="c0", logger=test_logger)
    c1 = combatant.Combatant(ac=2, max_hp=200, current_hp=200, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             name="c1", logger=test_logger)
    for i in range(50):  # pylint: disable=unused-variable
        fail_num = 0
        try:
            multiattack.remove_attack("Cure Wounds")
            multiattack.make_attack(source=c1, target=c0)
            c0_hp = c0.get_current_hp()
            multiattack.make_attack(source=c1, target=[c0, c1])
            assert c0_hp - a0.get_max_damage() <= c0.get_current_hp() < c0_hp
            c1_hp = c1.get_current_hp()
            assert c1_hp - a1.get_max_damage() <= c1_hp < c1.get_max_hp()
            c0_hp = c0.get_current_hp()

            # make one attack a HealingSpell so that I can tell both were made appropriately
            cure_wounds = attack_class.HealingSpell(damage_dice=(1, 8), level=1, casting_time="1 action", components=["v", "s"],
                                                    duration="instantaneous", name="Cure Wounds")
            multiattack.add_attack(cure_wounds)
            c2 = character_classes.Cleric(name="c2", ac=2, strength_mod=0, dexterity_mod=1, constitution_mod=1, wisdom_mod=3,
                                          intelligence_mod=2, charisma_mod=-2, level=7, spells=[cure_wounds], logger=test_logger)
            multiattack.make_attack(source=c2, target=[c2, c1, c0])
            assert c2.get_max_hp() - a0.get_max_damage() <= c2.get_current_hp() < c2.get_max_hp()
            assert c1_hp - a1.get_max_damage() <= c1.get_current_hp() < c1_hp
            assert c0_hp < c0.get_current_hp() <= c0_hp + cure_wounds.get_max_damage()
        except AssertionError:
            warnings.warn(traceback.format_exc())
            fail_num += 0
            if fail_num > 9:  # only a natural 1 will miss, so the probability of getting 10 or more failures is 0.00016
                raise ValueError("Too many failures occurred")

    multiattack.remove_attack("Cure Wounds")
    multiattack_1 = attack_class.MultiAttack(copy=multiattack, name="multiattack_1")  # shallow copy
    assert multiattack_1.get_name() == "multiattack_1"
    assert multiattack_1.get_attacks() == multiattack.get_attacks()
    assert multiattack_1.get_attacks() is not multiattack.get_attacks()
    assert multiattack_1.get_attack_by_name("a0") is a0
    assert multiattack_1.get_attack_by_name("a1") is a1
    assert multiattack_1.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack_1.get_average_damage() == 6.5 + 9.5
    assert multiattack_1 is not multiattack

    multiattack_2 = multiattack_1.get_copy(name="multiattack_2", deep_copy=True)
    assert multiattack_2.get_name() == "multiattack_2"
    assert multiattack_2.get_attacks() is not multiattack_1.get_attacks()  # pylint: disable=no-member
    assert multiattack_2.get_attack_by_name("a0") is not a0  # pylint: disable=no-member
    assert multiattack_2.get_attack_by_name("a1") is not a1  # pylint: disable=no-member
    assert multiattack_2.get_max_damage() == 8 + 2 + 12 + 3
    assert multiattack_2.get_average_damage() == 6.5 + 9.5

def test_barbarian():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Barbarian`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - rage
        - reckless attack
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    barb = character_classes.Barbarian(name="barb", strength_mod=4, dexterity_mod=4, constitution_mod=4,
                                       wisdom_mod=-2, intelligence_mod=-2, charisma_mod=-3, speed=30, level=20,
                                       logger=test_logger)
    assert barb.get_level() == 20
    assert barb.get_hit_dice() == (20, 12)
    assert barb.get_max_hp() == 12 + 4 + (7+4)*19
    assert barb.has_proficiency("simple weapons")
    assert barb.has_proficiency("martial weapons")
    assert barb.has_proficiency("strength")
    assert barb.has_proficiency("constitution")
    assert barb.get_proficiency_mod() == 6
    assert barb.get_rage_slots() == 6
    assert barb.get_rage_damage_bonus() == 4
    assert not barb.is_raging()
    assert barb.has_feature("rage")
    assert barb.has_feature("unarmored defense")
    assert barb.has_feature("reckless attack")
    assert barb.has_feature("danger sense")
    assert barb.has_feature("extra attack")
    assert barb.has_feature("feral instinct")
    assert barb.has_feature("brutal critical")
    assert barb.has_feature("relentless rage")
    assert barb.has_feature("persistent rage")
    assert barb.has_feature("indomitable might")
    assert barb.has_feature_class(features.UnarmoredDefenseBarbarian)
    assert barb.has_feature_method("get_unarmored_ac")
    assert barb.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseBarbarian.get_unarmored_ac
    assert barb.get_unarmored_ac() == barb.get_ac() == 10 + 4 + 4
    assert barb.has_feature_class(features.FastMovementBarbarian)
    assert barb.has_feature_method("get_speed")
    assert barb.get_speed() == barb._speed + 10  # pylint: disable=protected-access
    assert barb.get_speed() == 30 + 10, barb.get_speed()  # Fast Movement feature

    barb2 = character_classes.Barbarian(copy=barb, name="barb2")
    assert barb2.get_level() == 20
    assert barb2.get_hit_dice() == (20, 12)
    assert barb2.get_max_hp() == 12 + 4 + (7 + 4) * 19
    assert barb2.has_proficiency("simple weapons")
    assert barb2.has_proficiency("martial weapons")
    assert barb2.has_proficiency("strength")
    assert barb2.has_proficiency("constitution")
    assert barb2.get_proficiency_mod() == 6
    assert barb2.get_rage_slots() == 6
    assert barb2.get_rage_damage_bonus() == 4
    assert not barb2.is_raging()
    assert barb2.has_feature("rage")
    assert barb2.has_feature("unarmored defense")
    assert barb2.has_feature("reckless attack")
    assert barb2.has_feature("danger sense")
    assert barb2.has_feature("extra attack")
    assert barb2.get_speed() == 40
    assert barb2.has_feature("feral instinct")
    assert barb2.has_feature("brutal critical")
    assert barb2.has_feature("relentless rage")
    assert barb2.has_feature("persistent rage")
    assert barb2.has_feature("indomitable might")
    assert barb2 is not barb
    assert barb2.has_feature_class(features.UnarmoredDefenseBarbarian)
    assert barb2.has_feature_method("get_unarmored_ac")
    assert barb2.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseBarbarian.get_unarmored_ac
    assert barb2.get_unarmored_ac() == barb2.get_ac() == 10 + 4 + 4
    assert barb2.get_speed() == 30 + 10  # Fast Movement feature

    assert barb.equals(barb2) and barb2.equals(barb.get_copy())
    assert barb.current_eq(barb2)
    barb.start_rage()
    assert not barb.current_eq(barb2)
    assert barb.equals(barb2)

def test_fighter():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Fighter`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - action surge (NOT IMPLEMENTED YET)
        - extra attack (NOT IMPLEMENTED YET)
        - second wind
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_armory`
    :return: None
    """
    f0 = character_classes.Fighter(level=11, name="f0", ac=18, strength_mod=4, dexterity_mod=2, constitution_mod=4,
                                   wisdom_mod=-1, intelligence_mod=-2, charisma_mod=-3, fighting_style="archery",
                                   logger=test_logger)
    assert f0.get_level() == 11
    assert f0.get_hit_dice() == (11, 10)
    assert f0.get_max_hp() == 10 + 4 + (6+4)*10
    assert f0.has_proficiency("simple weapons")
    assert f0.has_proficiency("martial weapons")
    assert f0.has_proficiency("strength")
    assert f0.has_proficiency("constitution")
    assert f0.get_proficiency_mod() == 4
    assert f0.has_fighting_style("archery")
    assert f0.has_feature("fighting style")
    assert f0.has_feature("second wind")
    assert f0.get_second_wind_slots() == 1
    assert f0.has_feature("action surge")
    assert f0.get_action_surge_slots() == 1
    assert f0.has_feature("martial archetype")
    assert f0.has_feature("extra attack")
    assert f0.get_extra_attack_num() == 2
    assert f0.has_feature("indomitable")
    assert f0.get_indomitable_slots() == 1

    f1 = character_classes.Fighter(copy=f0, name="f1")
    assert f1.get_hit_dice() == (11, 10)
    assert f1.get_max_hp() == 10 + 4 + (6 + 4) * 10
    assert f1.has_proficiency("simple weapons")
    assert f1.has_proficiency("martial weapons")
    assert f1.has_proficiency("strength")
    assert f1.has_proficiency("constitution")
    assert f1.get_proficiency_mod() == 4
    assert f1.has_fighting_style("archery")
    assert f1.has_feature("fighting style")
    assert f1.has_feature("second wind")
    assert f1.get_second_wind_slots() == 1
    assert f1.has_feature("action surge")
    assert f1.get_action_surge_slots() == 1
    assert f1.has_feature("martial archetype")
    assert f1.has_feature("extra attack")
    assert f1.get_extra_attack_num() == 2
    assert f1.has_feature("indomitable")
    assert f1.get_indomitable_slots() == 1
    assert f1 is not f0

    assert f1.equals(f0) and f1.equals(f1.get_copy())
    assert f1.current_eq(f0)
    f1.take_action_surge()  # decreases action surge slots
    assert not f1.current_eq(f0)
    assert f1.equals(f0)

    shortbow = armory.Shortbow()
    assert isinstance(shortbow, armory.RangedWeapon)
    assert shortbow.get_range()

    f0.add_weapon(shortbow)
    for attack in f0.get_attacks():
        # both are ranged attacks
        assert attack.get_attack_mod() == f0.get_dexterity() + f0.get_proficiency_mod() + 2  # testing Archer fighting style
    f0.remove_weapon(shortbow)

    for attack in f0.get_attacks():
        # both are ranged attacks
        assert attack.get_attack_mod() == f0.get_strength() + f0.get_proficiency_mod() + 2  # testing Archer fighting style

def test_monk():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Monk`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - add an :py:class:`Attack` that uses a :py:class:`Monk` weapon when *self's* dexterity is greater than *self's* strength
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_armory`
    :return: None
    """
    m0 = character_classes.Monk(name="m0", level=20, strength_mod=5, dexterity_mod=5, constitution_mod=4,
                                wisdom_mod=1, intelligence_mod=0, charisma_mod=-2, logger=test_logger)
    assert m0.get_level() == 20
    assert m0.get_hit_dice() == (20, 8)
    assert m0.get_max_hp() == (8 + 4) + (5+4)*19
    assert m0.has_proficiency("monk weapons")
    assert m0.has_proficiency("strength")
    assert m0.has_proficiency("dexterity")
    assert m0.has_proficiency("constitution")
    assert m0.has_proficiency("wisdom")
    assert m0.has_proficiency("intelligence")
    assert m0.has_proficiency("charisma")
    assert m0.get_proficiency_mod() == 6
    assert m0.get_martial_arts_dice() == (1, 10)
    assert m0.has_feature("unarmored defense")
    assert m0.has_feature("flurry of blows")
    assert m0.has_feature("patient defense")
    assert m0.has_feature("step of the wind")
    assert m0.has_feature("deflect missiles")
    assert m0.has_feature("slow fall")
    assert m0.has_feature("extra attack")
    assert m0.has_feature("stunning strike")
    assert m0.has_feature("ki-empowered strikes")
    assert m0.has_feature("stillness of mind")
    assert m0.has_feature("evasion")
    assert m0.has_feature("purity of body")
    assert m0.has_feature("tongue of the sun and moon")
    assert m0.has_feature("diamond soul")
    assert m0.has_feature("empty body")
    assert m0.has_feature("perfect soul")
    assert m0.get_ki_points() == 20
    assert m0.get_ki_save_dc() == 8 + 6 + 1
    assert m0.get_saving_throw("strength") == m0.get_strength() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("dexterity") == m0.get_dexterity() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("constitution") == m0.get_constitution() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("wisdom") == m0.get_wisdom() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("intelligence") == m0.get_intelligence() + m0.get_proficiency_mod()
    assert m0.get_saving_throw("charisma") == m0.get_charisma() + m0.get_proficiency_mod()
    assert features.UnarmoredDefenseMonk in m0.get_feature_classes()
    assert "get_unarmored_ac" in m0.get_feature_methods()
    assert m0.has_feature_class(features.UnarmoredDefenseMonk)
    assert m0.has_feature_method("get_unarmored_ac")
    assert m0.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseMonk.get_unarmored_ac
    assert m0.get_unarmored_ac() == m0.get_ac() == 10 + 5 + 1

    m1 = character_classes.Monk(name="m1", copy=m0)
    assert m1.get_hit_dice() == (20, 8)
    assert m1.get_max_hp() == (8 + 4) + (5 + 4) * 19
    assert m1.has_proficiency("monk weapons")
    assert m1.has_proficiency("strength")
    assert m1.has_proficiency("dexterity")
    assert m1.has_proficiency("constitution")
    assert m1.has_proficiency("wisdom")
    assert m1.has_proficiency("intelligence")
    assert m1.has_proficiency("charisma")
    assert m1.get_proficiency_mod() == 6
    assert m1.get_martial_arts_dice() == (1, 10)
    assert m1.has_feature("unarmored defense")
    assert m1.has_feature("flurry of blows")
    assert m1.has_feature("patient defense")
    assert m1.has_feature("step of the wind")
    assert m1.has_feature("deflect missiles")
    assert m1.has_feature("slow fall")
    assert m1.has_feature("extra attack")
    assert m1.has_feature("stunning strike")
    assert m1.has_feature("ki-empowered strikes")
    assert m1.has_feature("stillness of mind")
    assert m1.has_feature("evasion")
    assert m1.has_feature("purity of body")
    assert m1.has_feature("tongue of the sun and moon")
    assert m1.has_feature("diamond soul")
    assert m1.has_feature("empty body")
    assert m1.has_feature("perfect soul")
    assert m1.get_ki_points() == 20
    assert m1.get_ki_save_dc() == 8 + 6 + 1
    assert m1.get_saving_throw("strength") == m1.get_strength() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("dexterity") == m1.get_dexterity() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("constitution") == m1.get_constitution() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("wisdom") == m1.get_wisdom() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("intelligence") == m1.get_intelligence() + m1.get_proficiency_mod()
    assert m1.get_saving_throw("charisma") == m1.get_charisma() + m1.get_proficiency_mod()
    assert m1.has_feature_class(features.UnarmoredDefenseMonk)
    assert m1.has_feature_method("get_unarmored_ac")
    assert m1.get_feature_dict()["get_unarmored_ac"] == features.UnarmoredDefenseMonk.get_unarmored_ac
    assert m1.get_unarmored_ac() == m1.get_ac() == 10 + 5 + 1
    assert m1 is not m0

    assert m0.equals(m1) and m1.equals(m1.get_copy())
    assert m0.current_eq(m1)
    assert m1.spend_ki_points(5) == 5
    assert m1.get_ki_points() == 15
    assert not m0.current_eq(m1)
    assert m0.equals(m1)

def test_rogue():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Rogue`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`take_stroke_of_luck` (NOT IMPLEMENTED YET)
        - attack with sneak attack
    :Test dependencies: :py:func:`test_combatant`
    :return: None
    """
    r0 = character_classes.Rogue(name="r0", level=20, ac=16, strength_mod=5, dexterity_mod=5, constitution_mod=3,
                                 wisdom_mod=2, intelligence_mod=0, charisma_mod=-2, logger=test_logger)
    assert r0.get_level() == 20
    assert r0.get_hit_dice() == (20, 8)
    assert r0.get_max_hp() == 8 + r0.get_constitution() + (5 + r0.get_constitution())*19
    assert r0.has_proficiency("simple weapons")
    assert r0.has_proficiency("hand crossbow")
    assert r0.has_proficiency("longsword")
    assert r0.has_proficiency("rapier")
    assert r0.has_proficiency("shortsword")
    assert r0.has_proficiency("dexterity")
    assert r0.has_proficiency("intelligence")
    assert r0.get_proficiency_mod() == 6
    assert r0.get_sneak_attack_dice() == (10, 6)
    assert r0.has_feature("sneak attack")
    assert r0.has_feature("cunning action")
    assert r0.has_feature("uncanny dodge")
    assert r0.has_feature("evasion")
    assert r0.has_feature("blindsense")
    assert r0.has_feature("slippery mind")
    assert r0.has_proficiency("wisdom")
    assert r0.get_saving_throw("wisdom") == r0.get_wisdom() + r0.get_proficiency_mod()
    assert r0.has_feature("elusive")
    assert r0.has_feature("stroke of luck")
    assert r0.get_stroke_of_luck_slots() == 1

    assert r0.can_see("magic")
    r0.add_condition("deafened")
    assert not r0.can_see("magic")
    r0.remove_condition("deafened")

    r0.modify_adv_to_be_hit(1)
    assert r0.get_adv_to_be_hit() == 0
    r0.add_condition("incapacitated")
    assert r0.get_adv_to_be_hit() == 1
    r0.remove_condition("incapacitated")

    r1 = character_classes.Rogue(copy=r0, name="r1")

    assert r1.get_max_hp() == 8 + r1.get_constitution() + (5 + r1.get_constitution()) * 19

    assert r1.has_proficiency("simple weapons")
    assert r1.has_proficiency("hand crossbow")
    assert r1.has_proficiency("longsword")
    assert r1.has_proficiency("rapier")
    assert r1.has_proficiency("shortsword")
    assert r1.has_proficiency("dexterity")
    assert r1.has_proficiency("intelligence")
    assert r1.get_proficiency_mod() == 6

    assert r1.get_sneak_attack_dice() == (10, 6)
    assert r1.has_feature("sneak attack")
    assert r1.has_feature("cunning action")
    assert r1.has_feature("uncanny dodge")
    assert r1.has_feature("evasion")
    assert r1.has_feature("blindsense")
    assert r1.has_feature("slippery mind")
    assert r1.has_proficiency("wisdom")
    assert r1.get_saving_throw("wisdom") == r1.get_wisdom() + r1.get_proficiency_mod()
    assert r1.has_feature("elusive")
    assert r1.has_feature("stroke of luck")
    assert r1.get_stroke_of_luck_slots() == 1

    assert r1 is not r0
    assert r0.equals(r1) and r0.equals(r0.get_copy())
    assert r0.current_eq(r1)
    r1.take_stroke_of_luck()
    assert r1.get_stroke_of_luck_slots() == 0
    assert not r0.current_eq(r1)
    assert r0.equals(r1)

    # test blindsense
    assert r1.can_see("magic")
    r1.add_condition("deafened")
    assert not r1.can_see("magic")
    r1.remove_condition("deafened")

    r1.modify_adv_to_be_hit(1)
    assert r1.get_adv_to_be_hit() == 0
    r1.add_condition("incapacitated")
    assert r1.get_adv_to_be_hit() == 1
    r1.remove_condition("incapacitated")

def test_spellcaster():
    """
    :Test module: :py:mod:`combatant`
    :Test class: :py:class:`SpellCaster`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: invalid input to constructor
    :Test dependencies:
        - :py:func:`test_combatant`
        - :py:func:`test_spell`
    :return: None
    """
    spell0 = attack_class.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, name="spell0")

    s0 = combatant.SpellCaster(ac=18, max_hp=70, current_hp=60, temp_hp=2, hit_dice='4d10', speed=30, vision='darkvision',
                               strength=14, dexterity=11, constitution=9, intelligence=12, wisdom=16, charisma=8, name="s0",
                               spell_ability="wisdom", spell_slots={1:3, 2:2, 3:1}, proficiency_mod=2, spells=[spell0],
                               logger=test_logger)
    assert s0.get_spell_ability() == "wisdom"
    assert s0.get_spell_ability_mod() == 3
    assert s0.get_spell_save_dc() == 13
    assert s0.get_spell_attack_mod() == 5
    assert s0.get_level_spell_slots(1) == 3
    assert s0.get_level_spell_slots(2) == 2
    assert s0.get_level_spell_slots(3) == 1
    assert s0.get_level_spell_slots(4) == 0
    s0.spend_spell_slot(3)
    assert not s0.get_level_spell_slots(3)
    s0.reset_spell_slots()
    assert s0.get_spell_slots() == {1:3, 2:2, 3:1}
    assert spell0 in s0.get_spells()
    assert spell0.get_attack_mod() == 5

    s1 = combatant.SpellCaster(copy=s0)
    assert s1.get_spell_ability() == "wisdom"
    assert s1.get_spell_ability_mod() == 3
    assert s1.get_spell_save_dc() == 13
    assert s1.get_spell_attack_mod() == 5
    assert s1.get_level_spell_slots(1) == 3
    assert s1.get_level_spell_slots(2) == 2
    assert s1.get_level_spell_slots(3) == 1
    assert s1.get_level_spell_slots(4) == 0
    assert s1 is not s0
    assert s1.equals(s0) and s0.equals(s1.get_copy())
    assert s1.get_spells() is not s0.get_spells()

    s1.spend_spell_slot(3)
    assert not s1.get_level_spell_slots(3)
    assert not s1.current_eq(s0)

    s1.reset_spell_slots()
    assert s1.get_spell_slots() == {1: 3, 2: 2, 3: 1}
    assert s1.get_spells()[0].get_attack_mod() == 5

def test_bard():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Bard`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`send_inspiration`
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    b0 = character_classes.Bard(name="b0", ac=14, strength_mod=-3, dexterity_mod=3, constitution_mod=1, intelligence_mod=0,
                                wisdom_mod=1, charisma_mod=4, level=13, logger=test_logger)
    assert b0.get_level() == 13
    assert b0.get_hit_dice() == (13, 8)
    assert b0.get_max_hp() == 8 + 1 + (5+1)*12
    assert b0.has_proficiency("simple weapons")
    assert b0.has_proficiency("hand crossbow")
    assert b0.has_proficiency("longsword")
    assert b0.has_proficiency("shortsword")
    assert b0.has_proficiency("rapier")
    assert b0.has_proficiency("dexterity")
    assert b0.has_proficiency("charisma")
    assert b0.get_proficiency_mod() == 5
    assert b0.get_spell_ability() == "charisma"
    assert b0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
    assert b0.has_feature("countercharm")
    assert b0.get_inspiration_dice() == (1, 10)
    assert b0.get_inspiration_slots() == 4

    b1 = character_classes.Bard(name="b1", copy=b0)
    assert b1.get_level() == 13
    assert b1.get_hit_dice() == (13, 8)
    assert b1.get_max_hp() == 8 + 1 + (5 + 1) * 12
    assert b1.has_proficiency("simple weapons")
    assert b1.has_proficiency("hand crossbow")
    assert b1.has_proficiency("longsword")
    assert b1.has_proficiency("shortsword")
    assert b1.has_proficiency("rapier")
    assert b1.has_proficiency("dexterity")
    assert b1.has_proficiency("charisma")
    assert b1.get_proficiency_mod() == 5
    assert b1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
    assert b1.has_feature("countercharm")
    assert b1.get_inspiration_dice() == (1, 10)
    assert b1.get_inspiration_slots() == 4
    assert b1 is not b0

    assert b0.equals(b1) and b1.equals(b0.get_copy())
    assert b0.current_eq(b1)
    b1.send_inspiration(b0)
    assert b1.get_inspiration_slots() == 3
    assert not b0.current_eq(b1)
    assert b0.equals(b1)

def test_cleric():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Cleric`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: invalid input to constructor
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    c0 = character_classes.Cleric(name="c0", ac=18, strength_mod=0, dexterity_mod=1, constitution_mod=1, wisdom_mod=3,
                                  intelligence_mod=2, charisma_mod=-2, level=7, logger=test_logger)
    assert c0.get_level() == 7
    assert c0.get_hit_dice() == (7, 8)
    assert c0.get_max_hp() == 8 + 1 + (5+1)*6
    assert c0.has_proficiency("simple weapons")
    assert c0.has_proficiency("wisdom")
    assert c0.has_proficiency("charisma")
    assert c0.get_proficiency_mod() == 3
    assert c0.get_spell_ability() == "wisdom"
    assert c0.get_level_spell_slots(1) == 4
    assert c0.get_level_spell_slots(2) == 3
    assert c0.get_level_spell_slots(3) == 3
    assert c0.get_level_spell_slots(4) == 2
    assert c0.has_feature("channel divinity")
    assert c0.get_channel_divinity_slots() == 2
    assert c0.has_feature("destroy undead")
    assert c0.get_destroy_undead_cr() == 0.5

    c1 = character_classes.Cleric(name="c1", copy=c0)
    assert c1.get_level() == 7
    assert c1.get_hit_dice() == (7, 8)
    assert c1.get_max_hp() == 8 + 1 + (5 + 1) * 6
    assert c1.has_proficiency("simple weapons")
    assert c1.has_proficiency("wisdom")
    assert c1.has_proficiency("charisma")
    assert c1.get_proficiency_mod() == 3
    assert c1.get_level_spell_slots(1) == 4
    assert c1.get_level_spell_slots(2) == 3
    assert c1.get_level_spell_slots(3) == 3
    assert c1.get_level_spell_slots(4) == 2
    assert c1.has_feature("channel divinity")
    assert c1.get_channel_divinity_slots() == 2
    assert c1.has_feature("destroy undead")
    assert c1.get_destroy_undead_cr() == 0.5
    assert c1 is not c0

    assert c0.equals(c1) and c1.equals(c0.get_copy())
    assert c0.current_eq(c1)
    c1.channel_divinity(use_type="channel undead")
    assert c1.get_channel_divinity_slots() == 1
    assert c1.equals(c0)
    assert not c1.current_eq(c0)

def test_druid():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Druid`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - everything involved with changing into a wild shape
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    d0 = character_classes.Druid(ac=14, level=5, strength_mod=-1, dexterity_mod=1, constitution_mod=1,
                                 wisdom_mod=3, intelligence_mod=0, charisma_mod=-1, name="d0", logger=test_logger)
    assert d0.get_level() == 5
    assert d0.get_hit_dice() == (5, 8)
    assert d0.get_max_hp() == 8 + 1 + (5 + 1)*4
    assert d0.has_proficiency("club")
    assert d0.has_proficiency("dagger")
    assert d0.has_proficiency("javelin")
    assert d0.has_proficiency("mace")
    assert d0.has_proficiency("quarterstaff")
    assert d0.has_proficiency("scimitar")
    assert d0.has_proficiency("sickle")
    assert d0.has_proficiency("sling")
    assert d0.has_proficiency("spear")
    assert d0.has_proficiency("intelligence")
    assert d0.has_proficiency("wisdom")
    assert d0.get_proficiency_mod() == 3
    assert d0.get_spell_ability() == "wisdom"
    assert d0.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert d0.get_wild_shape_slots() == 2
    assert d0.get_wild_shapes() == []
    assert d0.get_current_shape() is None

    d1 = character_classes.Druid(copy=d0, name="d1")
    assert d1.get_level() == 5
    assert d1.get_hit_dice() == (5, 8)
    assert d1.get_max_hp() == 8 + 1 + (5 + 1) * 4
    assert d1.has_proficiency("club")
    assert d1.has_proficiency("dagger")
    assert d1.has_proficiency("javelin")
    assert d1.has_proficiency("mace")
    assert d1.has_proficiency("quarterstaff")
    assert d1.has_proficiency("scimitar")
    assert d1.has_proficiency("sickle")
    assert d1.has_proficiency("sling")
    assert d1.has_proficiency("spear")
    assert d1.has_proficiency("intelligence")
    assert d1.has_proficiency("wisdom")
    assert d1.get_proficiency_mod() == 3
    assert d1.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert d1.get_wild_shape_slots() == 2
    assert d1.get_wild_shapes() == []
    assert d1.get_current_shape() is None
    assert d1 is not d0

    assert d0.current_eq(d1)
    d0.add_wild_shape(bestiary.Aboleth())
    assert not d0.current_eq(d1)

def test_paladin():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Paladin`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`spend_divine_sense_slot`
        - :py:meth:`spend_cleansing_touch_slot`
        - :py:meth:`send_lay_on_hands`
        - :py:meth:`send_divine_smite`
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    p0 = character_classes.Paladin(name="p0", level=14, ac=18, strength_mod=1, dexterity_mod=-2, constitution_mod=3,
                                   wisdom_mod=2, intelligence_mod=0, charisma_mod=4, fighting_style="archery",
                                   logger=test_logger)
    assert p0.get_level() == 14
    assert p0.get_hit_dice() == (14, 10)
    assert p0.get_max_hp() == (10 + 3 + (6+3)*13)
    assert p0.has_proficiency("simple weapons")
    assert p0.has_proficiency("martial weapons")
    assert p0.has_proficiency("wisdom")
    assert p0.has_proficiency("charisma")
    assert p0.get_proficiency_mod() == 5
    assert p0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1}
    assert p0.has_feature("divine sense")
    assert p0.get_divine_sense_slots() == 1 + 4
    assert p0.has_feature("lay on hands")
    assert p0.get_lay_on_hands_pool() == 14 * 5
    assert p0.has_feature("divine smite")
    assert p0.has_feature("divine health")
    assert p0.has_feature("sacred oath")
    assert p0.has_feature("extra attack")
    assert p0.has_feature("aura of protection")
    assert p0.has_feature("aura of courage")
    assert p0.get_aura() == 10
    assert p0.has_feature("improved divine smite")
    assert p0.has_feature("cleansing touch")
    assert p0.get_cleansing_touch_slots() == 4

    p1 = character_classes.Paladin(copy=p0, name="p1")
    assert p1.get_level() == 14
    assert p1.get_hit_dice() == (14, 10)
    assert p1.get_max_hp() == (10 + 3 + (6 + 3) * 13)
    assert p1.has_proficiency("simple weapons")
    assert p1.has_proficiency("martial weapons")
    assert p1.has_proficiency("wisdom")
    assert p1.has_proficiency("charisma")
    assert p1.get_proficiency_mod() == 5
    assert p1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 1}
    assert p1.has_feature("divine sense")
    assert p1.get_divine_sense_slots() == 1 + 4
    assert p1.has_feature("lay on hands")
    assert p1.get_lay_on_hands_pool() == 14 * 5
    assert p1.has_feature("divine smite")
    assert p1.has_feature("divine health")
    assert p1.has_feature("sacred oath")
    assert p1.has_feature("extra attack")
    assert p1.has_feature("aura of protection")
    assert p1.has_feature("aura of courage")
    assert p1.get_aura() == 10
    assert p1.has_feature("improved divine smite")
    assert p1.has_feature("cleansing touch")
    assert p1.get_cleansing_touch_slots() == 4
    assert p1 is not p0

    assert p0.equals(p1) and p0.equals(p0.get_copy())
    assert p0.current_eq(p1)
    p1.spend_cleansing_touch_slot()
    assert p1.get_cleansing_touch_slots() == 3
    assert not p0.current_eq(p1)
    assert p0.equals(p1)

def test_ranger():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Ranger`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`has_favored_enemy` with a :py:class:`Creature` as input
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    r0 = character_classes.Ranger(name="r0", level=9, ac=16, strength_mod=3, dexterity_mod=2, constitution_mod=2, wisdom_mod=1,
                                  intelligence_mod=-1, charisma_mod=-2, favored_enemies=("beast", "undead"),
                                  favored_terrains=("forest", "swamp"), fighting_style="defense", logger=test_logger)
    assert r0.get_level() == 9
    assert r0.get_hit_dice() == (9, 10)
    assert r0.get_max_hp() == 10 + 2 + (6+2)*8
    assert r0.has_proficiency("simple weapons")
    assert r0.has_proficiency("martial weapons")
    assert r0.has_proficiency("strength")
    assert r0.has_proficiency("dexterity")
    assert r0.get_proficiency_mod() == 4
    assert r0.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert r0.has_feature("favored enemy")
    assert r0.has_favored_enemy("beast")
    assert r0.has_favored_enemy("undead")
    assert r0.has_feature("favored terrain")
    assert r0.has_favored_terrain("forest")
    assert r0.has_favored_terrain("swamp")
    assert r0.has_feature("fighting style")
    assert r0.has_fighting_style("defense")
    assert r0.has_feature("ranger archetype")
    assert r0.has_feature("primeval awareness")
    assert r0.has_feature("extra attack")
    assert r0.has_feature("land's stride")

    r1 = character_classes.Ranger(copy=r0, name="r1")
    assert r1.get_level() == 9
    assert r1.get_hit_dice() == (9, 10)
    assert r1.get_max_hp() == 10 + 2 + (6 + 2) * 8
    assert r1.has_proficiency("simple weapons")
    assert r1.has_proficiency("martial weapons")
    assert r1.has_proficiency("strength")
    assert r1.has_proficiency("dexterity")
    assert r1.get_proficiency_mod() == 4
    assert r1.get_spell_slots() == {1: 4, 2: 3, 3: 2}
    assert r1.has_feature("favored enemy")
    assert r1.has_favored_enemy("beast")
    assert r1.has_favored_enemy("undead")
    assert r1.get_favored_enemies() is not r0.get_favored_enemies()
    assert r1.has_feature("favored terrain")
    assert r1.has_favored_terrain("forest")
    assert r1.has_favored_terrain("swamp")
    assert r1.get_favored_terrains() is not r0.get_favored_terrains()
    assert r1.has_feature("fighting style")
    assert r1.has_fighting_style("defense")
    assert r1.has_feature("ranger archetype")
    assert r1.has_feature("primeval awareness")
    assert r1.has_feature("extra attack")
    assert r1.has_feature("land's stride")
    assert r1 is not r0

    assert r0.equals(r1) and r0.equals(r0.get_copy())

def test_sorcerer():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Sorcerer`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - Draconic Resilience feature (because that's for a Draconic archetype of Sorcerer)
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    s0 = character_classes.Sorcerer(name="s0", level=20, ac=12, strength_mod=-1, dexterity_mod=3, constitution_mod=2,
                                    wisdom_mod=1, intelligence_mod=-1, charisma_mod=3,
                                    metamagic=["careful", "distant", "empowered"], logger=test_logger)
    assert s0.get_level() == 20
    assert s0.get_hit_dice() == (20, 6)
    assert s0.get_max_hp() == 6 + s0.get_constitution() + (4 + s0.get_constitution())*19
    assert s0.has_proficiency("dagger")
    assert s0.has_proficiency("dart")
    assert s0.has_proficiency("sling")
    assert s0.has_proficiency("quarterstaff")
    assert s0.has_proficiency("light crossbow")
    assert s0.has_proficiency("constitution")
    assert s0.has_proficiency("charisma")
    assert s0.get_spell_ability() == "charisma"
    assert s0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert s0.has_feature("font of magic")
    assert s0.get_sorcery_points() == s0.get_full_sorcery_points() == 20
    assert s0.has_feature("metamagic")
    assert s0.has_metamagic("careful")
    assert s0.has_metamagic("distant")
    assert s0.has_metamagic("empowered")

    s1 = character_classes.Sorcerer(copy=s0, name="s1")
    assert s1.get_level() == 20
    assert s1.get_max_hp() == 6 + s1.get_constitution() + (4 + s1.get_constitution()) * 19
    assert s1.has_proficiency("dagger")
    assert s1.has_proficiency("dart")
    assert s1.has_proficiency("sling")
    assert s1.has_proficiency("quarterstaff")
    assert s1.has_proficiency("light crossbow")
    assert s1.has_proficiency("constitution")
    assert s1.has_proficiency("charisma")
    assert s1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert s1.has_feature("font of magic")
    assert s1.get_sorcery_points() == s1.get_full_sorcery_points() == 20
    assert s1.has_feature("metamagic")
    assert s1.has_metamagic("careful")
    assert s1.has_metamagic("distant")
    assert s1.has_metamagic("empowered")
    assert s1.get_metamagic() is not s0.get_metamagic()
    assert s1 is not s0

    assert s0.equals(s1) and s0.equals(s0.get_copy())
    assert s0.current_eq(s1)
    assert s1.sorcery_points_to_spell_slot(2) == 3
    assert s1.get_sorcery_points() == 17
    assert s1.get_level_spell_slots(2) == 4
    assert not s0.current_eq(s1)
    assert s0.equals(s1)

    s1.spend_spell_slot(2)
    assert s1.get_level_spell_slots(2) == 3
    assert not s0.current_eq(s1)
    assert s1.spend_sorcery_points(5) == 5
    assert s1.get_sorcery_points() == 12  # 17 - 5 == 12

    s1.spell_slot_to_sorcery_points(7)
    assert s1.get_sorcery_points() == 19  # 12 + 7 == 19
    assert s1.get_level_spell_slots(7) == 1  # 2 - 1 == 1

    s1.reset_sorcery_points()
    assert s1.get_sorcery_points() == s1.get_full_sorcery_points() == 20

    try:
        s1.spend_sorcery_points(0)
        raise Exception("Spent an invalid amount of sorcery points")
    except ValueError:
        pass

    try:
        s1.spend_sorcery_points(30)
        raise Exception("Spent more sorcery points than you have")
    except ValueError:
        pass

    s3 = character_classes.Sorcerer(name="s3", level=1, ac=12, strength_mod=-1, dexterity_mod=3, constitution_mod=2,
                                    wisdom_mod=1, intelligence_mod=-1, charisma_mod=3)
    try:
        s3.reset_sorcery_points()
        raise Exception("Allowed Sorcerer without sorcery points to reset sorcery points")
    except ValueError:
        pass

    try:
        s3.spend_sorcery_points(2)
        raise Exception("Allowed Sorcerer without sorcery points to spend sorcery points")
    except ValueError:
        pass

    try:
        s3.spell_slot_to_sorcery_points(3)
        raise Exception("Allowed Sorcerer without sorcery points to convert spell slot to sorcery points")
    except ValueError:
        pass

    try:
        s1.sorcery_points_to_spell_slot(6)
        raise Exception("Converted sorcery points to spell slot of too high a level")
    except ValueError:
        pass

def test_warlock():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Warlock`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: invalid input to constructor
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    w0 = character_classes.Warlock(name="w0", level=20, ac=16, strength_mod=1, dexterity_mod=3, constitution_mod=2,
                                   wisdom_mod=1, intelligence_mod=-1, charisma_mod=3, pact_boon="blade",
                                   eldritch_invocations=["agonizing blast", "armor of shadows", "ascendant step",
                                                         "beast speech", "beguiling influence", "bewitching whispers",
                                                         "book of ancient secrets", "chains of carcerei"],
                                   logger=test_logger)
    assert w0.get_level() == 20
    assert w0.get_hit_dice() == (20, 8)
    assert w0.get_max_hp() == 8 + w0.get_constitution() + (5 + w0.get_constitution())*19
    assert w0.has_proficiency("simple weapons")
    assert w0.has_proficiency("wisdom")
    assert w0.has_proficiency("charisma")
    assert w0.get_proficiency_mod() == 6
    assert w0.get_spell_ability() == "charisma"
    assert w0.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1}
    assert w0.has_feature("pact boon")
    assert w0.get_pact_boon() == "blade"
    assert w0.has_feature("eldritch invocations")
    assert w0.get_eldritch_invocations() == set(["agonizing blast", "armor of shadows", "ascendant step",
            "beast speech", "beguiling influence", "bewitching whispers", "book of ancient secrets", "chains of carcerei"])
    assert w0.has_eldritch_invocation("chains of carcerei")

    w1 = character_classes.Warlock(copy=w0, name="w1")
    assert w1.get_level() == 20
    assert w1.get_max_hp() == 8 + w1.get_constitution() + (5 + w1.get_constitution()) * 19
    assert w1.has_proficiency("simple weapons")
    assert w1.has_proficiency("wisdom")
    assert w1.has_proficiency("charisma")
    assert w1.get_proficiency_mod() == 6
    assert w1.get_spell_slots() == {5: 4, 6: 1, 7: 1, 8: 1, 9: 1}
    assert w1.has_feature("pact boon")
    assert w1.get_pact_boon() == "blade"
    assert w1.has_feature("eldritch invocations")
    assert w1.get_eldritch_invocations() == set(["agonizing blast", "armor of shadows", "ascendant step",
                                                  "beast speech", "beguiling influence", "bewitching whispers",
                                                  "book of ancient secrets", "chains of carcerei"])
    assert w1.has_eldritch_invocation("chains of carcerei")
    assert w1.get_eldritch_invocations() is not w0.get_eldritch_invocations()
    assert w1 is not w0

    assert w0.equals(w1) and w0.equals(w0.get_copy())

def test_wizard():
    """
    :Test module: :py:mod:`character_classes`
    :Test class: :py:class:`Wizard`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - :py:meth:`has_spell_mastery` and :py:meth:`has_signature_spell` with Spells
        - :py:meth:`spend_spell_slot`
    :Test dependencies:
        - :py:func:`test_spellcaster`
        - :py:func:`test_character`
    :return: None
    """
    w0 = character_classes.Wizard(name="w0", level=20, ac=12, strength_mod=-2, dexterity_mod=1, constitution_mod=1,
                                  wisdom_mod=3, intelligence_mod=3, charisma_mod=-1, spell_mastery=["a", "b"],
                                  signature_spells=["c", "d"], logger=test_logger)
    assert w0.get_level() == 20
    assert w0.get_hit_dice() == (20, 6)
    assert w0.get_max_hp() == 6 + w0.get_constitution() + (4 + w0.get_constitution())*19
    assert w0.has_proficiency("dagger")
    assert w0.has_proficiency("dart")
    assert w0.has_proficiency("sling")
    assert w0.has_proficiency("light crossbow")
    assert w0.has_proficiency("intelligence")
    assert w0.has_proficiency("wisdom")
    assert w0.get_proficiency_mod() == 6
    assert w0.get_spell_ability() == "intelligence"
    assert w0.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert w0.has_feature("spell mastery")
    assert w0.has_spell_mastery("a")
    assert w0.has_spell_mastery("b")
    assert w0.has_feature("signature spells")
    assert w0.has_signature_spell("c")
    assert w0.has_signature_spell("d")
    assert w0.get_signature_spells() == ["c", "d"]

    w1 = character_classes.Wizard(copy=w0, name="w1")
    assert w1.get_level() == 20
    assert w1.get_hit_dice() == (20, 6)
    assert w1.get_max_hp() == 6 + w1.get_constitution() + (4 + w1.get_constitution()) * 19
    assert w1.has_proficiency("dagger")
    assert w1.has_proficiency("dart")
    assert w1.has_proficiency("sling")
    assert w1.has_proficiency("light crossbow")
    assert w1.has_proficiency("intelligence")
    assert w1.has_proficiency("wisdom")
    assert w1.get_proficiency_mod() == 6
    assert w1.get_spell_ability() == "intelligence"
    assert w1.get_spell_slots() == {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}
    assert w1.get_spell_slots() is not w0.get_spell_slots()
    assert w1.has_feature("spell mastery")
    assert w1.has_spell_mastery("a")
    assert w1.has_spell_mastery("b")
    assert w1.has_feature("signature spells")
    assert w1.has_signature_spell("c")
    assert w1.has_signature_spell("d")
    assert w1.get_signature_spells() == ["c", "d"]
    assert w1.get_signature_spell_slots() is not w0.get_signature_spell_slots()
    assert w1 is not w0

    assert w0.equals(w1) and w0.equals(w0.get_copy())
    assert w0.current_eq(w1)
    spell_c = attack_class.Spell(level=1, casting_time="1 minute", components=("v", "s"), duration="instantaneous",
                                school="magic", damage_dice=(1, 8), range=60, name="c")
    w1.spend_spell_slot(1, spell=spell_c)
    assert w1.get_signature_spell_slots()["c"] == 0
    assert not w0.current_eq(w1)
    assert w0.equals(w1)

def test_features():
    """
    Test the Features that require success of more than one other test method.

    :Test module: :py:mod:`features`
    :Test class: :py:class:`FastMovementBarbarian`, :py:class:`ArcheryFightingStyle`, :py:class:`DefenseFightingStyle`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies: :py:func:`test_barbarian`, :py:func:`test_armory`
    :return: None
    """
    barb = character_classes.Barbarian(name="barb", armor=armor.PlateArmor, strength_mod=4, dexterity_mod=4,
                                       constitution_mod=4, wisdom_mod=-2, intelligence_mod=-2, charisma_mod=-3,
                                       speed=30, level=20, logger=test_logger)
    assert barb.get_speed() == barb._speed  # pylint: disable=protected-access
    assert barb.get_speed() == 30, barb.get_speed()  # wearing Heavy Armor, so no Fast Movement
    barb.set_armor(None)
    assert barb.get_speed() == 30 + 10  # Fast Movement
    barb.set_armor(armor.LeatherArmor)
    assert barb.get_speed() == 30 + 10  # Fast Movement
    barb.set_armor(armor.SplintArmor)
    assert barb.get_speed() == 30

    archer = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength_mod=2, dexterity_mod=3, constitution_mod=-1, intelligence_mod=1, wisdom_mod=0,
                             charisma_mod=-1, proficiency_mod=2, proficiencies=["shortbow"],
                             name='archer', feature_classes=[features.ArcheryFightingStyle], logger=test_logger)
    assert archer.get_weapon_attack_mod(armory.Shortbow()) == 3 + 2 + 2
    # TODO: more tests once the attack mod method is implemented for adding weapon attacks

    defender = combatant.Combatant(max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength_mod=2, dexterity_mod=3, constitution_mod=-1, intelligence_mod=1, wisdom_mod=0,
                             charisma_mod=-1, proficiency_mod=2, proficiencies=["shortbow"],
                             name='archer', feature_classes=[features.DefenseFightingStyle], logger=test_logger)
    assert defender.get_ac() == 10 + 3
    defender.set_armor(armor.StuddedLeatherArmor)
    assert defender.get_ac() == 12 + 3 + 1

def test_tactics():
    """
    :Test module: :py:mod:`tactics`
    :Test class: :py:class:`Tactic`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts:
        - invalid input to constructor
        - include tiebreakers (but this is tested in :py:func:`test_combatant_tactics`)
    :Test dependencies: None
    :return: None
    """
    rand_tactic = tactics.Tactic(name="random")
    assert rand_tactic.get_name() == "random"
    assert rand_tactic.get_tiebreakers() == []

    rand_tactic2 = tactics.Tactic(copy=rand_tactic, name="random2")
    assert rand_tactic2.get_name() == "random2"
    assert rand_tactic2.get_tiebreakers() == []
    assert rand_tactic is not rand_tactic2
    assert rand_tactic2 == rand_tactic

def test_combatant_tactics():
    """
    :Test module: :py:mod:`combatant_tactics`
    :Test class:
        :py:class:`LowestAcTactic`, :py:class:`HighestAcTactic`, :py:class:`LowAcTactic`, :py:class:`HighAcTactic`,
        :py:class:`BloodiedTactic`, :py:class:`MaxHpTactic`, :py:class:`HpToMaxHighTactic`,
        :py:class:`MurderHoboTactic`, :py:class:`HasTempHpTactic`, :py:class:`NoTempHsTactic`,
        :py:class:`HasVulnerabilityTactic`, :py:class:`HasNoVulnerabilityTactic`, :py:class:`HasResistanceTactic`,
        :py:class:`HasNoResistanceTactic`, :py:class:`HasImmunityTactic`, :py:class:`HasNoImmunityTactic`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: invalid input to constructor
    :Test dependencies:
        - :py:func:`test_tactics`
        - :py:func:`test_combatant`
    :return: None
    """
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()
    c10 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    d10 = combatant.Combatant(copy=c10, name="d10")
    c11 = combatant.Combatant(ac=11, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    c12 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    test_list = [c10, c11, c12]
    assert lowest_ac_tactic.run_tactic(test_list) == [c10]
    c11.set_enemy_tactic(lowest_ac_tactic)
    assert c11.select_enemy(test_list) == c10
    test_list = [c10, d10, c11, c12]
    assert lowest_ac_tactic.run_tactic(test_list) == [c10, d10]
    assert lowest_ac_tactic == combatant_tactics.LowestAcTactic()

    highest_ac_tactic = combatant_tactics.HighestAcTactic()
    assert highest_ac_tactic.run_tactic(test_list) == [c12]
    d12 = combatant.Combatant(copy=c12, name="d12")
    test_list = [c10, d10, c11, c12, d12]
    assert highest_ac_tactic.run_tactic(test_list) == [c12, d12]
    assert highest_ac_tactic == combatant_tactics.HighestAcTactic()

    test_list = [c10, d10, c11, c12, d12]
    low_ac_tactic = combatant_tactics.LowAcTactic(threshold=11)
    low_ac_result = low_ac_tactic.run_tactic(test_list)
    assert len(low_ac_result) == 2 and c10 in low_ac_result and d10 in low_ac_result
    assert low_ac_tactic.get_copy() == low_ac_tactic
    low_ac_tactic2 = combatant_tactics.LowAcTactic(threshold=12)
    assert low_ac_tactic2 != low_ac_tactic
    assert low_ac_tactic2.run_tactic(test_list) == [c10, d10, c11]
    a0 = attack_class.Attack(damage_dice=(1, 8), attack_mod=5, damage_mod=2, damage_type="piercing", range=120, adv=-1,
                             melee_range=10, name="a0")
    assert combatant_tactics.LowAcTactic(attack=a0, use_max=True).get_threshold() == 25 + 1
    assert combatant_tactics.LowAcTactic(attack=a0).get_threshold() == 12 + 1

    test_list = [c10, d10, c11, c12, d12]
    high_ac_tactic = combatant_tactics.HighAcTactic(threshold=10)
    assert high_ac_tactic.run_tactic(test_list) == [c11, c12, d12]
    assert high_ac_tactic.get_copy() == high_ac_tactic
    high_ac_tactic2 = combatant_tactics.HighAcTactic(threshold=11)
    assert high_ac_tactic2 != high_ac_tactic
    assert high_ac_tactic2.run_tactic(test_list) == [c12, d12]

    bloodied_tactic = combatant_tactics.BloodiedTactic()
    c10 = combatant.Combatant(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c10", logger=test_logger)
    d10 = combatant.Combatant(copy=c10, name="d10")
    c11 = combatant.Combatant(ac=10, max_hp=30, current_hp=8, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c11", logger=test_logger)
    c12 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c12",
                             logger=test_logger)
    test_list = [c10, c12]
    assert bloodied_tactic.run_tactic(test_list) == [c10]
    test_list = [c10, d10, c11, c12]
    assert bloodied_tactic.run_tactic(test_list) == [c10, d10, c11]
    assert bloodied_tactic == combatant_tactics.BloodiedTactic()

    co20 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    co10 = combatant.Combatant(ac=10, max_hp=20, current_hp=10, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    co5 = combatant.Combatant(ac=10, max_hp=20, current_hp=5, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    co2 = combatant.Combatant(ac=10, max_hp=20, current_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="c10",
                             logger=test_logger)
    test_list = [co2, co5, co10, co20]
    lowest_hp_tactic = combatant_tactics.LowestHpTactic()
    assert lowest_hp_tactic.run_tactic(test_list) == [co2]
    co2_copy = co2.get_copy()
    assert lowest_hp_tactic.run_tactic([co2, co2_copy, co5]) == [co2, co2_copy]
    assert lowest_hp_tactic == combatant_tactics.LowestHpTactic()
    co10.set_heal_tactic(lowest_hp_tactic)
    assert co10.select_heal(test_list) == co2

    test_list = [co2, co5, co10, co20]
    highest_hp_tactic = combatant_tactics.HighestHpTactic()
    assert highest_hp_tactic.run_tactic(test_list) == [co20]
    co20_copy = co20.get_copy()
    highest_hp_result = highest_hp_tactic.run_tactic([co20, co20_copy, co10])
    assert len(highest_hp_result) == 2 and co20 in highest_hp_result and co20_copy in highest_hp_result
    assert highest_hp_tactic == combatant_tactics.HighestHpTactic()

    test_list = [co2, co5, co10, co20]
    hp_low_tactic = combatant_tactics.LowHpTactic(threshold=15)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 3 and co2 in hp_low_result and co5 in hp_low_result and co10 in hp_low_result
    a0 = attack_class.Attack(damage_dice=(1, 10), damage_type="slashing", range=120, name="a0")
    hp_low_tactic = combatant_tactics.LowHpTactic(attack=a0)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 2 and co2 in hp_low_result and co5 in hp_low_result
    hp_low_tactic = combatant_tactics.LowHpTactic(attack=a0, use_max=True)
    hp_low_result = hp_low_tactic.run_tactic(test_list)
    assert len(hp_low_result) == 3 and co2 in hp_low_result and co5 in hp_low_result and co10 in hp_low_result

    hp_high_tactic = combatant_tactics.HighHpTactic(threshold=15)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 1 and co20 in hp_high_result
    a0 = attack_class.Attack(damage_dice=(1, 10), damage_type="slashing", range=120, name="a0")
    hp_high_tactic = combatant_tactics.HighHpTactic(attack=a0)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 2 and co10 in hp_high_result and co20 in hp_high_result
    hp_high_tactic = combatant_tactics.HighHpTactic(attack=a0, use_max=True)
    hp_high_result = hp_high_tactic.run_tactic(test_list)
    assert len(hp_high_result) == 1 and co20 in hp_high_result

    test_list = [c10, c12]
    max_hp_tactic = combatant_tactics.MaxHpTactic()
    max_hp_result = max_hp_tactic.run_tactic(test_list)
    assert max_hp_result == [c12], max_hp_result
    d12 = combatant.Combatant(ac=12, max_hp=35, current_hp=35, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8, name="d12",
                             logger=test_logger)
    test_list = [c10, d10, c11, c12, d12]
    max_hp_result = max_hp_tactic.run_tactic(test_list)
    assert len(max_hp_result) == 2 and c12 in max_hp_result and d12 in max_hp_result
    assert max_hp_tactic == combatant_tactics.MaxHpTactic()

    test_list = [c10, d10, c11, c12, d12]
    hp_to_max_high_tactic = combatant_tactics.HighHpToMaxTactic(threshold=9)
    hp_to_max_high_result = hp_to_max_high_tactic.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 3 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result and c11 in hp_to_max_high_result
    healing_spell = attack_class.HealingSpell(damage_dice=(1, 8), damage_mod=1, name="healing", level=1, casting_time="1 action",
                                              duration="1 minute", components=["v", "s"])
    hp_to_max_high_tactic2 = combatant_tactics.HighHpToMaxTactic(healing=healing_spell)
    assert hp_to_max_high_tactic2 == hp_to_max_high_tactic2.get_copy()
    assert hp_to_max_high_tactic2 != hp_to_max_high_tactic
    c13 = combatant.Combatant(ac=12, max_hp=35, current_hp=30, hit_dice='3d6', speed=20, strength=14, dexterity=16,
                              constitution=9, intelligence=12, wisdom=11, charisma=8, name="c13", logger=test_logger)
    test_list = [c10, d10, c11, c12, d12, c13]
    hp_to_max_high_result = hp_to_max_high_tactic2.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 3 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result and c11 in hp_to_max_high_result
    hp_to_max_high_tactic2 = combatant_tactics.HighHpToMaxTactic(healing=healing_spell, use_max=False)
    hp_to_max_high_result = hp_to_max_high_tactic2.run_tactic(test_list)
    assert len(hp_to_max_high_result) == 4 and c10 in hp_to_max_high_result and d10 in hp_to_max_high_result \
           and c11 in hp_to_max_high_result and c13 in hp_to_max_high_result

    test_list = [c10, d10, c11, c12, d12, c13]
    c10.take_damage(10)  # go unconscious
    c11.take_damage(68)  # die instantly
    c12.take_damage(20)
    c12.take_damage(40)  # die
    c13.take_damage(31)  # go unconscious
    # d10 and d12 are the same
    murder_hobo_tactic = combatant_tactics.IsUnconsciousTactic()
    assert murder_hobo_tactic.run_tactic(test_list) == [c10, c13]
    assert murder_hobo_tactic == combatant_tactics.IsUnconsciousTactic()
    is_conscious_tactic = combatant_tactics.IsConsciousTactic()
    assert is_conscious_tactic.run_tactic(test_list) == [d10, d12]

    test_list = [c10, d10, c11, c12, d12, c13]
    has_temp_hp_tactic = combatant_tactics.HasTempHpTactic()
    c10.set_temp_hp(5)
    c12.set_temp_hp(1)
    c13.set_temp_hp(0)
    has_temp_hp_result = has_temp_hp_tactic.run_tactic(test_list)
    assert len(has_temp_hp_result) == 2 and c10 in has_temp_hp_result and c12 in has_temp_hp_result
    assert has_temp_hp_tactic == combatant_tactics.HasTempHpTactic()

    test_list = [c10, d10, c11, c12, d12, c13]
    no_temp_hp_tactic = combatant_tactics.NoTempHpTactic()
    no_temp_hp_result = no_temp_hp_tactic.run_tactic(test_list)
    assert len(no_temp_hp_result) == 4 and d10 in no_temp_hp_result and c11 in no_temp_hp_result \
           and d12 in no_temp_hp_result and c13 in no_temp_hp_result
    assert no_temp_hp_tactic == combatant_tactics.NoTempHpTactic()

    test_list = [c10, d10, c11, c12, d12, c13]
    c10.add_vulnerability("piercing")
    d10.add_resistance("piercing")
    c11.add_immunity("piercing")
    has_vulnerability_tactic = combatant_tactics.HasVulnerabilityTactic(vulnerability_type="piercing")
    assert has_vulnerability_tactic.run_tactic(test_list) == [c10]
    assert has_vulnerability_tactic == has_vulnerability_tactic.get_copy()
    assert has_vulnerability_tactic != combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning")
    has_no_vulnerability_tactic = combatant_tactics.HasNoVulnerabilityTactic(vulnerability_type="piercing")
    assert has_no_vulnerability_tactic.run_tactic(test_list) == [d10, c11, c12, d12, c13]
    assert has_no_vulnerability_tactic == has_no_vulnerability_tactic.get_copy()
    assert has_no_vulnerability_tactic != combatant_tactics.HasNoVulnerabilityTactic(vulnerability_type="bludgeoning")
    has_resistance_tactic = combatant_tactics.HasResistanceTactic(resistance_type="piercing")
    assert has_resistance_tactic.run_tactic(test_list) == [d10]
    assert has_resistance_tactic == has_resistance_tactic.get_copy()
    assert has_resistance_tactic != combatant_tactics.HasResistanceTactic(resistance_type="bludgeoning")
    has_no_resistance_tactic = combatant_tactics.HasNoResistanceTactic(resistance_type="piercing")
    assert has_no_resistance_tactic.run_tactic(test_list) == [c10, c11, c12, d12, c13]
    assert has_no_resistance_tactic == has_no_resistance_tactic.get_copy()
    assert has_no_resistance_tactic != combatant_tactics.HasNoResistanceTactic(resistance_type="bludgeoning")
    has_immunity_tactic = combatant_tactics.HasImmunityTactic(immunity_type="piercing")
    assert has_immunity_tactic.run_tactic(test_list) == [c11]
    assert has_immunity_tactic == has_immunity_tactic.get_copy()
    assert has_immunity_tactic != combatant_tactics.HasImmunityTactic(immunity_type="bludgeoning")
    has_no_immunity_tactic = combatant_tactics.HasNoImmunityTactic(immunity_type="piercing")
    assert has_no_immunity_tactic.run_tactic(test_list) == [c10, d10, c12, d12, c13]
    assert has_no_immunity_tactic == has_no_immunity_tactic.get_copy()
    assert has_no_immunity_tactic != combatant_tactics.HasNoImmunityTactic(immunity_type="bludgeoning")

    tact1 = combatant_tactics.LowAcTactic(threshold=12,
                                          tiebreakers=[combatant_tactics.BloodiedTactic(), combatant_tactics.IsUnconsciousTactic()])
    tact2 = combatant_tactics.NoTempHpTactic(tiebreakers=[combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning")])
    try:
        tact2.extend_tiebreakers("dog")
        raise Exception("Allowed invalid extend tiebreaker")
    except ValueError:
        pass
    tact2.extend_tiebreakers(tact1)
    assert tact2.get_tiebreakers() == [combatant_tactics.HasVulnerabilityTactic(vulnerability_type="bludgeoning"),
                                       combatant_tactics.BloodiedTactic(), combatant_tactics.IsUnconsciousTactic()]

def test_attack_tactics():
    """
    :Test module: :py:mod:`attack_tactics`
    :Test class: :py:class:`AttackTactic` and subclasses
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies:
        - :py:func:`test_tactics`
        - :py:func:`test_attack`
        - :py:func:`test_armory`
        - :py:func:`test_dice`
    :return: None
    """
    dagger_kwargs = armory.Dagger().get_attack_kwargs()
    dagger_range = attack_class.Attack(**(dagger_kwargs[0]))
    dagger_disadv = attack_class.Attack(**(dagger_kwargs[1]))
    dagger_melee = attack_class.Attack(**(dagger_kwargs[2]))
    javelin_kwargs = armory.Javelin().get_attack_kwargs()
    javelin_range = attack_class.Attack(**(javelin_kwargs[0]))
    javelin_disadv = attack_class.Attack(**(javelin_kwargs[1]))
    javelin_melee = attack_class.Attack(**(javelin_kwargs[2]))
    crossbow_kwargs = armory.LightCrossbow().get_attack_kwargs()
    crossbow = attack_class.Attack(**(crossbow_kwargs[0]))
    crossbow_disadv = attack_class.Attack(**(crossbow_kwargs[1]))
    glaive = attack_class.Attack(**(armory.Glaive().get_attack_kwargs()[0]))
    test_list = [dagger_melee, dagger_range, dagger_disadv, javelin_melee, javelin_range, javelin_disadv, crossbow,
                 crossbow_disadv, glaive]

    piercing_tactic = attack_tactics.DamageTypeTactic(damage_type="piercing")
    assert piercing_tactic.run_tactic(test_list) == [dagger_melee, dagger_range, dagger_disadv, javelin_melee, javelin_range,
                                                     javelin_disadv, crossbow, crossbow_disadv]
    c0 = combatant.Combatant(ac=12, max_hp=50, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                             strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                             proficiencies=["dexterity", "strength"], proficiency_mod=2,
                             name='t0', vulnerabilities=["slashing"],
                             resistances=["bludgeoning", "fire"], immunities=["piercing"], logger=test_logger)

    club = attack_class.Attack(**(armory.Club().get_attack_kwargs()[0]))
    torch = attack_class.Attack(name="torch", melee_range=5, damage_dice=dice.DamageDice(str_val="1d6", damage_type="fire"))
    hot_n_cold = attack_class.Attack(name="hot_n_cold", melee_range=5, damage_dice=dice.DamageDiceBag(
        dice_list=[dice.DamageDice(str_val="1d4", damage_type="fire"), dice.DamageDice(str_val="1d8", damage_type="cold")]))
    test_list_2 = test_list[:]
    test_list_2.extend([club, torch, hot_n_cold])
    for attack in test_list_2:
        c0.add_attack(attack)
    damage_vulnerability = attack_tactics.DamageVulnerabilityTactic()
    assert damage_vulnerability.run_tactic(test_list_2, target=c0) == [glaive]
    c0.set_attack_tactic(damage_vulnerability)
    assert c0.select_attack(target=c0) == glaive
    c0.remove_attack(club)
    c0.remove_attack(torch)
    c0.remove_attack(hot_n_cold)
    assert c0.get_attacks() == test_list
    damage_no_immune = attack_tactics.DamageNoImmuneTactic()
    assert damage_no_immune.run_tactic(test_list_2, target=c0) == [glaive, club, torch, hot_n_cold]
    damage_no_resistance = attack_tactics.DamageNoResistanceTactic()
    assert damage_no_resistance.run_tactic(test_list, target=c0) == [dagger_melee, dagger_range, dagger_disadv, javelin_melee, javelin_range,
                                                 javelin_disadv, crossbow, crossbow_disadv, glaive]

    range_normal_lt_80 = attack_tactics.LowRangeTactic(threshold=80)
    assert range_normal_lt_80.run_tactic(test_list) == [dagger_range, dagger_disadv, javelin_range]
    range_normal_gt_20 = attack_tactics.HighRangeTactic(threshold=20)
    assert range_normal_gt_20.run_tactic(test_list) == [dagger_disadv, javelin_range, javelin_disadv, crossbow, crossbow_disadv]
    melee_range_lt_10 = attack_tactics.LowMeleeRangeTactic(threshold=10)
    assert melee_range_lt_10.run_tactic(test_list) == [dagger_melee, javelin_melee]
    melee_range_gt_5 = attack_tactics.HighMeleeRangeTactic(threshold=5)
    assert melee_range_gt_5.run_tactic(test_list) == [glaive]
    c0.set_attack_tactic(melee_range_gt_5)
    assert c0.select_attack() == glaive

    dagger_melee_adv = attack_class.Attack(**(dagger_kwargs[2]), adv=1)
    test_list.append(dagger_melee_adv)
    has_adv = attack_tactics.HasAdvTactic()
    assert has_adv.run_tactic(test_list) == [dagger_melee_adv]
    has_disadv = attack_tactics.HasDisAdvTactic()
    assert has_disadv.run_tactic(test_list) == [dagger_disadv, javelin_disadv, crossbow_disadv]
    has_no_disadv = attack_tactics.HasNoDdisavTactic()
    assert has_no_disadv.run_tactic(test_list) == [dagger_melee, dagger_range, javelin_melee, javelin_range,
                                                   crossbow, glaive, dagger_melee_adv]

    dagger_melee_5 = attack_class.Attack(**(dagger_kwargs[2]), attack_mod=5)
    dagger_melee_3 = attack_class.Attack(**(dagger_kwargs[2]), attack_mod=3)
    test_list.extend([dagger_melee_3, dagger_melee_5])
    highest_max_hit = attack_tactics.HighestMaxHitTactic()
    assert highest_max_hit.run_tactic(test_list) == [dagger_melee_5]
    high_max_hit = attack_tactics.HighMaxHitTactic(threshold=20)
    assert high_max_hit.run_tactic(test_list) == [dagger_melee_3, dagger_melee_5]
    c0.set_ac(21)
    max_hit_ac = attack_tactics.MaxHitACTactic()
    assert max_hit_ac.run_tactic(test_list, target=c0) == [dagger_melee_3, dagger_melee_5]
    highest_avg_hit = attack_tactics.HighestAvgHitTactic()
    assert highest_avg_hit.run_tactic(test_list) == [dagger_melee_5]
    high_avg_hit = attack_tactics.HighAvgHitTactic(threshold=11)
    assert high_avg_hit.run_tactic(test_list) == [dagger_melee_adv, dagger_melee_3, dagger_melee_5]
    c0.set_ac(12)
    avg_hit_ac_high = attack_tactics.AvgHitACHighTactic()
    assert avg_hit_ac_high.run_tactic(test_list, target=c0) == [dagger_melee_adv, dagger_melee_3, dagger_melee_5]

    highest_max_damage = attack_tactics.HighestMaxDamageTactic()
    assert highest_max_damage.run_tactic(test_list) == [glaive]
    high_max_damage = attack_tactics.HighMaxDamageTactic(threshold=6)
    assert high_max_damage.run_tactic(test_list) == [crossbow, crossbow_disadv, glaive]
    max_damage_hp_high = attack_tactics.MaxDamageHPHighTactic()
    c0.take_damage(13)  # c0 had 20 hp, so now it has 7 hp
    assert max_damage_hp_high.run_tactic(test_list, target=c0) == [crossbow, crossbow_disadv, glaive]
    assert max_damage_hp_high.run_tactic(test_list, target=c0)
    low_max_damage = attack_tactics.LowMaxDamageTactic(threshold=6)
    assert low_max_damage.run_tactic(test_list) == [dagger_melee, dagger_range, dagger_disadv, dagger_melee_adv, dagger_melee_3, dagger_melee_5]
    c0.take_damage(1)  # c0 had 7 hp, so now it has 6 hp
    max_damage_hp_low = attack_tactics.MaxDamageHPLowTactic()
    assert max_damage_hp_low.run_tactic(test_list, target=c0) == [dagger_melee, dagger_range, dagger_disadv, dagger_melee_adv, dagger_melee_3, dagger_melee_5]

    greatsword = attack_class.Attack(**(armory.Greatsword().get_attack_kwargs()[0]))
    dagger_melee_d2 = attack_class.Attack(**(dagger_kwargs[2]), damage_mod=2)
    test_list.extend([greatsword, dagger_melee_d2])
    highest_min_damage = attack_tactics.HighestMinDamageTactic()
    assert highest_min_damage.run_tactic(test_list) == [dagger_melee_d2]
    low_min_damage = attack_tactics.LowMinDamageTactic(threshold=2)
    assert low_min_damage.run_tactic(test_list) == [dagger_melee, dagger_range, dagger_disadv, javelin_melee,
                                                    javelin_range, javelin_disadv, crossbow,
                                                    crossbow_disadv, glaive, dagger_melee_adv, dagger_melee_3,
                                                    dagger_melee_5]
    c0.take_damage(4)  # c0 had 6 hp, so now it has 2 hp
    min_damage_hp_low = attack_tactics.MinDamageHPLowTactic()
    assert min_damage_hp_low.run_tactic(test_list, target=c0) == [dagger_melee, dagger_range, dagger_disadv,
                                        javelin_melee, javelin_range, javelin_disadv, crossbow, crossbow_disadv,
                                        glaive, dagger_melee_adv, dagger_melee_3, dagger_melee_5]
    high_min_damage = attack_tactics.HighMinDamageTactic(threshold=1)
    assert high_min_damage.run_tactic(test_list) == [greatsword, dagger_melee_d2]
    min_damage_hp_high = attack_tactics.MinDamageHPHighTactic()
    assert min_damage_hp_high.run_tactic(test_list, target=c0) == [greatsword, dagger_melee_d2]

    highest_avg_damage = attack_tactics.HighestAvgDamageTactic()
    assert highest_avg_damage.run_tactic(test_list) == [greatsword]
    high_avg_damage = attack_tactics.HighAvgDamageTactic(threshold=4)
    assert high_avg_damage.run_tactic(test_list) == [crossbow, crossbow_disadv, glaive, greatsword, dagger_melee_d2]
    c0.take_healing(2)  # c0 had 2 hp, so now it has 4 hp
    avg_damage_hp_high = attack_tactics.AvgDamageHPHighTactic()
    assert avg_damage_hp_high.run_tactic(test_list, target=c0) == [crossbow, crossbow_disadv, glaive, greatsword, dagger_melee_d2]
    low_avg_damage = attack_tactics.LowAvgDamageTactic(threshold=3)
    assert low_avg_damage.run_tactic(test_list) == [dagger_melee, dagger_range, dagger_disadv, dagger_melee_adv,
                                                    dagger_melee_3, dagger_melee_5]
    c0.take_damage(1)  # c0 had 4 hp, so now it has 3 hp
    avg_damage_hp_low = attack_tactics.AvgDamageHPLowTactic()
    assert avg_damage_hp_low.run_tactic(test_list, target=c0) == [dagger_melee, dagger_range, dagger_disadv,
                                                                  dagger_melee_adv, dagger_melee_3, dagger_melee_5]

    dpr_max = attack_tactics.DprMaxTactic()
    assert dpr_max.run_tactic(test_list, target=c0) == [greatsword]
    dpr_high = attack_tactics.DprHighTactic(threshold=2)
    assert dpr_high.run_tactic(test_list, target=c0) == [crossbow, glaive, greatsword, dagger_melee_d2]
    c0.take_damage(1)  # c0 had 3 hp, so now it has 2 hp
    dpr_hp_high = attack_tactics.DprHpHighTactic()
    assert dpr_hp_high.run_tactic(test_list, target=c0) == [crossbow, glaive, greatsword, dagger_melee_d2]
    dpr_low = attack_tactics.DprLowTactic(threshold=1)
    assert dpr_low.run_tactic(test_list, target=c0) == [dagger_disadv, javelin_disadv, crossbow_disadv]
    c0.take_damage(1)  # c0 had 2 hp, so now it has 1 hp
    dpr_hp_low = attack_tactics.DprHpLowTactic()
    assert dpr_hp_low.run_tactic(test_list, target=c0) == [dagger_disadv, javelin_disadv, crossbow_disadv]

    # more likely to hit than greatsword and does the same amount of damage
    cha_save_attack = attack_class.SavingThrowAttack(save_type="charisma", dc=10, damage_dice=(2, 6), name="cha")
    test_list.append(cha_save_attack)
    assert dpr_max.run_tactic(test_list, target=c0) == [cha_save_attack]
    # less likely to hit than greatsword and does the same amount of damage
    dex_save_attack = attack_class.SavingThrowAttack(save_type="dexterity", dc=10, damage_dice=(2, 6), name="dex")
    test_list.remove(cha_save_attack)
    test_list.append(dex_save_attack)
    assert dpr_max.run_tactic(test_list, target=c0) == [greatsword]

def test_team():
    """
    :Test module: :py:mod:`team`
    :Test class: :py:class:`Team`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: invalid input to constructor, warning messages
    :Test dependencies:
        - :py:func:`test_tactics`
        - :py:func:`test_combatant`
    :return: None
    """
    c10 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c10", logger=test_logger)
    d10 = combatant.Combatant(copy=c10, name="d10")
    c11 = combatant.Combatant(ac=11, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c10", logger=test_logger)
    c12 = combatant.Combatant(ac=12, max_hp=20, current_hp=20, temp_hp=2, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c10", logger=test_logger)
    combatant_list = [c10, d10, c11, c12]
    lowest_ac_tactic = combatant_tactics.LowestAcTactic()

    t0 = team.Team(name="t0", combatant_list=combatant_list, enemy_tactic=lowest_ac_tactic)
    assert t0.get_name() == "t0"
    assert t0.get_combatants() == combatant_list
    assert t0.get_combatants() is not combatant_list
    assert t0.get_enemy_tactic() == lowest_ac_tactic
    assert t0.has_member(c10) and t0.has_member(d10) and t0.has_member(c11) and t0.has_member(c12)
    assert c10.get_team() == t0 and d10.get_team() == t0 and c11.get_team() == t0 and c12.get_team() == t0
    assert c11.is_on_my_team(c10)
    assert t0.has_all_alive() and t0.has_some_alive()
    stats = t0.get_stats()
    assert stats['num_conscious'] == 4
    assert stats['num_unconscious'] == 0
    assert stats['num_dead'] == 0
    assert isinstance(c10.get_enemy_tactic(), combatant_tactics.IsConsciousTactic)
    assert c10.get_enemy_tactic().get_tiebreakers() == [lowest_ac_tactic]

    t0_copy = t0.get_copy(name="t0_copy")
    assert t0_copy.get_name() == "t0_copy"
    assert t0_copy.get_combatants() == combatant_list
    assert t0_copy.get_combatants() is not t0.get_combatants()
    assert t0.get_enemy_tactic() is t0.get_enemy_tactic()
    assert t0.equals(t0_copy)

    t0_deep_copy = t0.get_copy(name="t0_deep_copy", deep_copy=True)
    assert t0_deep_copy.get_name() == "t0_deep_copy"
    assert t0_deep_copy.get_combatants() is not t0.get_combatants()
    for i in range(len(t0.get_combatants())):
        original_item = t0.get_combatants()[i]
        copied_item = t0_deep_copy.get_combatants()[i]
        assert original_item.equals(copied_item)
        assert copied_item is not original_item
    assert isinstance(t0_deep_copy.get_enemy_tactic(), combatant_tactics.LowestAcTactic)
    assert t0_deep_copy.get_enemy_tactic() is not t0.get_enemy_tactic()
    assert t0.equals(t0_deep_copy)

    t0.remove_combatant(c11)
    assert not t0.has_member(c11)
    assert c11.get_team() is None
    assert not c11.is_on_my_team(c10)
    assert t0.get_combatants() == [c10, d10, c12]

    assert not t0.has_some_unconscious()
    assert not t0.has_some_not_all_unconscious()
    assert not t0.has_all_unconscious()
    assert t0.has_some_conscious()
    assert t0.has_all_conscious()
    c10.become_unconscious()
    assert t0.has_some_unconscious()
    assert t0.has_some_not_all_unconscious()
    assert t0.has_some_conscious()
    stats = t0.get_stats()
    assert stats['num_conscious'] == 2, stats["num_conscious"]
    assert stats['num_unconscious'] == 1
    assert stats['num_dead'] == 0
    d10.become_unconscious()
    c12.become_unconscious()
    assert t0.has_some_unconscious()
    assert t0.has_all_unconscious()
    assert not t0.has_some_conscious()
    assert not t0.has_all_conscious()

    assert not t0.has_some_dead() and not t0.has_some_not_all_dead() and not t0.has_all_dead()
    c10.die()
    assert t0.has_some_alive()
    assert t0.has_some_dead() and t0.has_some_not_all_dead()
    assert not t0.has_all_dead()
    stats = t0.get_stats()
    assert stats['num_conscious'] == 0
    assert stats['num_unconscious'] == 2
    assert stats['num_dead'] == 1
    d10.die()
    c12.die()
    assert t0.has_some_dead() and t0.has_all_dead()
    assert not t0.has_some_not_all_dead()
    assert not t0.has_some_alive() and not t0.has_all_alive()

def test_encounter():
    """
    :Test module: :py:mod:`encounter`
    :Test class: :py:class:`Encounter`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies:
        - :py:func:`test_tactics`
        - :py:func:`test_combatant`
        - :py:func:`test_team`
    :return:
    """
    c1 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c1", weapons=[armory.Greatclub()], logger=test_logger)
    c2 = c1.get_copy(name="c2")
    team_c = team.Team(combatant_list=[c1, c2], name="team_c")
    d1 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=16, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="d1", weapons=[armory.Greatclub()], logger=test_logger)
    d2 = d1.get_copy(name="d2")
    team_d = team.Team(combatant_list=[d1, d2], name="team_d")
    try:
        encounter.Encounter(name="e", logger=test_logger)
        raise Exception("Allowed Encounter with no teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams=[], logger=test_logger)
        raise Exception("Allowed Encounter with empty list of teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams=1, logger=test_logger)
        raise Exception("Allowed Encounter with non-iterable teams")
    except ValueError:
        pass
    try:
        encounter.Encounter(name="e", teams="cat", logger=test_logger)
        raise Exception("Allowed Encounter with non-Teams")
    except ValueError:
        pass

    e1 = encounter.Encounter(name="e1", teams=[team_c, team_d], logger=test_logger)
    assert e1.get_name() == "e1"
    assert e1.get_teams() == [team_c, team_d]
    assert e1.get_combatants() == [c1, c2, d1, d2]
    assert e1.get_round() == 1
    assert e1.get_encounter_statnames() == ["rounds"]

    e1_copy = e1.get_copy(name="e1_copy")
    assert e1_copy.get_teams()[1].get_combatants()[1] == d2
    assert e1_copy.get_teams()[0].get_combatants() == [c1, c2]  # same Combatants
    assert e1_copy.get_combatants() == [c1, c2, d1, d2]
    assert e1.equals(e1_copy)
    assert e1_copy is not e1

    e1_deep_copy = e1.get_copy(name="e1_deep_copy", deep_copy=True)
    assert e1_deep_copy.get_teams()[0].get_combatants()[0] is not c1  # copied Combatants
    assert e1_deep_copy.get_teams()[0].get_combatants() != [c1, c2]  # copied Combatants
    assert e1_deep_copy.get_teams()[0].get_combatants()[1].equals(c2)
    assert e1.equals(e1_deep_copy)
    assert e1_deep_copy is not e1

    for i in range(0, 5):  # pylint: disable=unused-variable
        e1.roll_initiative()

    e1.run()
    assert e1.get_round() != 0
    stats = e1.get_stats()
    test_logger.info("team {team}\ncombatants {combatants}\nrounds {rounds}".format_map(stats))
    e1.reset()
    assert e1.get_round() == 1
    for comb in e1.get_combatants():
        assert comb.get_current_hp() == comb.get_max_hp()
        assert comb.get_conditions() == []
        assert comb.get_death_saves() == {0: 0, 1: 0}

def test_simulation():
    """
    :Test module: :py:mod:`simulation`
    :Test class: :py:class:`Simulation`
    :Test functions: N/A
    :Untested classes/functions: None
    :Untested parts: None
    :Test dependencies:
        - :py:func:`test_encounter`
    :return:
    """
    c1 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=14, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="c1", weapons=[armory.Greatclub()], logger=test_logger)
    c2 = c1.get_copy(name="c2")
    team_c = team.Team(combatant_list=[c1, c2], name="team_c")
    d1 = combatant.Combatant(ac=10, max_hp=20, current_hp=20, hit_dice='3d6', speed=20, vision='darkvision',
                              strength=16, dexterity=16, constitution=9, intelligence=12, wisdom=11, charisma=8,
                              name="d1", weapons=[armory.Greatclub()], logger=test_logger)
    d2 = d1.get_copy(name="d2")
    team_d = team.Team(combatant_list=[d1, d2], name="team_d")
    e0 = encounter.Encounter(name="e0", teams=[team_c, team_d], logger=test_logger)

    s0 = simulation.Simulation(name="s0", encounter=e0)
    assert s0.get_encounter() == e0
    s1 = s0.get_copy(deep_copy=True)
    assert s1.get_encounter() is not s0.get_encounter()
    assert s1.get_encounter().equals(s0.get_encounter())
    assert s1.get_encounter().get_combatants() != s0.get_encounter().get_combatants()
    # for i in range(len(e0.get_combatants())):
    #     print(id(e0.get_combatants()[i]), id(s1.get_encounter().get_combatants()[i]))

    s0.run(5)  # run the simulation 5 times

    # we don't want a bazillion logging statements
    null_logger = NullLogger()
    s1.get_encounter().set_logger(null_logger)
    for comb in s1.get_encounter().get_combatants():
        comb.set_logger(null_logger)

    print("s1")
    start = time.time()
    s1.run(100)
    elapsed_time = time.time() - start
    test_logger.info("Time to run simulation s1 100 times: %f", elapsed_time)

    for p in [1, 2, 4, 8]:
        start = time.time()
        s1.mp_run(10000, 1)
        elapsed_time = time.time() - start
        test_logger.info("Time to run simulation s1 10000 times with %d process: %f", p, elapsed_time)

test_attack()

fh.close()
ch.close()
