import warnings
from typing import Optional
from DnD_5e.combatant import Character, SpellCaster, Combatant, Creature
from DnD_5e.attack_class import Spell
from DnD_5e.utility_methods_dnd import ability_to_mod, proficiency_bonus_per_level, roll_dice, TYPE_DICE_TUPLE
from DnD_5e import armory, features

class Barbarian(Character):
    """
    Barbarian character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 12)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 12 + constitution_mod
        if level > 1:
            max_hp += (7 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', set())
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif isinstance(proficiencies, set):  # pragma: no cover
            pass
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("constitution")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod,
                       "feature_classes": [features.UnarmoredDefenseBarbarian, features.FastMovementBarbarian]})

        super().__init__(**kwargs)

        if self.get_level() < 3:
            self._rage_slots = 2
        elif self.get_level() < 6:
            self._rage_slots = 3
        elif self.get_level() < 12:
            self._rage_slots = 4
        elif self.get_level() < 17:
            self._rage_slots = 5
        else:
            self._rage_slots = 6

        if self.get_level() < 9:
            self._rage_damage_bonus = 2
        elif self.get_level() < 16:
            self._rage_damage_bonus = 3
        else:
            self._rage_damage_bonus = 4

        self._rage_state = False

        self.add_feature("rage")
        self.add_feature("unarmored defense")
        if self.get_level() > 1:
            self.add_feature("reckless attack")
            self._reckless_state = False
            self.add_feature("danger sense")
        if self.get_level() > 2:
            self._specialization = kwargs.get("specialization")
        if self.get_level() > 4:
            self.add_feature("extra attack")
            self.add_feature_class(features.FastMovementBarbarian)
        if self.get_level() > 6:
            self.add_feature("feral instinct")
        if self.get_level() > 8:
            self.add_feature("brutal critical")
        if self.get_level() > 10:
            self.add_feature("relentless rage")
            self._relentless_rage_dc = 10
        if self.get_level() > 14:
            self.add_feature("persistent rage")
        if self.get_level() > 17:
            self.add_feature("indomitable might")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Barbarian to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        """
        if not isinstance(other, Barbarian):
            raise ValueError("Cannot copy Barbarian from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._rage_slots = other.get_rage_slots()
        self._rage_damage_bonus = other.get_rage_damage_bonus()
        self._rage_state = False
        if self.has_feature("reckless attack"):
            self._reckless_state = False

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal
        based on what is checked in the superclass method as well as rage damage bonus

        :param other: the Barbarian to be compared
        :type other: Barbarian
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_rage_damage_bonus() == other.get_rage_damage_bonus()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: rage slots, rage state

        :param other: the Barbarian to compare
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_rage_slots() == other.get_rage_slots() \
            and self.is_raging() == other.is_raging()

    def get_rage_slots(self) -> int:
        """
        :return: rage slots
        :rtype: non-negative integer
        """
        return self._rage_slots

    def get_rage_damage_bonus(self) -> int:
        """
        :return: rage damage bonus
        :rtype: int
        """
        return self._rage_damage_bonus

    def is_raging(self) -> bool:
        """
        :return: True if *self* is raging, False otherwise
        :rtype: bool
        """
        return self._rage_state

    def start_rage(self):
        """
        Go into a rage state

        :return: None
        """
        if self._rage_state:
            self.get_logger().warning("You cannot start raging because you are already raging", stack_info=True)
            warnings.warn("You cannot start raging because you are already raging")
            return
        self.get_logger().info("%s would like to RAGE!", self.get_name())
        self._rage_state = True
        for attack in self._attacks:
            if attack.get_melee_range():
                weapon = attack.get_weapon()
                if not weapon or isinstance(weapon, armory.MeleeWeapon):
                    attack.set_damage_mod(attack.get_damage_mod() + self._rage_damage_bonus)
        self.add_resistance("bludgeoning")
        self.add_resistance("piercing")
        self.add_resistance("slashing")

    def stop_rage(self):
        """
        Stop the rage state

        :return: None
        """
        if not self._rage_state:
            self.get_logger().warning("You cannot stop raging because you are not raging", stack_info=True)
            warnings.warn("You cannot stop raging because you are not raging")
            return
        self.get_logger().info("%s has finished raging.", self.get_name())
        self._rage_state = False
        for attack in self._attacks:
            if attack.get_melee_range():
                attack.set_damage_mod(attack.get_damage_mod() - self._rage_damage_bonus)
        self.remove_resistance("bludgeoning")
        self.remove_resistance("piercing")
        self.remove_resistance("slashing")

    def start_reckless(self):
        """
        Enter this state when attacking recklessly. It lasts for one round.

        :return: None
        """
        if self._reckless_state:
            self.get_logger().warning("You cannot start recklessly attacking because you are already doing so", stack_info=True)
            warnings.warn("You cannot start recklessly attacking because you are already doing so")
            return
        self.get_logger().info("%s attacks recklessly!", self.get_name())
        self.modify_adv_to_be_hit(1)
        self._reckless_state = True

    def stop_reckless(self):
        """
        End the reckless state. This happens after one round of recklessly attacking.

        :return: None
        """
        if not self._reckless_state:
            self.get_logger().warning("You cannot stop recklessly attacking because you are not currently recklessly attacking", stack_info=True)
            warnings.warn("You cannot stop recklessly attacking because you are not currently recklessly attacking")
            return
        self.modify_adv_to_be_hit(-1)
        self._reckless_state = False

    def make_saving_throw(self, save_type: str, adv=0):
        """
        Roll a saving throw of the given type.
        This is different from the superclass method in that it is affected by raging and the "danger sense" feature.

        :param save_type: the kind of saving throw to make
        :type save_type: one of these strings: "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"
        :param adv: indicates advantage (positive), disadvantage (negative), or neither (0)
        :type adv: int
        :return: the number rolled for the saving throw
        :rtype: int
        """
        if save_type == "strength" and self.is_raging():
            local_adv = 1
        elif self.has_feature("danger sense") and save_type == "dexterity" and not self.has_condition("blinded") \
            and not self.has_condition("deafened") and not self.has_condition("incapacitated"):
            local_adv = 1
        else:
            local_adv = 0
        adv += local_adv
        return super().make_saving_throw(save_type=save_type, adv=local_adv)

    def become_unconscious(self):
        """
        Go unconscious. This differs from the superclass method
        in that *self* may drop to 1 hit point instead due to the "relentless rage" feature.
        If *self* saves, then *self* drops to 1 hit point. Otherwise, *self* is unconscious and has current hp of 0

        :return: None
        """
        if self.is_raging():
            if self.has_feature("relentless rage"):
                if self.take_saving_throw("constitution", self._relentless_rage_dc):
                    self._current_hp = 1
                    self.get_logger().info("Fueled by rage, %s staves off unconsciousness and drops to 1 hit point instead.", self.get_name())
                    return
                self._relentless_rage_dc += 5
            self.stop_rage()
        super().become_unconscious()

    def send_attack(self, target, attack, adv=0):
        """
        Attack a given target using a given attack.
        This differs from the superclass method in that if *self* is recklessly attacking, the attack has advantage

        :param target: the Combatant to attack
        :type target: Combatant
        :param attack: the Attack being made
        :type attack: Attack
        :param adv: indicates whether *self* has advantage for this attack
        :type adv: int
        :return: the damage *target* took from *attack*, or None if the attack failed to hit
        """
        if attack.get_melee_range() and self.has_feature("reckless attack") and self._reckless_state:
            adv += 1
        super().send_attack(target, attack, adv)

class Bard(SpellCaster, Character):
    """
    Bard character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("hand crossbow")
        proficiencies.add("longsword")
        proficiencies.add("rapier")
        proficiencies.add("shortsword")
        proficiencies.add("dexterity")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        feature_seq = kwargs.get('features', set())
        if isinstance(feature_seq, (list, tuple, set)):
            feature_seq = set(feature_seq)
        elif feature_seq is None:
            feature_seq = set()
        else:
            raise ValueError("Features must be a set (or a list or tuple to convert to a set)")

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3}
            feature_seq.add("countercharm")
            if level == 7:
                spell_slots.update({4: 1})
            elif level == 8:
                spell_slots.update({4: 2})
            elif level == 9:
                spell_slots.update({4: 3, 5: 1})
            elif level == 10:
                spell_slots.update({4: 3, 5: 2})
            elif level < 13:
                spell_slots.update({4: 3, 5: 2, 6: 1})
            elif level < 15:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1})
            elif level < 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1})
            elif level == 17:
                spell_slots.update({4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 18:
                spell_slots.update({4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1})
            elif level == 19:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1})
            elif level == 20:
                spell_slots.update({4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1})

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_ability": "charisma", "spell_slots": spell_slots,
                       "features": feature_seq})

        super().__init__(**kwargs)

        if self.get_level() < 5:
            self._inspiration_dice = (1, 6)
        elif self.get_level() < 10:
            self._inspiration_dice = (1, 8)
        elif self.get_level() < 15:
            self._inspiration_dice = (1, 10)
        else:
            self._inspiration_dice = (1, 12)

        self._inspiration_slots = self._charisma

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Bard to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Bard
        """
        if not isinstance(other, Bard):
            self.get_logger().error("Cannot copy Bard from other class", stack_info=True)
            raise ValueError("Cannot copy Bard from other class")
        super().copy_constructor(other, **kwargs)
        self._inspiration_dice = other.get_inspiration_dice()
        self._inspiration_slots = other.get_inspiration_slots()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on
        what is checked in the superclass method as well as inspiration dice

        :param other: the Bard to be compared
        :type other: Bard
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_inspiration_dice() == other.get_inspiration_dice()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: inspiration slots

        :param other: the Bard to compare
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_inspiration_slots() == other.get_inspiration_slots()

    def get_inspiration_dice(self) -> TYPE_DICE_TUPLE:
        """
        :return: inspiration dice
        :rtype: TYPE_DICE_TUPLE
        """
        return self._inspiration_dice

    def get_inspiration_slots(self) -> int:
        """
        :return: inspiration slots
        :rtype: non-negative integer
        """
        return self._inspiration_slots

    def send_inspiration(self, target: Combatant):
        """
        Give an inspiration die to *target*. NOT IMPLEMENTED YET (i.e., nobody can take inspiration)

        :param target: the Combatant to give inspiration to
        :type target: Combatant
        :return: None
        """
        if target is self or not isinstance(target, Combatant):
            self.get_logger().error("Can only give inspiration to a Combatant other than yourself", stack_info=True)
            raise ValueError("Can only give inspiration to a Combatant other than yourself")
        if not self._inspiration_slots:
            self.get_logger().error("Cannot inspire %s because you have no inspiration slots left", target.get_name())
            raise ValueError("Cannot inspire %s because you have no inspiration slots left" % target.get_name())
        self._inspiration_slots -= 1

class Cleric(SpellCaster, Character):
    """
    Cleric character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class. These are the keyword arguments that the user can/must still define:
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:
            proficiencies = set()
        else:
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        elif level == 6:
            spell_slots = {1: 4, 2: 3, 3: 3}
        elif level == 7:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 8:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 9:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 1}
        elif level == 10:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2}
        elif level < 13:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1}
        elif level < 15:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
        elif level < 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1}
        elif level == 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 18:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 19:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_ability": "wisdom", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("channel divinity")
            if self.get_level() < 6:
                self._channel_divinity_slots = 1
            elif self.get_level() < 18:
                self._channel_divinity_slots = 2
            else:
                self._channel_divinity_slots = 3
            if self.get_level() > 4:
                self.add_feature("destroy undead")
                if self.get_level() < 8:
                    self._destroy_undead_cr = 1/2
                elif self.get_level() < 11:
                    self._destroy_undead_cr = 1
                elif self.get_level() < 14:
                    self._destroy_undead_cr = 2
                elif self.get_level() < 17:
                    self._destroy_undead_cr = 3
                else:
                    self._destroy_undead_cr = 4

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Barbarian to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Cleric
        """
        if not isinstance(other, Cleric):
            self.get_logger().error("Cannot copy Cleric from other class", stack_info=True)
            raise ValueError("Cannot copy Cleric from other class")
        super().copy_constructor(other, **kwargs)
        if self.has_feature("channel divinity"):
            self._channel_divinity_slots = other.get_channel_divinity_slots()
            if self.has_feature("destroy undead"):
                self._destroy_undead_cr = other.get_destroy_undead_cr()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on
        what is checked in the superclass method as well as destroy undead cr

        :param other: the Cleric to be compared
        :type other: Cleric
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        result = super().equals(other)
        if self.has_feature("destroy undead"):
            result = result and self.get_destroy_undead_cr() == other.get_destroy_undead_cr()
        return result

    def current_eq(self, other) -> bool:
        """
        Check to see if *self* is identical to *other* by looking at everything in *equals* as well as the following attributes:
        channel divinity slots

        :param other: the Cleric to be compared
        :type other: Cleric
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        result = super().current_eq(other)
        if self.has_feature("channel divinity"):
            result = result and self.get_channel_divinity_slots() == other.get_channel_divinity_slots()
        return result

    def get_channel_divinity_slots(self) -> int:
        """
        :return: channel divinity slots
        :rtype: non-negative integer
        """
        try:
            return self._channel_divinity_slots
        except AttributeError:
            return 0

    def channel_divinity(self, use_type: str = "turn undead"):
        """
        Channel divinity (uses 1 Channel Divinity slot)

        :param use_type: the kind of use (e.g., "turn undead")
        :type use_type: str
        :return: None
        """
        if not self.has_feature("channel divinity"):
            self.get_logger().error("You don't yet know how to channel divinity", stack_info=True)
            raise ValueError("You don't yet know how to channel divinity")
        if not self._channel_divinity_slots:
            self.get_logger().error("You have no more charges of channel divinity left", stack_info=True)
            raise ValueError("You have no more charges of channel divinity left")
        self._channel_divinity_slots -= 1
        self.get_logger().info("%s channels divinity for %s", self.get_name(), use_type)

    def get_destroy_undead_cr(self):
        """
        :return: Max challenge rating of Creatures that will be destroyed with Destroy Undead
        :rtype: number
        """
        try:
            return self._destroy_undead_cr
        except AttributeError:
            self.get_logger().error("You cannot yet destroy undead", stack_info=True)
            raise ValueError("You cannot yet destroy undead")

class Druid(SpellCaster, Character):
    """
    Druid character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param wild_shapes: all the Creatures available to shapeshift into
        :type wild_shapes: set, list, or tuple of Creatures (will be converted to set of creatures)
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("club")
        proficiencies.add("dagger")
        proficiencies.add("javelin")
        proficiencies.add("mace")
        proficiencies.add("quarterstaff")
        proficiencies.add("scimitar")
        proficiencies.add("sickle")
        proficiencies.add("sling")
        proficiencies.add("spear")
        proficiencies.add("intelligence")
        proficiencies.add("wisdom")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 2}
        elif level == 2:
            spell_slots = {1: 3}
        elif level == 3:
            spell_slots = {1: 4, 2: 2}
        elif level == 4:
            spell_slots = {1: 4, 2: 3}
        elif level == 5:
            spell_slots = {1: 4, 2: 3, 3: 2}
        elif level == 6:
            spell_slots = {1: 4, 2: 3, 3: 3}
        elif level == 7:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 1}
        elif level == 8:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 2}
        elif level == 9:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 1}
        elif level == 10:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2}
        elif level < 13:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1}
        elif level < 15:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1}
        elif level < 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1}
        elif level == 17:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 2, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 18:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 1, 7: 1, 8: 1, 9: 1}
        elif level == 19:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 1, 8: 1, 9: 1}
        else:
            spell_slots = {1: 4, 2: 3, 3: 3, 4: 3, 5: 3, 6: 2, 7: 2, 8: 1, 9: 1}

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "spell_slots": spell_slots, "spell_ability": "wisdom"})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self._wild_shape_slots = 2
            wild_shapes = kwargs.get("wild_shapes")
            self._wild_shapes = set()
            if isinstance(wild_shapes, (list, tuple, set)):
                for beast in wild_shapes:
                    if isinstance(beast, Creature):
                        self.add_wild_shape(beast)
                    else:
                        raise ValueError("Wild shapes must be Creatures")
            elif wild_shapes is None:
                warnings.warn("Created a druid of level 2 or above without any wild shapes")
                self._wild_shapes = []
            else:
                raise ValueError("Wild shapes must be a list, tuple, or set of Creatures")
            self._current_shape = None

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*, performing a deep copy of wild shapes

        :param other: the Druid to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Druid
        """
        if not isinstance(other, Druid):
            self.get_logger().error("Cannot copy Druid from other class", stack_info=True)
            raise ValueError("Cannot copy Druid from other class")
        super().copy_constructor(other, **kwargs)
        if self.get_level() > 1:
            self._wild_shape_slots = other.get_wild_shape_slots()
            self._wild_shapes = []
            for beast in other.get_wild_shapes():
                self.add_wild_shape(beast.get_copy())
            self._current_shape = None

    def current_eq(self, other) -> bool:
        """
        Check to see if *self* is identical to *other* by looking at everything in *equals* as well as the following attributes:
        wild shape slots, wild shapes, current shape

        :param other: the Druid to be compared
        :type other: Druid
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_wild_shape_slots() == other.get_wild_shape_slots() \
            and self.get_wild_shapes() == other.get_wild_shapes() \
            and self.get_current_shape() == other.get_current_shape()

    def get_wild_shape_slots(self) -> int:
        """
        :return: wild shape slots
        :rtype: int
        """
        try:
            return self._wild_shape_slots
        except AttributeError:
            return 0

    def get_wild_shapes(self) -> set:
        """
        :return: wild shapes
        :rtype: set of Creatures
        """
        try:
            return self._wild_shapes
        except AttributeError:
            return set()

    def get_current_shape(self) -> Optional[Creature]:
        """
        :return: current shape (or None if in original shape)
        :rtype: Creature (or None)
        """
        try:
            return self._current_shape
        except AttributeError:
            return None

    def get_ac(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: armor class
        :rtype: positive integer
        """
        if self.get_current_shape():
            return self.get_current_shape().get_ac()
        return super().get_ac()

    def get_hit_dice(self) -> TYPE_DICE_TUPLE:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: hit dice
        :rtype: TYPE_DICE_TUPLE
        """
        if self.get_current_shape():
            return self.get_current_shape().get_hit_dice()
        return super().get_hit_dice()

    def get_max_hp(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: max hit points
        :rtype: positive integer
        """
        if self.get_current_shape():
            return self.get_current_shape().get_max_hp()
        return super().get_max_hp()

    def get_temp_hp(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: temporary hit points
        :rtype: non-negative integer
        """
        if self.get_current_shape():
            return 0  # beasts don't have temp hp. Right?
        return super().get_temp_hp()

    def get_current_hp(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: current hit points
        :rtype: non-negative integer
        """
        if self.get_current_shape():
            return self.get_current_shape().get_current_hp()
        return super().get_current_hp()

    def is_bloodied(self) -> bool:
        """
        Tell whether *self* is bloodied (current hit points at or below half of maximum).
        If a wild shape is active, use the statistics of the wild shape.

        :return: True if *self* is bloodied, False otherwise
        :rtype: bool
        """
        if self.get_current_shape():
            return self.get_current_shape().is_bloodied()
        return super().is_bloodied()

    def is_hp_max(self) -> bool:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: True if current hp equals max hp, False otherwise
        :rtype: bool
        """
        if self.get_current_shape():
            return self.get_current_shape().is_hp_max()
        return super().is_hp_max()

    def get_speed(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: speed
        :rtype: positive integer
        """
        if self.get_current_shape():
            return self.get_current_shape().get_speed()
        return super().get_speed()

    def get_vision(self) -> str:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: vision
        :rtype: one of these strings: "normal", "darkvision", "blindsight", "truesight"
        """
        if self.get_current_shape():
            return self.get_current_shape().get_vision()
        return super().get_vision()

    def get_ability(self, ability: str) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :param ability: the name of an ability score
        :type ability: one of these strings: "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"
        :return: the ability score modifier
        :rtype: int
        :raise: ValueError if *ability* is not valid
        """
        if self.get_current_shape():
            return self.get_current_shape().get_ability(ability)
        return super().get_ability(ability)

    def get_strength(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: strength modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_strength()
        return super().get_strength()

    def get_dexterity(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: dexterity modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_dexterity()
        return super().get_dexterity()

    def get_constitution(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: constitution modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_constitution()
        return super().get_constitution()

    def get_intelligence(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: intelligence modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_intelligence()
        return super().get_intelligence()

    def get_wisdom(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: wisdom modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_wisdom()
        return super().get_wisdom()

    def get_charisma(self) -> int:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: charisma modifier
        :rtype: int
        """
        if self.get_current_shape():
            return self.get_current_shape().get_charisma()
        return super().get_charisma()

    def get_proficiencies(self) -> set:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: proficiencies
        :rtype: set of strings
        """
        if self.get_current_shape():
            return self.get_current_shape().get_proficiencies()
        return super().get_proficiencies()

    def get_vulnerabilities(self) -> set:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: vulnerabilities
        :rtype: set of strings
        """
        if self.get_current_shape():
            return self.get_current_shape().get_vulnerabilities()
        return super().get_vulnerabilities()

    def get_resistances(self) -> set:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: resistances
        :rtype: set of strings
        """
        if self.get_current_shape():
            return self.get_current_shape().get_resistances()
        return super().get_resistances()

    def get_immunities(self) -> set:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: immunities
        :rtype: set of strings
        """
        if self.get_current_shape():
            return self.get_current_shape().get_immunities()
        return super().get_immunities()

    def get_saving_throw(self, ability: str) -> int:
        """
        Get the modifier for an *ability* saving throw. Note: this does NOT roll the saving throw.
        If a wild shape is active, use the statistics of the wild shape.

        :param ability: an ability score name
        :type ability: one of these strings: "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma'
        :return: the modifier for an *ability* saving throw
        :rtype: int
        :raise: ValueError if *ability* is not valid
        """
        if self.get_current_shape():
            return self.get_current_shape().get_saving_throw(ability)
        return super().get_saving_throw(ability)

    def get_attacks(self) -> list:
        """
        If a wild shape is active, use the statistics of the wild shape

        :return: attacks
        :rtype: list of Attacks
        """
        if self.get_current_shape():
            return self.get_current_shape().get_attacks()
        return super().get_attacks()

    def add_wild_shape(self, beast: Creature):
        """
        Add a wild shape

        :param beast: the wild shape to add
        :type beast: Creature
        :return: None
        """
        if not isinstance(beast, Creature):
            self.get_logger().error("Wild shape must be a Creature", stack_info=True)
            raise ValueError("Wild shape must be a Creature")
        if beast in self.get_wild_shapes():
            self.get_logger().warning("Tried to add a wild shape you already have", stack_info=True)
            warnings.warn("Tried to add a wild shape you already have")
            return
        # pylint: disable=protected-access
        beast._intelligence = self._intelligence
        beast._wisdom = self._wisdom
        beast._charisma = self._charisma
        for proficiency in self._proficiencies:
            beast.get_proficiencies().add(proficiency)
        # rebuild saving throws
        beast._saving_throws = {"strength": beast._strength, "dexterity": beast._dexterity,
                               "constitution": beast._constitution, "intelligence": beast._intelligence,
                               "wisdom": beast._wisdom, "charisma": beast._charisma}
        for ability in beast._saving_throws:
            if ability in beast._proficiencies:
                beast._saving_throws[ability] += self._proficiency_mod
        # pylint: enable=protected-access
        self._wild_shapes.append(beast)

    def can_cast(self, spell):
        return super().can_cast(spell) and (not self.get_current_shape()
                                            or (self.get_level() > 17 and not spell.has_component("material"))
                                            or self.get_level() > 20)

    def take_damage(self, damage, damage_type=None):
        """
        Take damage, applying vulnerabilities, resistances, and immunities as necessary.
        If *self* has a wild shape active, that beast takes damage first.
        If the beast reaches 0 hit points,
        the wild shape ends and *self* goes back to their original form and takes the rest of the damage.

        :param damage: the number of hit points of damage to take
        :type damage: positive integer
        :param damage_type: the type of damage
        :type damage_type: str
        :return: the actual damage taken
        :rtype: int
        """
        # TODO: minimize code duplication
        if self.get_current_shape():
            beast = self.get_current_shape()
            if beast.is_vulnerable(damage_type):
                self.get_logger().info("%s (in beast form) is vulnerable to %s!", self.get_name(), damage_type)
                damage *= 2
            elif beast.is_resistant(damage_type):
                self.get_logger().info("%s (in beast form) is resistant to %s.", self.get_name(), damage_type)
                damage //= 2
            elif beast.is_immune(damage_type):
                self.get_logger().info("%s (in beast form) is immune to %s.", self.get_name(), damage_type)
                return
            self.get_logger().info("%s (in beast form) takes %d damage", self.get_name(), damage)
            # skip temp hp
            if damage <= beast.get_current_hp():
                beast._current_hp -= damage
            else:
                damage -= beast.get_current_hp()
                self.end_shape()
                super().take_damage(damage, damage_type)
        else:
            super().take_damage(damage, damage_type)

    def start_shape(self, beast: Creature):
        """
        Shift into the given wild shape

        :param beast: the beast to shift into
        :type beast: Creature
        :return: None
        :raise: ValueError if *self* has no wild shape slots left
        """
        if not isinstance(beast, Creature):
            self.get_logger().error("Cannot shift into something that is not a creature", stack_info=True)
            raise ValueError("Cannot shift into something that is not a creature")
        if not self.get_wild_shape_slots():
            self.get_logger().error("You don't have any wild shape slots left", stack_info=True)
            raise ValueError("You don't have any wild shape slots left")
        self._wild_shape_slots -= 1
        if beast not in self.get_wild_shapes():
            self.get_logger().warning("You are shifting into a Creature not assigned to you. Stats may not be set correctly", stack_info=True)
            warnings.warn("You are shifting into a Creature not assigned to you. Stats may not be set correctly")
        self.get_logger().info("%s shifts into %s", self.get_name(), beast.get_name())
        self._current_shape = beast

    def end_shape(self):
        """
        End the current wild shape

        :return: None
        :raise: ValueError if *self* has no wild shape active
        """
        if not self._current_shape:
            self.get_logger().error("Can't end wild shape when no wild shape is active", stack_info=True)
            raise ValueError("Can't end wild shape when no wild shape is active")
        self.get_logger().info("%s shifts out of %s into their original form.", self._name, self.get_current_shape().get_name())
        self._current_shape = None

class Fighter(Character):
    """
    Fighter character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :param fighting_style: the fighting style that *self* uses
        :type fighting_style: set, list, or tuple of strings (will be converted to set of strings)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 10 + constitution_mod
        if level > 1:
            max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies', None)
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("constitution")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod, "hit_dice": hit_dice})

        super().__init__(**kwargs)

        self.add_feature("fighting style")
        fighting_style = kwargs.get("fighting_style")
        self.add_fighting_style(fighting_style)

        self.add_feature("second wind")
        self._second_wind_slots = 1
        if self.get_level() > 1:
            self.add_feature("action surge")
            self._action_surge_slots = 1
        if self.get_level() > 2:
            self.add_feature("martial archetype")
        if self.get_level() > 4:
            self.add_feature("extra attack")
            self._extra_attack_num = 1
        if self.get_level() > 8:
            self.add_feature("indomitable")
            self._indomitable_slots = 1
        if self.get_level() > 10:
            self._extra_attack_num = 2
        if self.get_level() > 12:
            self._indomitable_slots = 2
        if self.get_level() > 16:
            self._action_surge_slots = 2
            self._indomitable_slots = 3
        if self.get_level() > 19:
            self._extra_attack_num = 3

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Fighter to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Fighter
        """
        if not isinstance(other, Fighter):
            self.get_logger().error("Cannot copy Fighter from other class", stack_info=True)
            raise ValueError("Cannot copy Fighter from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._second_wind_slots = 1
        self._fighting_styles = set()
        for fighting_style in other.get_fighting_styles():
            self.add_fighting_style(fighting_style)
        if self.has_feature("action surge"):
            self._action_surge_slots = other.get_action_surge_slots()
        if self.has_feature("extra attack"):
            self._extra_attack_num = other.get_extra_attack_num()
        if self.has_feature("indomitable"):
            self._indomitable_slots = other.get_indomitable_slots()

    def equals(self, other) -> bool:
        """
        Check equality based on superclass method and these attributes: extra attack num

        :param other: the Fighter to compare
        :type other: Fighter
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_extra_attack_num() == other.get_extra_attack_num()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: action surge slots, indomitable

        :param other: the Fighter to compare
        :type other: Fighter
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_action_surge_slots() == other.get_action_surge_slots() \
            and self.get_indomitable_slots() == other.get_indomitable_slots()

    def get_second_wind_slots(self) -> int:
        """
        :return: second wind slots
        :rtype: non-negative integer
        """
        return self._second_wind_slots

    def get_action_surge_slots(self) -> int:
        """
        :return: action surge slots
        :rtype: non-negative integer
        """
        try:
            return self._action_surge_slots
        except AttributeError:
            return 0

    def get_extra_attack_num(self) -> int:
        """
        :return: the number of extra attacks (additional attacks allowed when using the attack action)
        :rtype: non-negative integer
        """
        try:
            return self._extra_attack_num
        except AttributeError:
            return 0

    def get_indomitable_slots(self) -> int:
        """
        :return: indomitable slots
        :rtype: non-negative integer
        """
        try:
            return self._indomitable_slots
        except AttributeError:
            return 0

    def take_action_surge(self):
        """
        Do an action surge. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* has no action surge slots left
        """
        if self.get_action_surge_slots():
            self._action_surge_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to action surge", stack_info=True)
            raise ValueError("You don't have the slots to action surge")

    def take_extra_attack(self):
        """
        Make an extra attack. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* has no extra attack slots left
        """
        if self.get_extra_attack_num():
            self._extra_attack_num -= 1
        else:
            self.get_logger().error("You don't have the slots for an extra attack", stack_info=True)
            raise ValueError("You don't have the slots for an extra attack")

    def take_indomitable(self):
        """
        Use the indomitable feature. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* doesn't have indomitable slots
        """
        if self.get_indomitable_slots():
            self._indomitable_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to use indomitable", stack_info=True)
            raise ValueError("You don't have the slots to use indomitable")

    def take_second_wind(self):
        """
        Use the second wind feature. NOT IMPLEMENTED YET (i.e., this isn't called anywhere).

        :return: None
        :raise: ValueError if *self* doesn't have second wind slots
        """
        # TODO: make this a bonus action
        if self.get_second_wind_slots():
            self.get_logger().info("%s uses second wind!", self.get_name())
            healing = roll_dice(10, modifier=self.get_level())[0]
            self.take_healing(healing)
            self._second_wind_slots -= 1
        else:
            self.get_logger().error("You don't have the slots to use second wind", stack_info=True)
            raise ValueError("You don't have the slots to use second wind")

class Monk(Character):
    """
    Monk character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("monk weapons")
        proficiencies.add("strength")
        proficiencies.add("dexterity")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"hit_dice": hit_dice, "max_hp": max_hp, "proficiencies": proficiencies,
                       "proficiency_mod": proficiency_mod, "feature_classes": [features.UnarmoredDefenseMonk]})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            if self.get_level() < 6:
                self._unarmored_movement = 10
            elif self.get_level() < 10:
                self._unarmored_movement = 15
            elif self.get_level() < 14:
                self._unarmored_movement = 20
            elif self.get_level() < 18:
                self._unarmored_movement = 25
            else:
                self._unarmored_movement = 30
        if self.get_level() < 5:
            self._martial_arts_dice = (1, 4)
        elif self.get_level() < 11:
            self._martial_arts_dice = (1, 6)
        elif self.get_level() < 17:
            self._martial_arts_dice = (1, 8)
        else:
            self._martial_arts_dice = (1, 10)

        self.add_feature("unarmored defense")
        if self.get_level() > 1:
            self._ki_points = self.get_level()
            self._ki_save_dc = 8 + self.get_proficiency_mod() + self.get_wisdom()
            self.add_feature("flurry of blows")
            self.add_feature("patient defense")
            self.add_feature("step of the wind")
            if self.get_level() > 2:
                self.add_feature("deflect missiles")
            if self.get_level() > 3:
                self.add_feature("slow fall")
            if self.get_level() > 4:
                self.add_feature("extra attack")
                self.add_feature("stunning strike")
            if self.get_level() > 5:
                self.add_feature("ki-empowered strikes")
            if self.get_level() > 6:
                self.add_feature("stillness of mind")
                self.add_feature("evasion")
            if self.get_level() > 9:
                self.add_feature("purity of body")
            if self.get_level() > 12:
                self.add_feature("tongue of the sun and moon")
            if self.get_level() > 13:
                self.add_feature("diamond soul")
                # proficiency in all Saving Throws
                self._proficiencies.add("constitution")
                self._proficiencies.add("intelligence")
                self._proficiencies.add("wisdom")
                self._proficiencies.add("charisma")
                self._saving_throws = {"strength": self.get_strength() + self.get_proficiency_mod(),
                                       "dexterity": self.get_dexterity() + self.get_proficiency_mod(),
                                       "constitution": self.get_constitution() + self.get_proficiency_mod(),
                                       "intelligence": self.get_intelligence() + self.get_proficiency_mod(),
                                        "wisdom": self.get_wisdom() + self.get_proficiency_mod(),
                                       "charisma": self.get_charisma() + self.get_proficiency_mod()}
            if self.get_level() > 17:
                self.add_feature("empty body")
            if self.get_level() == 20:
                self.add_feature("perfect soul")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Monk to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Monk
        """
        if not isinstance(other, Monk):
            self.get_logger().error("Cannot copy Monk from other class", stack_error=True)
            raise ValueError("Cannot copy Monk from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._martial_arts_dice = other.get_martial_arts_dice()
        if self.get_level() > 1:
            self._ki_points = other.get_ki_points()
            self._ki_save_dc = other.get_ki_save_dc()
            if self.get_level() > 13:
                self._saving_throws = {"strength": self.get_strength() + self.get_proficiency_mod(),
                                       "dexterity": self.get_dexterity() + self.get_proficiency_mod(),
                                       "constitution": self.get_constitution() + self.get_proficiency_mod(),
                                       "intelligence": self.get_intelligence() + self.get_proficiency_mod(),
                                       "wisdom": self.get_wisdom() + self.get_proficiency_mod(),
                                       "charisma": self.get_charisma() + self.get_proficiency_mod()}

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        ki save dc

        :param other: the Monk to compare
        :type other: Monk
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_ki_save_dc() == other.get_ki_save_dc()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: ki points

        :param other: the Monk to compare
        :type other: Monk
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_ki_points() == other.get_ki_points()

    def get_martial_arts_dice(self) -> TYPE_DICE_TUPLE:
        """
        :return: martial arts dice
        :rtype: TYPE_DICE_TUPLE
        """
        return self._martial_arts_dice

    def get_ki_points(self) -> int:
        """
        :return: ki points
        :rtype: non-negative integer
        """
        try:
            return self._ki_points
        except AttributeError:
            return 0

    def get_ki_save_dc(self) -> int:
        """
        :return: ki save dc
        :rtype: positive integer
        """
        try:
            return self._ki_save_dc
        except AttributeError:
            self.get_logger().error("Cannot use a ki saving throw attack", stack_info=True)
            raise ValueError("Cannot use a ki saving throw attack")

    def spend_ki_points(self, num: int) -> int:
        """
        Spend *num* number of ki points

        :param num: the number of ki points to spend
        :return: None
        :raise: ValueError if *self* doesn't have enough ki points or *num* is invalid
        """
        if not isinstance(num, int) or num < 1:
            self.get_logger().error("Num of ki points must be a positive integer", stack_info=True)
            raise ValueError("Num of ki points must be a positive integer")
        if num > self.get_ki_points():
            self.get_logger().error("You don't have enough ki points left to spend", stack_info=True)
            raise ValueError("You don't have enough ki points left to spend")
        self._ki_points -= num
        return num

    def add_attack(self, attack):
        """
         Add the given attack. If the attack is related to a monk weapon and dexterity mod is less than strength mod,
         change strength mod for attack and damage to dexterity mod.

        :param attack: the Attack to add
        :type attack: Attack
        :return: None
        """
        if armory.is_monk_weapon(attack.get_weapon()) and self.get_dexterity() > self.get_strength():
            # change from str to dex
            attack.set_attack_mod(attack.get_attack_mod() - self.get_strength() + self.get_dexterity())
            attack.set_damage_mod(attack.get_damage_mod() - self.get_strength() + self.get_dexterity())
        super().add_attack(attack)

class Paladin(SpellCaster, Character):
    """
    Paladin character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :param fighting_style: the fighting style *self* knows
        :type fighting_style: str
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 10 + constitution_mod
        if level > 1:
            max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 0}
        if level > 1:
            spell_slots = {1: 2}
            if level > 2:
                spell_slots.update({1: 3})
            if level > 4:
                spell_slots.update({1: 4, 2: 2})
            if level > 6:
                spell_slots.update({2: 3})
            if level > 8:
                spell_slots.update({3: 2})
            if level > 9:
                spell_slots.update({3: 3})
            if level > 12:
                spell_slots.update({4: 1})
            if level > 14:
                spell_slots.update({4: 2})
            if level > 16:
                spell_slots.update({4: 3, 5: 1})
            if level > 18:
                spell_slots.update({5: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})
        # include empty spell slots so that SpellCaster init doesn't throw a fit

        super().__init__(**kwargs)

        self.add_feature("divine sense")
        self._divine_sense_slots = 1 + self.get_charisma()
        self.add_feature("lay on hands")
        self._lay_on_hands_pool = self.get_level() * 5

        if self.get_level() > 1:
            self.add_feature("fighting style")
            fighting_style = kwargs.get("fighting_style")
            self.add_fighting_style(fighting_style)

            self.add_feature("divine smite")

            if level > 2:
                self.add_feature("divine health")
                self.add_feature("sacred oath")
            if level > 4:
                self.add_feature("extra attack")
            if level > 5:
                self.add_feature("aura of protection")
                self._aura = 10
            if level > 9:
                self.add_feature("aura of courage")
            if level > 10:
                self.add_feature("improved divine smite")
            if level > 13:
                self.add_feature("cleansing touch")
                self._cleansing_touch_slots = max(self.get_charisma(), 1)
            if level > 17:
                self._aura = 30

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Paladin to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Paladin
        """
        if not isinstance(other, Paladin):
            self.get_logger().error("Cannot copy Paladin from other class", stack_info=True)
            raise ValueError("Cannot copy Paladin from other class")
        super().copy_constructor(other, **kwargs)
        self._divine_sense_slots = other.get_divine_sense_slots()
        self._lay_on_hands_pool = other.get_lay_on_hands_pool()
        self._fighting_styles = set()
        for fighting_style in other.get_fighting_styles():
            self.add_fighting_style(fighting_style)
        if other.get_level() > 5:
            self._aura = other.get_aura()
        if other.has_feature("cleansing touch"):
            self._cleansing_touch_slots = other.get_cleansing_touch_slots()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        aura

        :param other: the Paladin to compare
        :type other: Paladin
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_aura() == other.get_aura()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: divine sense slots, lay on hands pool, cleansing touch slots

        :param other: the Paladin to compare
        :type other: Paladin
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_divine_sense_slots() == other.get_divine_sense_slots() \
            and self.get_lay_on_hands_pool() == other.get_lay_on_hands_pool() \
            and self.get_cleansing_touch_slots() == other.get_cleansing_touch_slots()

    def get_divine_sense_slots(self) -> int:
        """
        :return: divine sense slots
        :rtype: non-negative integer
        """
        return self._divine_sense_slots

    def get_lay_on_hands_pool(self) -> int:
        """
        :return: the hit points in the lay on hands pool
        :rtype: non-negative integer
        """
        return self._lay_on_hands_pool

    def get_aura(self) -> int:
        """
        :return: the number of feet for the aura features
        :rtype: non-negative integer
        """
        try:
            return self._aura
        except AttributeError:
            return 0

    def get_cleansing_touch_slots(self) -> int:
        """
        :return: cleansing touch slots
        :rtype: non-negative integer
        """
        try:
            return self._cleansing_touch_slots
        except AttributeError:
            return 0

    def spend_divine_sense_slot(self):
        """
        Spend a divine sense slot

        :return: None
        :raise: ValueError if *self* has no divine sense slots left
        """
        if self.get_divine_sense_slots():
            self._divine_sense_slots -= 1
        else:
            self.get_logger().error("You have no slots left to cast divine sense", stack_info=True)
            raise ValueError("You have no slots left to cast divine sense")

    def spend_cleansing_touch_slot(self):
        """
        Spend a cleansing touch slot

        :return: None
        :raise: ValueError if *self* has no cleansing touch slots left
        """
        if not self.get_cleansing_touch_slots():
            self.get_logger().error("You don't have any cleansing touch slots left", stack_info=True)
            raise ValueError("You don't have any cleansing touch slots left")
        self._cleansing_touch_slots -= 1

    def send_lay_on_hands(self, hp: int, target=None, use="healing"):  # pylint: disable=inconsistent-return-statements
        """
        Use the Lay on Hands feature

        :param hp: the number of hit points to take from the Lay on Hands pool
        :type hp: non-negative integer
        :param target: the Combatant to use Lay on Hands on
        :type target: Combatant
        :param use: what to use Lay on Hands for
        :type use: str
        :return: the hit points healed for, if *use* is "healing"
        :raise: ValueError if *self* doesn't have enough Lay on Hands points
        """
        if self.get_lay_on_hands_pool() >= hp:
            self._lay_on_hands_pool -= hp
            if use == "healing" and isinstance(target, Combatant):
                self.get_logger().info("%s uses Lay on Hands to heal %s", self.get_name(), target.get_name())
                return target.take_healing(hp)
        else:
            self.get_logger().error("You don't have enough lay on hands points left for that", stack_info=True)
            raise ValueError("You don't have enough lay on hands points left for that")

    def send_divine_smite(self, target: Combatant, level=1) -> int:
        """
        Use Divine Smite on a specified target

        :param target: the Combatant to use Divine Smite on
        :type target: Combatant
        :param level: the level to cast Divine Smite at
        :type level: integer between 1 and 9 (inclusive)
        :return: the damage taken
        :rtype: non-negative integer
        :raise: ValueError if *self* doesn't have the Divine Smite feature
        """
        if not self.has_feature("divine smite"):
            self.get_logger().error("You don't have the divine smite feature", stack_info=True)
            raise ValueError("You don't have the divine smite feature")
        self.spend_spell_slot(level)
        dice_num = max(2 + level-1, 5)
        dice_type = 8
        damage = roll_dice(dice_type, num=dice_num)[0]  # TODO: use Dice
        self.get_logger().info("%s uses Divine Smite on %s", self.get_name(), target.get_name())
        return target.take_damage(damage, damage_type="radiant")

class Ranger(SpellCaster, Character):
    """
    Ranger character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :param fighting_style: the fighting style that *self* knows
        :type fighting_style: str

        Class specific parameters:
        :param favored_enemies: the kind of enemies that *self* knows well
        :type favored_enemies: set of strings (each string is one of these: "aberration", "beast", "celestial",
        "construct", "dragon", "elemental", "fey", "fiend", "giant", "monstrosity", "ooze", "plant", "undead")
        :param favored_terrains: the terrains that *self* knows well
        :type favored_terrains: set of strings

        :raise: ValueError if input is invalid

        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 10)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 10 + constitution_mod
        if level > 1:
            max_hp += (6 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("martial weapons")
        proficiencies.add("strength")
        proficiencies.add("dexterity")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 0}
        if level > 1:
            spell_slots.update({1: 2})
            if level > 2:
                spell_slots.update({1: 3})
            if level > 4:
                spell_slots.update({1: 4, 2: 2})
            if level > 6:
                spell_slots.update({2: 3})
            if level > 8:
                spell_slots.update({3: 2})
            if level > 10:
                spell_slots.update({3: 3})
            if level > 12:
                spell_slots.update({4: 1})
            if level > 14:
                spell_slots.update({4: 2})
            if level > 16:
                spell_slots.update({4: 3, 5: 1})
            if level > 19:
                spell_slots.update({5: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "wisdom", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        self.add_feature("favored enemy")
        self._favored_enemies = kwargs.get("favored_enemies")
        if isinstance(self._favored_enemies, (list, tuple, set)):
            self._favored_enemies = set(self._favored_enemies)
        else:
            self._favored_enemies = set()
            favored_enemy = kwargs.get("favored_enemy")
            self._favored_enemies.add(favored_enemy)

        self.add_feature("favored terrain")
        self._favored_terrains = kwargs.get("favored_terrains")
        if isinstance(self._favored_terrains, (list, tuple, set)):
            self._favored_terrains = set(self._favored_terrains)
        else:
            self._favored_terrains = set()
            favored_terrain = kwargs.get("favored_terrain")
            self._favored_terrains.add(favored_terrain)

        if self.get_level() > 1:
            self.add_feature("fighting style")
            fighting_style = kwargs.get("fighting_style")
            self.add_fighting_style(fighting_style)
            if self.get_level() > 2:
                self.add_feature("ranger archetype")
                self.add_feature("primeval awareness")
            if self.get_level() > 4:
                self.add_feature("extra attack")
            if self.get_level() > 7:
                self.add_feature("land's stride")
            if self.get_level() > 9:
                self.add_feature("hide in plain sight")
            if self.get_level() > 13:
                self.add_feature("vanish")
            if self.get_level() > 17:
                self.add_feature("feral senses")
            if self.get_level() > 19:
                self.add_feature("foe slayer")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Ranger to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Ranger
        """
        if not isinstance(other, Ranger):
            self.get_logger().error("Cannot copy Ranger from other class", stack_info=True)
            raise ValueError("Cannot copy Ranger from other class")
        super().copy_constructor(other, **kwargs)
        self._favored_enemies = other.get_favored_enemies().copy()
        self._favored_terrains = other.get_favored_terrains().copy()
        self._fighting_styles = set()
        for fighting_style in other.get_fighting_styles():
            self.add_fighting_style(fighting_style)

    def equals(self, other):
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        favored enemies, favored terrains

        :param other: the Ranger to compare
        :type other: Ranger
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_favored_enemies() == other.get_favored_enemies() \
            and self.get_favored_terrains() == other.get_favored_terrains()

    def get_favored_enemies(self):
        """
        :return: favored enemies
        :rtype: set of strings
        """
        return self._favored_enemies

    def has_favored_enemy(self, enemy) -> bool:
        """
        Determine whether the given enemy is favored or not

        :param enemy: the enemy or name of the enemy
        :type enemy: Creature or str
        :return: True if *enemy* is a favored enemy, False otherwise
        :rtype: bool
        """
        if isinstance(enemy, Creature):
            enemy = enemy.get_creature_type()
        elif isinstance(enemy, str):
            pass
        else:
            self.get_logger().error("Enemy must be a Creature or a string", stack_info=True)
            raise ValueError("Enemy must be a Creature or a string")
        return enemy in self.get_favored_enemies()

    def get_favored_terrains(self) -> set:
        """
        :return: favored terrains
        :rtype: set of strings
        """
        return self._favored_terrains

    def has_favored_terrain(self, terrain) -> bool:
        """
        Determine whether the given terrain is favored or not

        :param terrain: the terrain to look at
        :type terrain: str
        :return: True if *terrain* is a favored terrain, False otherwise
        :rtype: bool
        """
        return terrain in self.get_favored_terrains()

class Rogue(Character):
    """
    Rogue character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)
        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("hand crossbow")
        proficiencies.add("longsword")
        proficiencies.add("rapier")
        proficiencies.add("shortsword")
        proficiencies.add("dexterity")
        proficiencies.add("intelligence")

        proficiency_mod = proficiency_bonus_per_level(level)

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice})

        super().__init__(**kwargs)

        self.add_feature("sneak attack")
        sneak_dice_num = (self.get_level() - 1) // 2 + 1
        self._sneak_attack_dice = (sneak_dice_num, 6)

        if self.get_level() > 1:
            self.add_feature("cunning action")
            if self.get_level() > 4:
                self.add_feature("uncanny dodge")
            if self.get_level() > 6:
                self.add_feature("evasion")
            if self.get_level() > 10:
                self.add_feature("reliable talent")
            if self.get_level() > 13:
                self.add_feature("blindsense")
            if self.get_level() > 14:
                self.add_feature("slippery mind")
                self._proficiencies.add("wisdom")
                self._saving_throws = {"strength": self.get_strength(), "dexterity": self.get_dexterity(),
                                       "constitution": self.get_constitution(), "intelligence": self.get_intelligence(),
                                       "wisdom": self.get_wisdom(), "charisma": self.get_charisma()}
                for ability in self._saving_throws:
                    if ability in self._proficiencies:
                        self._saving_throws[ability] += self._proficiency_mod
            if self.get_level() > 17:
                self.add_feature("elusive")
            if self.get_level() > 19:
                self.add_feature("stroke of luck")
                self._stroke_of_luck_slots = 1

    def copy_constructor(self, other, **kwargs):
        """
         Make *self* a copy of *other*

        :param other: the Rogue to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Rogue
        """
        if not isinstance(other, Rogue):
            self.get_logger().error("Cannot copy Rogue from other class", stack_info=True)
            raise ValueError("Cannot copy Rogue from other class")
        kwargs.update({"level": other.get_level()})
        super().copy_constructor(other, **kwargs)
        self._sneak_attack_dice = other.get_sneak_attack_dice()
        if self.has_feature("stroke of luck"):
            self._stroke_of_luck_slots = other.get_stroke_of_luck_slots()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        sneak attack dice

        :param other: the Rogue to compare
        :type other: Rogue
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_sneak_attack_dice() == other.get_sneak_attack_dice()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: stroke of luck slots

        :param other: the Rogue to compare
        :type other: Rogue
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_stroke_of_luck_slots() == other.get_stroke_of_luck_slots()

    def can_see(self, light_src: str) -> bool:
        """
        Determine whether *self* can see a given light source.
        If *self* can't see according to superclass method, look at blindsense feature.

        :param light_src: a kind of light
        :type light_src: one of these strings: "normal", "dark", "magic"
        :return: True if *self* can see *light_src*, False otherwise
        :rtype: bool
        """
        result = super().can_see(light_src)
        if not result:
            result = self.has_feature("blindsense") and not self.has_condition("deafened")
        return result

    def get_adv_to_be_hit(self) -> int:
        """
        The sum of advantage (+1) and disadvantage (-1) circumstances affecting *self* is stored in *self._adv_to_be_hit*.
        Look at this number and return an integer indicating whether an attack against *self* has advantage, disadvantage, or neither.
        Look at the "elusive" feature.

        :return: positive if attacks against *self* have advantage, negative if they have disadvantage, and 0 otherwise
        :rtype: one of these integers: -1, 0, 1
        """
        super_adv = super().get_adv_to_be_hit()
        if self.has_feature("elusive") and not self.has_condition("incapacitated") and super_adv > 0:
            return 0
        return super_adv

    def get_stroke_of_luck_slots(self) -> int:
        """
        :return: stroke of luck slots
        :rtype: non-negative integer
        """
        try:
            return self._stroke_of_luck_slots
        except AttributeError:
            return 0

    def get_sneak_attack_dice(self) -> TYPE_DICE_TUPLE:
        """
        :return: sneak attack dice
        :rtype: TYPE_DICE_TUPLE
        """
        return self._sneak_attack_dice

    def can_make_sneak_attack(self, weapon, target, adv) -> bool:  # pylint: disable=unused-argument,no-self-use
        """
        Determine if *self* can make a sneak attack against *target*. NOT IMPLEMENTED YET

        :param weapon: the Weapon used for the attack
        :type weapon: Weapon
        :param target: the Combatant being attacked
        :type target: Combatant
        :param adv: advantage (positive), disadvantage (negative), or neither (0)
        :type adv: int
        :return: True if *self* can make a sneak attack against *target*, False otherwise
        :rtype: bool
        """
        # Note: this assumes the attack hit
        try:
            if not (weapon.has_prop("finesse") or isinstance(weapon, armory.RangedWeapon)):
                return False
        except NameError:
            return False
        if adv > 0:
            return True
        return False

    def roll_sneak_attack_dice(self) -> int:
        """
        Roll sneak attack dice

        :return: the number rolled by the sneak attack dice
        :rtype: non-negative integer
        """
        return roll_dice(num=self.get_sneak_attack_dice()[0], dice_type=self.get_sneak_attack_dice()[1])[0]

    def take_stroke_of_luck(self):
        """
        Use the Stroke of Luck feature. NOT IMPLEMENTED YET.

        :return: None
        :raise: ValueError if *self* has no stroke of luck slots
        """
        if not self.get_stroke_of_luck_slots():
            self.get_logger().error("You have no slots left for stroke of luck", stack_info=True)
            raise ValueError("You have no slots left for stroke of luck")
        self._stroke_of_luck_slots -= 1

    def send_attack(self, target, attack, adv=0) -> Optional[int]:
        """
        Attack a given target using a given attack. Roll and add sneak attack damage if applicable

        :param target: the Combatant to attack
        :type target: Combatant
        :param attack: the Attack being made
        :type attack: Attack
        :param adv: indicates whether *self* has advantage for this attack
        :type adv: int
        :return: the damage *target* took from *attack*, or None if the attack failed to hit
        """
        weapon = attack.get_weapon()
        adv_calc = adv + target.get_adv_to_be_hit()
        damage = super().send_attack(attack=attack, target=target)
        if damage is not None and self.can_make_sneak_attack(weapon, target, adv_calc):
            damage += target.take_damage(self.roll_sneak_attack_dice(), weapon.get_damage_type())
        return damage

class Sorcerer(SpellCaster, Character):
    """
    Sorcerer character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)

        Class specific parameters:
        :param metamagic: the names of the metamagic that *self* can do
        :type metamagic: set, list, or tuple of strings (will be converted to set)

        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 6)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 6 + constitution_mod
        if level > 1:
            max_hp += (4 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("dagger")
        proficiencies.add("dart")
        proficiencies.add("sling")
        proficiencies.add("quarterstaff")
        proficiencies.add("light crossbow")
        proficiencies.add("constitution")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 2}
        if level > 1:
            spell_slots.update({1: 3})
            if level > 2:
                spell_slots.update({1:4, 2: 2})
            if level > 3:
                spell_slots.update({2: 3})
            if level > 4:
                spell_slots.update({3: 2})
            if level > 5:
                spell_slots.update({3: 3})
            if level > 6:
                spell_slots.update({4: 1})
            if level > 7:
                spell_slots.update({4: 2})
            if level > 8:
                spell_slots.update({4: 3, 5: 1})
            if level > 9:
                spell_slots.update({5: 2})
            if level > 10:
                spell_slots.update({6: 1})
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})
            if level > 17:
                spell_slots.update({5: 3})
            if level > 18:
                spell_slots.update({6: 2})
            if level > 19:
                spell_slots.update({7: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("font of magic")
            self._sorcery_points = self.get_level()
            self._full_sorcery_points = self._sorcery_points
            if self.get_level() > 2:
                self.add_feature("metamagic")
                self._metamagic = set()
                metamagic = kwargs.get("metamagic", ())
                for item in metamagic:
                    self.add_metamagic(item)

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Sorcerer to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Sorcerer
        """
        if not isinstance(other, Sorcerer):
            self.get_logger().error("Cannot copy Sorcerer from other class", stack_info=True)
            raise ValueError("Cannot copy Sorcerer from other class")
        super().copy_constructor(other, **kwargs)
        if self.get_level() > 1:
            self._sorcery_points = other.get_sorcery_points()
            self._full_sorcery_points = other.get_full_sorcery_points()
            if self.get_level() > 2:
                self._metamagic = other.get_metamagic().copy()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        full sorcery points, metamagic

        :param other: the Sorcerer to compare
        :type other: Sorcerer
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_full_sorcery_points() == other.get_full_sorcery_points() \
            and self.get_metamagic() == other.get_metamagic()

    def current_eq(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: sorcery points

        :param other: the Sorcerer to compare
        :type other: Sorcerer
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_sorcery_points() == other.get_sorcery_points()

    def get_sorcery_points(self) -> int:
        """
        :return: sorcery points
        :rtype: non-negative integer
        """
        if self.has_feature("font of magic"):
            return self._sorcery_points
        return 0

    def get_full_sorcery_points(self) -> int:
        """
        :return: full sorcery points
        :rtype: non-negative integer
        """
        if self.has_feature("font of magic"):
            return self._full_sorcery_points
        return 0

    def get_metamagic(self) -> set:
        """
        :return: metamagic
        :rtype: set of strings
        """
        try:
            return self._metamagic
        except AttributeError:
            return set()

    def has_metamagic(self, item: str) -> bool:
        """
        Determine whether *self* has a given metamagic

        :param item: the metamagic to check for
        :type item: str
        :return: True if *self* has the metamagic *item*, False otherwise
        :rtype: bool
        """
        if not isinstance(item, str):
            self.get_logger().error("Metamagic name must be a string", stack_info=False)
            raise ValueError("Metamagic name must be a string")
        return item in self.get_metamagic()

    def spend_sorcery_points(self, points: int) -> int:
        """
        Spend a given number of sorcery points

        :param points: the number of sorcery points to spend
        :type points: non-negative integer
        :return: the number of sorcery points spent
        :rtype: positive integer
        :raise: ValueError if *points* is invalid or *self* doesn't have enough sorcery points
        """
        if not self.has_feature("font of magic"):
            self.get_logger().error("You can't spend sorcery points because you can't store sorcery points yet", stack_info=True)
            raise ValueError("You can't spend sorcery points because you can't store sorcery points yet")
        if not isinstance(points, int) or points < 1:
            self.get_logger().error("Sorcery points must be a positive integer", stack_info=True)
            raise ValueError("Sorcery points must be a positive integer")
        if points > self.get_sorcery_points():
            self.get_logger().error("You doesn't have enough sorcery points to spend", stack_info=True)
            raise ValueError("You doesn't have enough sorcery points to spend")
        self._sorcery_points -= points
        return points

    def reset_sorcery_points(self):
        """
        Reset sorcery points

        :return: None
        """
        if not self.has_feature("font of magic"):
            self.get_logger().error("You can't reset sorcery points because you can't store sorcery points yet", stack_info=True)
            raise ValueError("You can't reset sorcery points because you can't store sorcery points yet")
        self._sorcery_points = self.get_full_sorcery_points()
        self.get_logger().info("%s resets sorcery points", self.get_name())

    def spell_slot_to_sorcery_points(self, level: int):
        """
        Convert a spell slot to sorcery points

        :param level: the level of the spell slot being used
        :type level: integer between 1 and 9 (inclusive)
        :return: None
        :raise: ValueError if *level* is invalid
        """
        if not self.has_feature("font of magic"):
            self.get_logger().error("You can't convert a spell slot to sorcery points because you can't store sorcery points yet", stack_info=True)
            raise ValueError("You can't convert a spell slot to sorcery points because you can't store sorcery points yet")
        if not isinstance(level, int) or not (0 < level < 10):  # pylint: disable=superfluous-parens
            self.get_logger().error("Spell slot level must be an integer between 1 and 9", stack_info=True)
            raise ValueError("Spell slot level must be an integer between 1 and 9")
        self.spend_spell_slot(level)
        self._sorcery_points += level
        self.get_logger().info("%s converts a %dth level spell slot to sorcery points", self.get_name(), level)

    def sorcery_points_to_spell_slot(self, level: int) -> int:
        """
        Convert sorcery points to a spell slot

        :param level: the level of spell slot to gain
        :type level: integer between 1 and 5 (inclusive)
        :return: the number of sorcery points spent
        :rtype: positive integer
        :raise ValueError if *level* is invalid
        """
        self.get_logger().info("%s (tries to) convert sorcery points to a level %d spell slot", self.get_name(), level)
        if level == 1:
            result = self.spend_sorcery_points(2)
            self.get_spell_slots()[1] += 1
            return result
        if level == 2:
            result = self.spend_sorcery_points(3)
            self.get_spell_slots()[2] += 1
            return result
        if level == 3:
            result = self.spend_sorcery_points(5)
            self.get_spell_slots()[3] += 1
            return result
        if level == 4:
            result = self.spend_sorcery_points(6)
            self.get_spell_slots()[4] += 1
            return result
        if level == 5:
            result = self.spend_sorcery_points(7)
            self.get_spell_slots()[5] += 1
            return result
        self.get_logger().error("Spell slot level must be an integer between 1 and 5", stack_info=True)
        raise ValueError("Spell slot level must be an integer between 1 and 5")

    def add_metamagic(self, item: str):
        """
        Add the given metamagic

        :param item: the metamagic to add
        :type item: one of these strings: "careful", "distant", "empowered", "extended", "heightened", "quickened", "subtle", "twinned"
        :return: None
        :raise: ValueError if *item* is invalid
        """
        if not self.has_feature("metamagic"):
            self.get_logger().error("You can't do metamagic yet", stack_info=True)
            raise ValueError("You can't do metamagic yet")
        if len(self.get_metamagic()) >= 2 + (self.get_level() - 3) // 7:
            self.get_logger().error("You can't add another metamagic option; you aren't a high enough level", stack_info=True)
            raise ValueError("You can't add another metamagic option; you aren't a high enough level")
        if item in ["careful", "distant", "empowered", "extended", "heightened", "quickened", "subtle", "twinned"]:
            if self.has_metamagic(item):
                self.get_logger().warning("You already have that kind of metamagic (%s). It cannot be added again.", item, stack_info=True)
                warnings.warn("You already have that kind of metamagic (%s). It cannot be added again." % item)
                return
            self._metamagic.add(item)
            self.get_logger().info("%s adds %s metamagic", self.get_name(), item)
        else:
            self.get_logger().error('Metamagic must be one of these kinds: "careful", "distant", "empowered", '
                                    '"extended", "heightened", "quickened", "subtle", "twinned"', stack_info=True)
            raise ValueError('Metamagic must be one of these kinds: "careful", "distant", "empowered", "extended", '
                             '"heightened", "quickened", "subtle", "twinned"')

class Warlock(SpellCaster, Character):
    """
    Warlock character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)

        Class specific parameters:
        :param eldritch_invocations: the Eldritch Invocations that *self* knows
        :type eldritch_invocations: set of strings
        :param pact_boon: Pact Boon
        :type pact_boon: str

        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 8)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 8 + constitution_mod
        if level > 1:
            max_hp += (5 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("simple weapons")
        proficiencies.add("wisdom")
        proficiencies.add("charisma")

        proficiency_mod = proficiency_bonus_per_level(level)

        if level == 1:
            spell_slots = {1: 1}
        elif level == 2:
            spell_slots = {1: 2}
        elif level < 5:
            spell_slots = {2: 2}
        elif level < 7:
            spell_slots = {3: 2}
        elif level < 9:
            spell_slots = {4: 2}
        elif level < 11:
            spell_slots = {5: 2}
        elif level < 17:
            spell_slots = {5: 3}
        else:
            spell_slots = {5: 4}
        if level > 10:
            spell_slots.update({6: 1})  # combine Mystic Arcanum with regular spell slots because they are about the same
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "charisma", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 1:
            self.add_feature("eldritch invocations")
            self._eldritch_invocations = kwargs.get("eldritch_invocations")
            if isinstance(self._eldritch_invocations, (tuple, list)):
                self._eldritch_invocations = set(self._eldritch_invocations)
            elif not isinstance(self._eldritch_invocations, set):
                raise ValueError("Eldritch invokations must be a set (or a list or tuple to convert to set)")

            if self.get_level() > 2:
                self.add_feature("pact boon")
                self._pact_boon = kwargs.get("pact_boon")
                if not self._pact_boon:
                    raise ValueError("Must provide a pact boon")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Warlock to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Warlock
        """
        if not isinstance(other, Warlock):
            self.get_logger().error("Cannot copy Warlock from other class", stack_info=True)
            raise ValueError("Cannot copy Warlock from other class")
        super().copy_constructor(other, **kwargs)
        if self.has_feature("eldritch invocations"):
            self._eldritch_invocations = other.get_eldritch_invocations().copy()
        if self.has_feature("pact boon"):
            self._pact_boon = other.get_pact_boon()

    def equals(self, other):
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        eldritch invocations, pact boon

        :param other: the Warlock to compare
        :type other: Warlock
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_eldritch_invocations() == other.get_eldritch_invocations() \
            and self.get_pact_boon() == other.get_pact_boon()

    def get_pact_boon(self) -> Optional[str]:
        """
        :return: pact boon
        :rtype: str (or None)
        """
        if self.has_feature("pact boon"):
            return self._pact_boon
        return None

    def get_eldritch_invocations(self) -> Optional[set]:
        """
        :return: eldritch invocations
        :rtype: set of strings (or None)
        """
        if self.has_feature("eldritch invocations"):
            return self._eldritch_invocations
        return None

    def has_eldritch_invocation(self, invocation: str) -> bool:
        """
        Determine if *self* has a given eldritch invocation

        :param invocation: the eldritch invocation to look for
        :type invocation: str
        :return: True if *self* has the eldritch invocation *invocation*, False otherwise
        :rtype: bool
        """
        if self.has_feature("eldritch invocations"):
            return invocation in self.get_eldritch_invocations()
        return False

class Wizard(SpellCaster, Character):
    """
    Wizard character class
    """
    def __init__(self, **kwargs):
        """
        Validate the input and set the instance variables, calling *copy_constructor* if necessary

        :param kwargs: keyword arguments. Some of the keyword arguments are overridden by this class.
        :param copy: if this argument is provided with anything that evaluates to True, the copy constructor is called
        :param name: what *self* is called. A unique name is recommended but not required
        :type name: str
        :param vulnerabilities: all the damage types that *self* is vulnerable to
        :type vulnerabilities: set, list, or tuple of strings (will be converted to set of strings)
        :param resistances: all the damage types that *self* is resistant to
        :type resistances: set, list, or tuple of strings (will be converted to set of strings)
        :param immunities: all the damage types that *self* is immune to
        :type immunities: set, list, or tuple of strings (will be converted to set of strings)
        :param ac: *self's* armor class
        :type ac: positive integer
        :param temp_hp: temporary hit points
        :type temp_hp: non-negative integer
        :param conditions: all conditions currently affecting *self*
        :type conditions: list of strings
        :param strength: strength score. Will be converted to modifier and stored as such.
        :type strength: integer between 1 and 30 (inclusive)
        :param strength_mod: dexterity modifier
        :type strength_mod: int
        :param dexterity: dexterity score. Will be converted to modifier and stored as such.
        :type dexterity: integer between 1 and 30 (inclusive)
        :param dexterity_mod: dexterity modifier
        :type dexterity_mod: int
        :param constitution: constitution score. Will be converted to modifier and stored as such.
        :type constitution: integer between 1 and 30 (inclusive)
        :param constitution_mod: constitution modifier
        :type constitution_mod: int
        :param intelligence: intelligence score. Will be converted to modifier and stored as such.
        :type intelligence: integer between 1 and 30 (inclusive)
        :param intelligence_mod: intelligence modifier
        :type intelligence_mod: int
        :param wisdom: wisdom score. Will be converted to modifier and stored as such.
        :type wisdom: integer between 1 and 30 (inclusive)
        :param wisdom_mod: wisdom modifier
        :type wisdom_mod: int
        :param charisma: charisma score. Will be converted to modifier and stored as such.
        :type charisma: integer between 1 and 30 (inclusive)
        :param charisma_mod: charisma modifier
        :type charisma_mod: int
        :param death_saves: NOT IMPLEMENTED YET
        :param attacks: NOT IMPLEMENTED YET
        :param weapons: Weapons (see weapons module) that *self* has available to use
        :type weapons: list of Weapons
        :param size: size
        :type size: one of these strings: "tiny", "small", "medium", "large", "huge", "gargantuan"
        :param items: NOT IMPLEMENTED YET
        :param level: character level
        :type level: integer between 1 and 20 (inclusive)

        Class specific parameters:
        :param spell_mastery: the Spells *self* has mastered
        :type spell_mastery: set, list, or tuple of strings or Spells (will be converted to set of strings)
        :param signature_spells: *self's* Signature Spells
        :type signature_spells: set, list, or tuple of strings or Spells (will be converted to set of strings)

        :raise: ValueError if input is invalid
        """
        copy = kwargs.get("copy")
        if copy:
            self.copy_constructor(copy, name=kwargs.get("name"))
            return

        level = kwargs.get("level")
        if not level:
            raise ValueError("No level provided or level is 0")
        if not isinstance(level, int) or level < 1 or level > 20:
            raise ValueError("Level must be an integer between 1 and 20")
        hit_dice = (1 * level, 6)

        constitution = kwargs.get('constitution')
        if not constitution:
            constitution_mod = kwargs.get("constitution_mod")
            if constitution_mod is not None:
                if isinstance(constitution_mod, int):
                    self._constitution = constitution_mod
                else:
                    raise ValueError("Constitution mod must be an integer")
            else:
                raise ValueError("Must provide constitution score or modifier")
        else:  # pragma: no cover
            constitution_mod = ability_to_mod(constitution)

        max_hp = kwargs.get("max_hp")
        if max_hp and (not isinstance(max_hp, int) or max_hp <= 0):
            raise ValueError("Must provide positive integer max hp")
        max_hp = 6 + constitution_mod
        if level > 1:
            max_hp += (4 + constitution_mod) * (level - 1)

        proficiencies = kwargs.get('proficiencies')
        if isinstance(proficiencies, (tuple, list, set)):
            proficiencies = set(proficiencies)
        elif proficiencies is None:  # pragma: no cover
            proficiencies = set()
        else:  # pragma: no cover
            raise ValueError("Proficiencies must be provided as a set, list, or tuple")
        proficiencies.add("dagger")
        proficiencies.add("dart")
        proficiencies.add("sling")
        proficiencies.add("quarterstaff")
        proficiencies.add("light crossbow")
        proficiencies.add("intelligence")
        proficiencies.add("wisdom")

        proficiency_mod = proficiency_bonus_per_level(level)

        spell_slots = {1: 2}
        if level > 1:
            spell_slots.update({1: 3})
            if level > 2:
                spell_slots.update({1:4, 2: 2})
            if level > 3:
                spell_slots.update({2: 3})
            if level > 4:
                spell_slots.update({3: 2})
            if level > 5:
                spell_slots.update({3: 3})
            if level > 6:
                spell_slots.update({4: 1})
            if level > 7:
                spell_slots.update({4: 2})
            if level > 8:
                spell_slots.update({4: 3, 5: 1})
            if level > 9:
                spell_slots.update({5: 2})
            if level > 10:
                spell_slots.update({6: 1})
            if level > 12:
                spell_slots.update({7: 1})
            if level > 14:
                spell_slots.update({8: 1})
            if level > 16:
                spell_slots.update({9: 1})
            if level > 17:
                spell_slots.update({5: 3})
            if level > 18:
                spell_slots.update({6: 2})
            if level > 19:
                spell_slots.update({7: 2})

        kwargs.update({"max_hp": max_hp, "proficiencies": proficiencies, "proficiency_mod": proficiency_mod,
                       "hit_dice": hit_dice, "spell_ability": "intelligence", "spell_slots": spell_slots})

        super().__init__(**kwargs)

        if self.get_level() > 17:
            self.add_feature("spell mastery")
            self._spell_mastery_names = set()
            spell_names = kwargs.get("spell_mastery")
            if not isinstance(spell_names, (set, list, tuple)):
                raise ValueError("Spell mastery names must be provided in a set, list, or tuple")
            for name in spell_names:
                if not isinstance(name, str):
                    if isinstance(name, Spell):
                        name = name.get_name()
                    else:
                        raise ValueError("Spell mastery must be names of spells (or actual spells to get names from)")
                if len(self._spell_mastery_names) < 2:
                    self._spell_mastery_names.add(name)
                else:
                    raise ValueError("Too many spell mastery spells/names provided")
            if self.get_level() > 19:
                self.add_feature("signature spells")
                self._signature_spell_slots = dict()
                signature_spell_names = kwargs.get("signature_spells")
                if not isinstance(signature_spell_names, (set, list, tuple)):
                    raise ValueError("Signature spell names must be provided in a set, list, or tuple")
                for name in signature_spell_names:
                    if not isinstance(name, str):
                        if isinstance(name, Spell):
                            name = name.get_name()
                        else:
                            raise ValueError("Signature spell must be names of spells (or actual spells to get names from)")
                    if len(self._signature_spell_slots) < 2:
                        self._signature_spell_slots[name] = 1
                    else:
                        raise ValueError("Too many spell mastery spells/names provided")

    def copy_constructor(self, other, **kwargs):
        """
        Make *self* a copy of *other*

        :param other: the Wizard to copy
        :param kwargs: keyword arguments (see superclass method)
        :return: None
        :raise: ValueError if *other* is not a Wizard
        """
        if not isinstance(other, Wizard):
            self.get_logger().error("Cannot copy something that is not a Wizard", stack_info=True)
            raise ValueError("Cannot copy something that is not a Wizard")
        super().copy_constructor(other, **kwargs)
        if self.has_feature("spell mastery"):
            self._spell_mastery_names = other.get_spell_mastery_names()
        if self.has_feature("signature spells"):
            self._signature_spell_slots = other.get_signature_spell_slots().copy()

    def equals(self, other) -> bool:
        """
        Compare *self* and *other* to determine if they are equal based on the superclass method and these attributes:
        spell mastery, signature spells

        :param other: the Wizard to compare
        :type other: Wizard
        :return: True if *self* equals *other*, False otherwise
        :rtype: bool
        """
        return super().equals(other) \
            and self.get_spell_mastery_names() == other.get_spell_mastery_names() \
            and self.get_signature_spells() == other.get_signature_spells()

    def current_eq(self, other):
        """
        Compare *self* and *other* to determine if they are identical based on the attributes checked in *equals*
        and also these attributes: signature spell slots

        :param other: the Wizard to compare
        :type other: Wizard
        :return: True if *self* is identical to *other*, False otherwise
        :rtype: bool
        """
        return super().current_eq(other) \
            and self.get_signature_spell_slots() == other.get_signature_spell_slots()

    def get_spell_mastery_names(self) -> set:
        """
        :return: name of Spells that *self* has Spell Mastery for
        :rtype: set of strings
        """
        return self._spell_mastery_names

    def has_spell_mastery(self, item) -> bool:
        """
        Determine if *self* has mastery of a given spell

        :param item: a Spell or name of a Spell
        :type item: Spell or str
        :return: True if *self* has mastery of the spell *item*, False otherwise
        :rtype: bool
        """
        if not self.has_feature("spell mastery"):
            return False
        if isinstance(item, str):
            name = item
        elif isinstance(item, Spell):
            name = item.get_name()
        else:
            self.get_logger().error("Must pass a string or a Spell to has_spell_mastery", stack_info=True)
            raise ValueError("Must pass a string or a Spell to has_spell_mastery")
        return name in self.get_spell_mastery_names()

    def get_signature_spell_slots(self) -> dict:
        """
        :return: signature spell slots
        :rtype: dict mapping spell names to the number of slots left to use that as a signature spell
        """
        return self._signature_spell_slots

    def get_signature_spells(self) -> list:
        """
        :return: singature spell names
        :rtype: list of strings
        """
        return sorted(self.get_signature_spell_slots().keys())

    def has_signature_spell(self, item) -> bool:
        """
        Determine whether a given spell is a signature spell

        :param item: a Spell or the name of a Spell
        :type item: Spell or str
        :return: True if *item* is a signature spell, False otherwise
        :rtype: bool
        """
        if not self.has_feature("signature spells"):
            return False
        if isinstance(item, str):
            name = item
        elif isinstance(item, Spell):
            name = item.get_name()
        else:
            self.get_logger().error("Must pass a string or a Spell to has_signature_spell", stack_info=True)
            raise ValueError("Must pass a string or a Spell to has_signature_spell")
        return name in self.get_signature_spell_slots()

    def spend_spell_slot(self, level: int, spell=None):
        """
        Spend a spell slot of the given level. Deal with spell mastery and signature spells appropriately.

        :param level: the spell level
        :type level: an integer from 0 to 9 (inclusive)
        :param spell: the spell that is being used
        :type spell: Spell
        :return: None
        :raise: ValueError if *self* doesn't have any spell slots of *level* level
        """
        if self.has_spell_mastery(spell) and level == spell.get_level():
            return
        if self.has_signature_spell(spell) and level == spell.get_level():
            if self._signature_spell_slots[spell.get_name()]:
                self._signature_spell_slots[spell.get_name()] -= 1
                return
        super().spend_spell_slot(level)
