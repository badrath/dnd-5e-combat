class Armor:
    """
    This class represents Armor.

    :ac (int): the base armor class (without including dex modifier)

    :dex bonus (int): the maximum dexterity bonus that can be applied to the ac

    :strength (int): the minimum strength modifier required to wear the armor without a movement penalty (TODO: implement movement penalty)

    :stealth (int): -1 if the armor imposes disadvantage on stealth checks, 1 if it imposes advantage on stealth checks, 0 otherwise
    (TODO: implement stealth penalty)

    TODO: implement proficiency

    """
    ac = 11
    dex_bonus = 10
    strength = -5
    stealth = 0

    @classmethod
    def get_ac(cls):
        return cls.ac

    @classmethod
    def get_dex_bonus(cls):
        return cls.dex_bonus

    @classmethod
    def get_strength(cls):
        return cls.strength

    @classmethod
    def get_stealth(cls):
        return cls.stealth

    @classmethod
    def get_total_ac(cls, combatant):
        try:
            return cls.get_ac() + min(combatant.get_dexterity(), cls.get_dex_bonus())
        except AttributeError:
            raise ValueError("Invalid combatant to don this Armor (must be type Combatant)")

class LightArmor(Armor):
    """
    Light Armor
    """
    pass  # pylint: disable=unnecessary-pass

class MediumArmor(Armor):
    """
    Medium Armor, which allows maximum dexterity bonus of 2
    """
    dex_bonus = 2

class HeavyArmor(Armor):
    """
    Heavy Armor, which imposes disadvantage on stealth checks
    """
    stealth = -1
    dex_bonus = 0

class PaddedArmor(LightArmor):
    """
    :ac: 11 + dex
    :stealth: disadvantage
    """
    ac = 11
    stealth = -1

class LeatherArmor(LightArmor):
    """
    :ac: 11 + dex
    """
    ac = 11

class StuddedLeatherArmor(LightArmor):
    """
    :ac: 12 + dex
    """
    ac = 12

class HideArmor(MediumArmor):
    """
    :ac: 12 + dex (max 2)
    """
    ac = 12

class ChainShirtArmor(MediumArmor):
    """
    :ac: 13 + dex (max 2)
    """
    ac = 13

class ScaleMailArmor(MediumArmor):
    """
    :ac: 14 + dex (max 2)
    :stealth: disadvantage
    """
    ac = 14
    stealth = -1

class BreastplateArmor(MediumArmor):
    """
    :ac: 14 + dex (max 2)
    """
    ac = 14

class HalfPlateArmor(MediumArmor):
    """
    :ac: 15 + dex (max 2)
    :stealth: disadvantage
    """
    ac = 15
    stealth = -1

class RingMailArmor(HeavyArmor):
    """
    :ac: 14
    :stealth: disadvantage
    """
    ac = 14

class ChainMailArmor(HeavyArmor):
    """
    :ac: 16
    :strength: 1 (Note: RAW strength restriction is 13)
    :stealth: disadvantage
    """
    ac = 16
    strength = 1

class SplintArmor(HeavyArmor):
    """
    :ac: 17
    :strength: 2 (Note: RAW strength restriction is 15)
    :stealth: disadvantage
    """
    ac = 17
    strength = 2

class PlateArmor(HeavyArmor):
    """
    :ac: 18
    :strength: 2 (Note: RAW strength restriction is 15)
    :stealth: disadvantage
    """
    ac = 18
    strength = 2

class Shield:
    """
    This class represents a shield

    :ac (int): the bonus to AC from having the shield equipped
    """
    ac = 2

    @classmethod
    def get_ac(cls):
        return cls.ac
