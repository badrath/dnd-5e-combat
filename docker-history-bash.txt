SIZE COMMENT
RUN pip install --trusted-host pypi.python.org -r requirements.txt 32MB 
ADD dir:4507aa076e65ba2090d0998b206f82423062e4bf72c259ee9755a04d2bf037fc in . 5.97MB 
WORKDIR / 0B 
CMD ["python3"] 0B 
RUN set -ex; wget -O get-pip.py 'https://bootstrap.pypa.io/get-pip.py'; python get-pip.py --disable-pip-version-check --no-cache-dir "pip==$PYTHON_PIP_VERSION" ; pip --version; find /usr/local -depth \( \( -type d -a \( -name test -o -name tests \) \) -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \) -exec rm -rf '{}' +; rm -f get-pip.py 6MB 
ENV PYTHON_PIP_VERSION=10.0.1 0B 
RUN cd /usr/local/binn   &&  ln -s idle3 idlen   &&  ln -s pydoc3 pydocn   &&  ln -s python3 pythonn   &&  ln -s python3-config python-config 32B 
RUN set -exn   &&  wget -O python.tar.xz "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz"n   &&  wget -O python.tar.xz.asc "https://www.python.org/ftp/python/${PYTHON_VERSION%%[a-z]*}/Python-$PYTHON_VERSION.tar.xz.asc"n   &&  export GNUPGHOME="$(mktemp -d)"n   &&  gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY"n   &&  gpg --batch --verify python.tar.xz.asc python.tar.xzn   &&  { command -v gpgconf > /dev/nulln   &&  gpgconf --kill all || :; }n   &&  rm -rf "$GNUPGHOME" python.tar.xz.ascn   &&  mkdir -p /usr/src/pythonn   &&  tar -xJC /usr/src/python --strip-components=1 -f python.tar.xzn   &&  rm python.tar.xzn   &&  cd /usr/src/pythonn   &&  gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"n   &&  ./configure --build="$gnuArch" --enable-loadable-sqlite-extensions --enable-shared --with-system-expat --with-system-ffi --without-ensurepipn   &&  make -j "$(nproc)"n   &&  make installn   &&  ldconfign   &&  find /usr/local -depth \( \( -type d -a \( -name test -o -name tests \) \) -o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \) \) -exec rm -rf '{}' +n   &&  rm -rf /usr/src/pythonn   &&  python3 --version 65.2MB 
ENV PYTHON_VERSION=3.6.6 0B 
ENV GPG_KEY=0D96DF4D4110E5C43FBFB17F2D347EA6AA65421D 0B 
RUN apt-get updaten   &&  apt-get install -y --no-install-recommends tk-devn   &&  rm -rf /var/lib/apt/lists/* 16.8MB 
ENV LANG=C.UTF-8 0B 
ENV PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin 0B 
RUN set -ex; apt-get update; apt-get install -y --no-install-recommends autoconf automake bzip2 dpkg-dev file g++ gcc imagemagick libbz2-dev libc6-dev libcurl4-openssl-dev libdb-dev libevent-dev libffi-dev libgdbm-dev libgeoip-dev libglib2.0-dev libjpeg-dev libkrb5-dev liblzma-dev libmagickcore-dev libmagickwand-dev libncurses5-dev libncursesw5-dev libpng-dev libpq-dev libreadline-dev libsqlite3-dev libssl-dev libtool libwebp-dev libxml2-dev libxslt-dev libyaml-dev make patch xz-utils zlib1g-dev $( if apt-cache show 'default-libmysqlclient-dev' 2>/dev/null | grep -q '^Version:'; then echo 'default-libmysqlclient-dev'; else echo 'libmysqlclient-dev'; fi ) ; rm -rf /var/lib/apt/lists/* 556MB 
RUN apt-get updaten   &&  apt-get install -y --no-install-recommends bzr git mercurial openssh-client subversion procpsn   &&  rm -rf /var/lib/apt/lists/* 142MB 
RUN set -ex; if ! command -v gpg > /dev/null; then apt-get update; apt-get install -y --no-install-recommends gnupg dirmngr ; rm -rf /var/lib/apt/lists/*; fi 7.8MB 
RUN apt-get updaten   &&  apt-get install -y --no-install-recommends ca-certificates curl netbase wgetn   &&  rm -rf /var/lib/apt/lists/* 23.2MB 
CMD ["bash"] 0B 
ADD file:370028dca6e8ca9ed228549d52231cf8139515cc3a14c00aaed75a60b679775f in / 101MB 
